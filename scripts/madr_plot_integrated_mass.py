#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on This Oct 31 15:27 2024

@author: darwin
"""

import os
import numpy as np
from pNbody import *
from pNbody import Nbody
import matplotlib.pyplot as plt
import argparse
from MaDarwinR.scripts_utils import RawTextArgumentDefaultsHelpFormatter, store_as_array
from MaDarwinR.stars_utils import compute_integrated_mass
from MaDarwinR.cosmo import get_redshift

#Ignore division errors and log(0) errors
np.seterr(divide='ignore')

# %% Functions


def make_plot(t_mean, integrated_mass, output_location, output_filename, **kwargs):
    dpi = kwargs.get("dpi", 300)
    image_format = kwargs.get("image_format", "pdf")
    figsize = kwargs.get("figsize", (4, 4))
    x_min = kwargs.get("x_min", np.min(t_mean))
    x_max = kwargs.get("x_max", np.max(t_mean))
    y_min = kwargs.get("y_min", np.min(integrated_mass))
    y_max = kwargs.get("y_max", np.max(integrated_mass))

    fig, ax = plt.subplots(num=3, ncols=1, nrows=1, figsize=figsize, layout="tight")
    ax.cla()
    ax.plot(t_mean, integrated_mass)
    ax.set_xlabel("Time [Gyr]")
    ax.set_ylabel("Mass [M$_\odot$]")

    if x_min is None:
        x_min = ax.get_xlim()[0]
    if x_max is None:
        x_max = ax.get_xlim()[1]

    if y_min is None:
        y_min = ax.get_ylim()[0]
    if y_max is None:
        y_max = ax.get_ylim()[1]

    ax.set_xlim([x_min, x_max])
    ax.set_ylim([y_min, y_max])

    plt.savefig(output_location + output_filename +
                "_imass." + image_format, format=image_format, bbox_inches='tight', dpi=dpi)
    plt.close()

    #Save the data
    np.savez_compressed(output_location+output_filename, time=t_mean, integrated_mass=integrated_mass)

    return ax

# %% Parser object


def parse_option():
    description = """"
Plot the integrated mass from the SFR
    """
    epilog = """
Examples:
--------

WIP
madr_plot_integrated_mass.py.py snap_0020_UFD.hdf5 --n_bins=25 -o snap_0020 --output_location="imass"
madr_plot_integrated_mass.py.py snap_0020_UFD.hdf5 --n_bins=25 -o snap_0020
madr_plot_integrated_mass.py.py snap/*.hdf5 --n_bins=25 --output_location="imass"

"""
    parser = argparse.ArgumentParser(description=description, epilog=epilog,
                                     formatter_class=RawTextArgumentDefaultsHelpFormatter)

    parser.add_argument("files",
                        nargs="+",
                        type=str,
                        help="File name(s).")

    parser.add_argument("--output_location",
                        action="store",
                        type=str,
                        dest="output_location",
                        default=None,
                        help="Name of the output location")

    parser.add_argument("-o",
                        action="store",
                        type=str,
                        dest="output_filename",
                        default=None,
                        help="Name of the output file. Use it if you only give ONE input file. Otherwise, "
                        "all files will have the same name.")

    parser.add_argument("-t",
                        action="store",
                        type=str,
                        dest="ftype",
                        default='swift',
                        help="Type of the output file")

    # Particle parameters
    parser.add_argument("--n_bins",
                        action="store",
                        type=int,
                        default=100,
                        help="Number of bins for the histogram IMF.")

    parser.add_argument("--image_format",
                        action="store",
                        type=str,
                        default="png",
                        help="Format of the output image file. This must be recognized by Matplotlib.")

    parser.add_argument("--dpi",
                        action="store",
                        type=float,
                        default=300,
                        help="Output image resolution.")

    parser.add_argument("--x_min",
                        action="store",
                        type=float,
                        default=0.5,
                        help="Minimal x-axis value.")

    parser.add_argument("--x_max",
                        action="store",
                        type=float,
                        default=None,
                        help="Maximal x-axis value.")

    parser.add_argument("--y_min",
                        action="store",
                        type=float,
                        default=None,
                        help="Minimal y-axis value.")

    parser.add_argument("--y_max",
                        action="store",
                        type=float,
                        default=None,
                        help="Maximal y-axis value.")

    parser.add_argument("--figsize",
                        action=store_as_array,
                        nargs=2,
                        type=float,
                        default=np.array([6.4,4.8]),
                        help="Size of the figures (width, height)")

    parser.add_argument("--stylesheet",
                        action="store",
                        type=str,
                        default=None,
                        help="Matplotlib stylesheet.")

    parser.parse_args()
    args = parser.parse_args()
    files = args.files

    for f in files:
        if not os.path.exists(f):
            raise FileNotFoundError("You need to provide one file")

    return args, files

# %% Main


if __name__ == "__main__":
    # Parse the input
    opt, files = parse_option()

    print("\n-------------------------------------")
    print("Welcome to integrated mass script !\n")

    output_location = opt.output_location
    output_filename = opt.output_filename

    n_bins = opt.n_bins
    x_min = opt.x_min
    x_max = opt.x_max
    y_min = opt.y_min
    y_max = opt.y_max
    image_format = opt.image_format
    dpi = opt.dpi
    figsize = opt.figsize
    stylesheet = opt.stylesheet

    if stylesheet is not None:
        plt.style.use(stylesheet)


    # If no output, exit
    if output_location is None:
        output_location = ""
    else:
        if not (os.path.exists(output_location)):
            print("Output directory does not exist. It will be created.")
            os.makedirs(output_location)

        # Adds the "/" at the end if needed
        if (output_location[-1] != "/"):
            output_location = output_location+"/"

    for filename in files:

        # If we do not give output_filename
        # Notice that I keep opt.output_filename, otherwise this condition is not fullfilled for the next
        # iteration
        if opt.output_filename is None:
            # Keeps the base name (without /path/to/file) and without extension .hdf5
            output_filename = os.path.split(filename)[1]
            output_filename = os.path.splitext(output_filename)[0]

        # Select the object
        print("Loading the data...")
        nb_star = Nbody(p_name=filename, ptypes=[4])

        # Do not continue if you do not find stars.
        if nb_star.npart[4] == 0:
            print("No stars found in this file. Continue to the next file...")
            continue

        # Compute the SFR
        print("Computing integrated mass from SFR...")
        t_mean, integrated_mass = compute_integrated_mass(nb_star, n_bins)

        # Make the plot
        print("Plotting...")
        make_plot(t_mean, integrated_mass, output_location, output_filename, image_format=image_format,
                      x_min=x_min, x_max = x_max, y_min = y_min, y_max = y_max,dpi=dpi, figsize=figsize)

    print("END-------------------------------------")
