#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Mar 25 10:59:51 2024

@author: darwin
"""

import os
from MaDarwinR.scripts_utils import RawTextArgumentDefaultsHelpFormatter, store_as_array
from MaDarwinR.analysis_functions import is_IMF_continuous_part
from MaDarwinR.plot_utils import  set_label
from pNbody import *
from pNbody import Nbody
import matplotlib.pyplot as plt
import numpy as np
import argparse

#Plot the redshift as title if one wants a title

#%%

def choose_y_axis(y_axis, nb_star):
    if y_axis == "mass":
        observable = "M"
        units = r"[M$_\odot$]"
        y_data = nb_star.Mass(units="Msol")
    elif y_axis == "Fe/H":
        observable = "[Fe/H]"
        units = ""
        y_data = nb_star.AbRatio("Fe", "H")
    elif y_axis == "Ca/Fe":
        observable = "[Ca/Fe]"
        units = ""
        y_data = nb_star.CaFe()
    elif y_axis == "Mg/Fe":
        observable = "[Mg/Fe]"
        units = ""
        y_data = nb_star.MgFe()
    elif y_axis == "Z" or x_axis == "metallicity":
        observable = "Metallicity"
        units = ""
        y_data = nb_star.MetalsH()
    else:
        raise ValueError("Observable {} not yet implemented".format(y_axis))

    return y_data, observable, units

def make_plot(nb, x_axis, y_axis, output_location, output_filename,**kwargs):
    title = kwargs.get("title", None)
    # cmap = kwargs.get("cmap", "viridis")
    dpi = kwargs.get("dpi", 300)
    image_format = kwargs.get("image_format", "png")
    figsize = kwargs.get("figsize", (6.4, 4.8))
    # tight_layout = kwargs.get("tight_layout", False)
    log = kwargs.get("log", True)
    x_min = kwargs.get("x_min", None)
    x_max = kwargs.get("x_max", None)
    y_min = kwargs.get("y_min", None)
    y_max = kwargs.get("y_max", None)

    x_data = nb.num
    x_observable = "Particle ID"
    x_units = ""
    y_data, y_observable, y_units = choose_y_axis(y_axis, nb)

    if log == "x" or log == "xy":
        log_x = True
    else:
        log_x = False

    if log == "y" or log == "xy":
        log_y = True
    else:
        log_y = False

    # label_x = "Particle ID"
    label_x = set_label(False, x_observable, x_units)
    label_y = set_label(log_y, y_observable, y_units)

    fig, ax = plt.subplots(num=1, ncols=1, nrows=1, layout="tight", figsize=figsize)
    if title is not None:
        fig.suptitle(title, fontsize=20)
    ax.cla()
    ax.plot(x_data, y_data, linestyle="None", marker=".", color="tomato")
    ax.set_xlabel(label_x)
    ax.set_ylabel(label_y)

    #Set the axis limits
    x_left, x_right = ax.get_xlim()
    y_left, y_right = ax.get_ylim()
    if x_min is None:
        x_min = x_left
    if x_max is None:
        x_max = x_right
    if y_min is None:
        y_min = y_left
    if y_max is None:
        y_max =y_right

    ax.set_xlim([x_min, x_max])
    ax.set_ylim([y_min, y_max])

    if log_x:
        ax.set_xscale("log")
    if log_y:
        ax.set_yscale("log")

    #Save the plot
    plt.savefig(output_location + output_filename + ".png", format=image_format, bbox_inches='tight', dpi=dpi)
    plt.close()

    #Save the data
    np.savez_compressed(output_location+output_filename, x_observable=x_data,y_observable=y_data)


#%% Parse options

def parse_option():
    description = """"
Plot the chemistry properties of gas particle as a function of their ids. It
helps showing the evolution of these properties for particles
"""
    epilog = """
Examples:
--------
madr_plot_particle_chemistry.py --y_axis "Mg/Fe" snapshot_0010.hdf5
"""
    parser = argparse.ArgumentParser(description=description, epilog=epilog,
                                     formatter_class=RawTextArgumentDefaultsHelpFormatter)

    parser.add_argument("files",
                        nargs="+",
                        type=str,
                        help="File name(s).")

    parser.add_argument("--output_location",
                        action="store",
                        type=str,
                        dest="output_location",
                        default=None,
                        help="Name of the output location")

    parser.add_argument("-o",
                        action="store",
                        type=str,
                        dest="output_filename",
                        default=None,
                        help="Name of the output file. Use it if you only give ONE input file. Otherwise, "
                        "all files will have the same name.")

    parser.add_argument("-t",
                        action="store",
                        type=str,
                        dest="ftype",
                        default='swift',
                        help="Type of the output file")

    parser.add_argument("--x_axis",
                        action="store",
                        choices= [ "mass", "Fe/H", "Ca/Fe", "Mg/Fe", "Z", "metallicity"],
                        type=str,
                        default="Mass",
                        help="Observable on the x-axis.")

    parser.add_argument("--y_axis",
                        action="store",
                        choices= [ "mass", "Fe/H", "Ca/Fe", "Mg/Fe", "Z", "metallicity"],
                        type=str,
                        default="Fe/H",
                        help="Observable on y-axis.")

    parser.add_argument("--log",
                        action="store",
                        type=str,
                        choices=["none", "x", "y", "xy"],
                        default="none",
                        help="Put the selected axis in log-scale.")

    parser.add_argument("--select_ids",
                        action="store",
                        type=str,
                        default=None,
                        help="Give a file with a list of ids to select.")

    #Plot parameters
    parser.add_argument("--image_format",
                        action="store",
                        type=str,
                        default="png",
                        help="Format of the output image file. This must be recognized by Matplotlib.")

    parser.add_argument("--dpi",
                        action="store",
                        type=float,
                        default=300,
                        help="Output image resolution.")

    parser.add_argument("--x_min",
                        action="store",
                        type=float,
                        default=None,
                        help="Minimal x-axis value.")

    parser.add_argument("--x_max",
                        action="store",
                        type=float,
                        default=None,
                        help="Maximal x-axis value.")

    parser.add_argument("--y_min",
                        action="store",
                        type=float,
                        default=None,
                        help="Minimal y-axis value.")

    parser.add_argument("--y_max",
                        action="store",
                        type=float,
                        default=None,
                        help="Maximal y-axis value.")

    parser.add_argument("--figsize",
                        action=store_as_array,
                        nargs=2,
                        type=float,
                        default=np.array([6.4,4.8]),
                        help="Size of the figures (width, height)")

    parser.add_argument("--stylesheet",
                        action="store",
                        type=str,
                        default=None,
                        help="Matplotlib stylesheet.")


    parser.parse_args()
    args = parser.parse_args()
    files = args.files

    for f in files:
        if not os.path.exists(f):
            raise FileNotFoundError("You need to provide one file")

    return args, files#%%


# %% Main

if __name__ == "__main__":
    # Parse the input
    opt, files = parse_option()

    print("\n-------------------------------------")
    print("Welcome to Particle IDs chemistry script !\n")

    output_location = opt.output_location
    output_filename = opt.output_filename
    x_axis = opt.x_axis
    y_axis = opt.y_axis
    log = opt.log
    x_min = opt.x_min
    x_max = opt.x_max
    y_min = opt.y_min
    y_max = opt.y_max
    image_format = opt.image_format
    dpi = opt.dpi
    figsize = opt.figsize
    stylesheet = opt.stylesheet
    ids_file = opt.select_ids

    if stylesheet is not None:
        plt.style.use(stylesheet)

    # If no output, exit
    if output_location is None:
        output_location = ""
    else:
        if not (os.path.exists(output_location)):
            print("Output directory does not exist. It will be created.")
            os.makedirs(output_location)

        # Adds the "/" at the end if needed
        if (output_location[-1] != "/"):
            output_location = output_location+"/"

    for filename in files:

        # If we do not give output_filename
        # Notice that I keep opt.output_filename, otherwise this condition is not fullfilled for the next
        # iteration
        if opt.output_filename is None:
            # Keeps the base name (without /path/to/file) and without extension .hdf5
            output_filename = os.path.split(filename)[1]
            output_filename = os.path.splitext(output_filename)[0]

        # Select the object
        print("Loading the data...")

        if ids_file is not None:
            ids = np.loadtxt(ids_file, dtype=np.int64)
            nb = Nbody(p_name=filename, num = ids_file)
        else:
            nb = Nbody(p_name=filename, ptypes=[0])


        print("Plotting...")
        title = r"$z=" + "{:.2f}".format(nb.redshift) + "$"
        make_plot(nb, x_axis, y_axis, output_location, output_filename, image_format=image_format, figsize=figsize, log=log, x_min=x_min, x_max=x_max, y_min=y_min, y_max=y_max, dpi=dpi, title=title)



    print("END-------------------------------------")
