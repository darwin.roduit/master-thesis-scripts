#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Apr 25 13:03:33 2024

@author: darwinr
"""

import os
import argparse
import numpy as np
import matplotlib.pyplot as plt
from pNbody import Nbody
from pNbody import *
from tqdm import tqdm
from MaDarwinR.scripts_utils import RawTextArgumentDefaultsHelpFormatter, store_as_array

from astropy import units as u
from astropy.constants import G as G_a
from astropy.constants import m_p

#%%
def getr200(r, nb1, G, Hubble, Omega_0):
    """This function is used by a root finding algorithm to find r_200"""
    rho_crit =  3 * Hubble * Hubble / (8 * np.pi * G);
    nbs = nb1.selectc(nb1.rxyz()<r)
    r_2 = nbs.rxyz()
    r_2 = np.max(r_2)
    M = np.sum(nbs.mass)
    V = 4/3.*np.pi*r_2**3
    return (M/V - 200*rho_crit*Omega_0)

def get_r_200(nb_dm, G, Hubble):
    """Determine r_200 for a halo. The nb_dm object must be centered on the halo of interest."""
    r = nb_dm.rxyz()
    r_max = np.max(r)
    r_min =1.01 * np.min(r)
    Omega_0 = nb_dm.omega0
    r_200 = bisect(getr200, r_min, r_max, args = (nb_dm, G, Hubble, Omega_0), xtol = 1e-4, maxiter = 400)
    return r_200

def get_M_200(nb_dm, r_200):
    nb_dm_r200 = nb_dm.selectc(nb_dm.rxyz() < r_200)
    M_200 = nb_dm_r200.TotalMass()
    return M_200

def get_baryon_fraction(mass_matter, mass_DM):
    return mass_matter/(mass_DM + mass_matter)

def get_star_mass(nb_star, maximal_distance, star_BH_mass_limit):
    nb_star_frac = nb_star.selectc(nb_star.rxyz() < maximal_distance)
    nb_star_frac = nb_star.selectc(nb_star_frac.Mass() < star_BH_mass_limit)
    M_star = nb_star_frac.TotalMass()
    return M_star

def v_circ(nb, G, unit_length, unit_mass, unit_time, unit_velocity):
    pos = nb.pos*unit_length
    r = np.linalg.norm(pos, axis=1)
    M = np.cumsum(nb.mass*unit_mass);
    G = G.to(unit_length**3/(unit_mass * unit_time**2))
    v_circ = np.sqrt(G * M / r).to(unit_velocity)
    return v_circ

def get_softening_length(epsilon_comoving, max_epsilon_physical, scale_factor):
    return min(epsilon_comoving, max_epsilon_physical/scale_factor)



def compute_dm_properties(filename):
    """Compute the properties of the DM halo"""
    #Load DM only
    nb_dm = Nbody(p_name=filename, ptypes=[1])

    #Get the units from the object
    UnitLength_in_cm = nb_dm.UnitLength_in_cm[0]*u.cm
    UnitMass_in_g = nb_dm.UnitMass_in_g[0]*u.g
    UnitVelocity_in_cm_per_s = nb_dm.UnitVelocity_in_cm_per_s*u.cm/u.s
    Unit_time_in_cgs = nb_dm.Unit_time_in_cgs[0]*u.s

    #Convert to kpc, M_sun km/s and Gyr
    unit_length = UnitLength_in_cm.to(u.kpc)
    unit_mass = UnitMass_in_g.to(u.M_sun)
    unit_velocity = UnitVelocity_in_cm_per_s.to(u.km/u.s)
    unit_time = Unit_time_in_cgs.to(u.Gyr)
    unit_atom_per_cm_3 = m_p.to(u.g)/u.cm**3


    #Parameters
    comoving_DM_softening =  0.1427*unit_length
    max_physical_DM_softening = 0.037*unit_length
    maximal_radius = 50
    hist_radius = 150
    figsize = (6.4, 4.8)
    bins_r = 50
    r_200_frac = 0.1 #used to select the stars within r_200*r_200_frac
    star_BH_mass_limit = 40*u.M_sun #Limit between stars regime and FSNe/PISNe


    #Center at the maximum density. It should correspond to the main UFD halo
    #Do it twice to be better centered.
    center = nb_dm.get_histocenter(rbox=hist_radius, nb=500)
    nb_dm.translate(-center)
    center += nb_dm.get_histocenter(rbox=hist_radius/10, nb=500)
    nb_dm.translate(-center)

    #Cosmological parameters
    Hubble = 0.1
    G  = G_a.to(unit_length**3/(unit_mass * unit_time**2)).value
    Omega_0 = nb_dm.omega0

    r_200 = get_r_200(nb_dm, G, Hubble)*unit_length
    M_200 = get_M_200(nb_dm, r_200.value)*unit_mass
    print('r_200 = {:e}'.format(r_200))
    print('M_200 = {:e}'.format(M_200))


# %% Parser options

def parse_option():
    description = """"
    """
    epilog = """
Examples:
--------

"""
    parser = argparse.ArgumentParser(description=description, epilog=epilog,
                                     formatter_class=RawTextArgumentDefaultsHelpFormatter)

    parser.add_argument("files",
                        nargs="+",
                        type=str,
                        help="File name(s).")

    parser.add_argument("--output_location",
                        action="store",
                        type=str,
                        dest="output_location",
                        default=None,
                        help="Name of the output location")

    parser.add_argument("-o",
                        action="store",
                        type=str,
                        dest="output_filename",
                        default=None,
                        help="Name of the output file. Use it if you only give ONE input file. Otherwise, "
                        "all files will have the same name.")

    parser.add_argument("-t",
                        action="store",
                        type=str,
                        dest="ftype",
                        default='swift',
                        help="Type of the output file")

    parser.add_argument("--log_cmap",
                        action="store_true",
                        help="Put the z map in log-scale.")

    parser.add_argument("--n_bins",
                        action="store",
                        type=int,
                        default=100,
                        help="Number of bins for the histogram IMF.")

    parser.add_argument("--particle_type",
                        action="store",
                        choices= ["no_DM", "DM", "stars", "gas"],
                        type=str,
                        default="no_DM",
                        help="Type of particles to compute the quantities.")

    parser.add_argument("--use_binned_statistic",
                        action="store_true",
                        help="Use scipy.stats.binned_statistic instead of numpy histogram (in 2D). You can "
                                  "specify the statistic with the option '--statistic'.")

    parser.add_argument("--statistic",
                        action="store",
                        choices= ["mean", "std", "median", "count", "sum", "min", "max"],
                        type=str,
                        default="mean",
                        help="Statistic to use with scipy.stats.binned_statistic_2D.")

    #Plot options
    parser.add_argument("--x_min",
                        action="store",
                        type=float,
                        default=None,
                        help="Minimal x-axis value (physical axis, not matplotlib).")

    parser.add_argument("--x_max",
                        action="store",
                        type=float,
                        default=None,
                        help="Maximal x-axis value (physical axis, not matplotlib).")

    parser.add_argument("--y_min",
                        action="store",
                        type=float,
                        default=None,
                        help="Minimal y-axis value (physical axis, not matplotlib).")

    parser.add_argument("--y_max",
                        action="store",
                        type=float,
                        default=None,
                        help="Maximal y-axis value. (physical axis, not matplotlib)")

    parser.add_argument("--z_min",
                        action="store",
                        type=float,
                        default=None,
                        help="Minimal z-axis value (physical axis, not matplotlib).")

    parser.add_argument("--z_max",
                        action="store",
                        type=float,
                        default=None,
                        help="Maximal z-axis value (physical axis, not matplotlib).")

    parser.add_argument("--cmap_min",
                        action="store",
                        type=float,
                        default=None,
                        help="Minimal colormap value.")

    parser.add_argument("--cmap_max",
                        action="store",
                        type=float,
                        default=None,
                        help="Maximal colormap value.")


    parser.parse_args()
    args = parser.parse_args()
    files = args.files

    for f in files:
        if not os.path.exists(f):
            raise FileNotFoundError("You need to provide one file")

    return args, files

# %% Main

if __name__ == "__main__":
    # Parse the input
    opt, files = parse_option()

    print("\n-------------------------------------")
    print("Welcome to Plot Observable script !\n")

    output_location = opt.output_location
    output_filename = opt.output_filename
    n_bins = opt.n_bins


    # If no output, exit
    if output_location is None:
        output_location = ""
    else:
        if not (os.path.exists(output_location)):
            print("Output directory does not exist. It will be created.")
            os.makedirs(output_location)

        # Adds the "/" at the end if needed
        if (output_location[-1] != "/"):
            output_location = output_location+"/"

    for filename in tqdm(files):

        # If we do not give output_filename
        # Notice that I keep opt.output_filename, otherwise this condition is not fullfilled for the next
        # iteration
        if opt.output_filename is None:
            # Keeps the base name (without /path/to/file) and without extension .hdf5
            output_filename = os.path.split(filename)[1]
            output_filename = os.path.splitext(output_filename)[0]

        #Compute DM properties

        # make_plot()

    print("END-------------------------------------")
