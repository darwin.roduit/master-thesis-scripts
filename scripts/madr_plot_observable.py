#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Mar 12 11:33:42 2024

@author: darwin
"""
import os
import argparse
import numpy as np
import matplotlib.pyplot as plt
from pNbody import Nbody
from pNbody import *
from scipy.stats import binned_statistic_2d
from mpl_toolkits.axes_grid1 import make_axes_locatable
from tqdm import tqdm
from MaDarwinR.scripts_utils import RawTextArgumentDefaultsHelpFormatter, store_as_array

#Ignore division errors and log(0) errors
np.seterr(divide='ignore')

# plt.style.use(SCRIPT_DIR + "/"  + 'lab_style_figure.mplstyle')

# %%

def choose_particle_type(particle_type, filename):
    if part_type =="DM":
        nb_plot = Nbody(p_name=filename, ptypes=[1]) #Keep only DM and not the boundary DM
        if nb_plot.npart[1] == 0:
            print("No DM particles found in this file.")
    elif part_type =="stars":
        nb_plot = Nbody(p_name=filename, ptypes=[4])
        if nb_plot.npart[4] == 0:
            print("No stars found in this file. ")
    elif part_type =="no_DM":
        nb_plot = Nbody(p_name=filename, ptypes=[0, 3, 4]) #Remove DM and boundary DM
        if nb_plot.npart[0] == 0 and nb_plot.npart[4] == 0:
            print("No gas or star particles found in this file. ")
    elif part_type == "gas":
        nb_plot = Nbody(p_name=filename, ptypes=[0])
        if nb_plot.npart[0] == 0:
            print("No gas particles found in this file. ")
    else:
        raise ValueError("This type of particle is not yet implemented")
    return nb_plot

def choose_z_map(z_map, nb_plot):
    if z_map == "T":
        observable = "T"
        units = "[K]"
        c_array = nb_plot.T()
    elif z_map == "rho" or z_map == "density":
        observable = r"$\rho$"
        units = r"[atom/cm$^3$]"
        c_array = nb_plot.Rho(units="acc")
    elif z_map == "metallicity" or z_map == "Z":
        observable = "Metallicity"
        units = ""
        c_array = nb_plot.MetalsH()
    elif z_map == "mass":
        observable = "M"
        units = r"[M$_\odot$]"
        c_array = nb_plot.Mass(units="Msol")
    elif z_map == "stellarAge":
        observable = "Age"
        units = r"[Gyr]"
        c_array = nb_plot.StellarAge(units="Gyr")
    elif z_map == "XHI":
        observable = "XHI"
        units = ""
        c_array = nb_plot.XHI*nb_plot.Mass(units="Msol")
    elif z_map == "XHII":
        observable = "XHII"
        units = ""
        c_array = nb_plot.XHII
    elif z_map == "XH2I":
        observable = "XH2I"
        units = ""
        c_array = nb_plot.XH2I*nb_plot.Mass(units="Msol")
    elif z_map == "XH2II":
        observable = "XH2II"
        units = ""
        c_array = nb_plot.XH2II*nb_plot.Mass(units="Msol")
    elif z_map == "XDI":
        observable = "XDI"
        units = ""
        c_array = nb_plot.XDI*nb_plot.Mass(units="Msol")
    elif z_map == "XDII":
        observable = "XDII"
        units = ""
        c_array = nb_plot.XDII*nb_plot.Mass(units="Msol")
    elif z_map == "XHDI":
        observable = "XHDI"
        units = ""
        c_array = nb_plot.XHDI*nb_plot.Mass(units="Msol")
    elif z_map == "XHM":
        observable = "XHM"
        units = ""
        c_array = nb_plot.XHM*nb_plot.Mass(units="Msol")
    elif z_map == "XHeI":
        observable = "XHeI"
        units = ""
        c_array = nb_plot.XHeI*nb_plot.Mass(units="Msol")
    elif z_map == "XHeII":
        observable = "XHeII"
        units = ""
        c_array = nb_plot.XHeII*nb_plot.Mass(units="Msol")
    elif z_map == "XHeIII":
        observable = "XHeIII"
        units = ""
        c_array = nb_plot.XHeIII*nb_plot.Mass(units="Msol")
    elif z_map == "Xe":
        observable = "Xe"
        units = ""
        c_array = nb_plot.Xe*nb_plot.Mass(units="Msol")
    return c_array, observable, units

def clean_z_map_nan_and_inf(c_array, x, y, z):
    mask_nan = np.logical_not(np.isnan(c_array))
    c_array = c_array[mask_nan] ; x = x[mask_nan] ; y = y[mask_nan] ; z = z[mask_nan]

    mask_inf = np.isfinite(c_array)
    c_array = c_array[mask_inf] ; x = x[mask_inf] ; y = y[mask_inf] ; z = z[mask_inf]
    return c_array, x, y, z

def set_label(log_cmap, observable, units):
    if log_cmap:
        label = r"$\log_{10}$"
    else:
        label =""

    label += r"{}".format(observable) + " " + units
    return label

def make_plot(nb_plot, z_map , output_location, output_filename, **kwargs):
    use_binned_statistic = kwargs.get("use_binned_statistic", False)
    statistic = kwargs.get("statistic", "mean")
    title = kwargs.get("title", None)
    cmap = kwargs.get("cmap", "viridis")
    dpi = kwargs.get("dpi", 300)
    image_format = kwargs.get("image_format", "png")
    figsize = kwargs.get("figsize", (5, 10))
    tight_layout = kwargs.get("tight_layout", False)
    log_cmap = kwargs.get("log_cmap", True)
    x_min = kwargs.get("x_min", None)
    x_max = kwargs.get("x_max", None)
    y_min = kwargs.get("y_min", None)
    y_max = kwargs.get("y_max", None)
    z_min = kwargs.get("z_min", None)
    z_max = kwargs.get("z_max", None)
    cmap_min = kwargs.get("cmap_min", None)
    cmap_max = kwargs.get("cmap_max", None)
    bins_x = kwargs.get("bins_x", None)
    bins_y = kwargs.get("bins_y", None)
    bins_z= kwargs.get("bins_z", None)

    #Select the z axis for the colormap
    c_array, observable, units = choose_z_map(z_map, nb_plot)

    #Take the positions of the particles
    pos = nb_plot.Pos(units="pc")
    x = pos[: , 0] ; y = pos[: , 1] ; z = pos[: , 2]

    #Remove Nan, inf and -inf
    c_array, x, y, z = clean_z_map_nan_and_inf(c_array, x, y, z)

    #Set the colorbar label
    label = set_label(log_cmap, observable, units)

    #Plot
    if tight_layout:
        fig, ax = plt.subplots(num=3, nrows=3, ncols=1, figsize=figsize, layout="tight")
    else:
        fig, ax = plt.subplots(num=3, nrows=3, ncols=1, figsize=figsize)

    if title is not None:
        fig.suptitle(title, fontsize=26)


    #Figure 1--------------------------------------------------
    if not use_binned_statistic:
        val, x_edges, y_edges = np.histogram2d(x, y, weights=c_array, bins=(bins_x, bins_y), density=False)
    else:
        val, x_edges, y_edges, binnumber = binned_statistic_2d(x, y, c_array, statistic=statistic, bins=(bins_x, bins_y))

    val = val.transpose()

    #Use logscale or not for cmap?
    if log_cmap:
        val = np.log10(val)

    im = ax[0].imshow(val, interpolation='none', origin='lower', extent=[x_edges[0], x_edges[-1], y_edges[0], y_edges[-1]],
                      aspect='equal', zorder = 10, cmap = cmap, vmin=cmap_min, vmax=cmap_max) #or rainbow
    ax[0].set_xlabel("$x$ [cpc]", labelpad=0)
    ax[0].set_ylabel("$y$ [cpc]", labelpad=0)
    divider = make_axes_locatable(ax[0])
    cax = divider.append_axes('right', size='5%', pad=0.05)
    fig.colorbar(im, cax=cax, orientation='vertical', label=label)

    if x_min is None:
        x_min = x_edges[0]
    if x_max is None:
        x_max = x_edges[-1]
    if y_min is None:
        y_min = y_edges[0]
    if y_max is None:
        y_max = y_edges[-1]

    ax[0].set_xlim([x_min, x_max])
    ax[0].set_ylim([y_min, y_max])

    #Figure 2--------------------------------------------------
    if not use_binned_statistic:
        val, x_edges, z_edges = np.histogram2d(x, z, weights=c_array, bins=(bins_x, bins_z), density=False)
    else:
        val, x_edges, z_edges, binnumber = binned_statistic_2d(x, z, c_array, statistic=statistic, bins=(bins_x, bins_z))

    val = val.transpose()

    if log_cmap:
        val = np.log10(val)

    im = ax[1].imshow(val, interpolation='none', origin='lower', extent=[x_edges[0], x_edges[-1], z_edges[0], z_edges[-1]],
                      aspect='equal', zorder = 10, cmap = cmap, vmin=cmap_min, vmax=cmap_max) #or rainbow
    ax[1].set_xlabel("$x$ [cpc]", labelpad=0)
    ax[1].set_ylabel("$z$ [cpc]", labelpad=0)
    divider = make_axes_locatable(ax[1])
    cax = divider.append_axes('right', size='5%', pad=0.05)
    fig.colorbar(im, cax=cax, orientation='vertical', label=label)

    if x_min is None:
        x_min = x_edges[0]
    if x_max is None:
        x_max = x_edges[-1]
    if z_min is None:
        z_min = z_edges[0]
    if z_max is None:
        z_max = z_edges[-1]

    ax[1].set_xlim([x_min, x_max])
    ax[1].set_ylim([z_min, z_max])

    #Figure 3--------------------------------------------------
    if not use_binned_statistic:
        val, y_edges, z_edges = np.histogram2d(y, z, weights=c_array, bins=(bins_y, bins_z), density=False)
    else:
        val, y_edges, z_edges, binnumber = binned_statistic_2d(y, z, c_array, statistic=statistic, bins=(bins_y, bins_z))

    val = val.transpose()

    if log_cmap:
        val = np.log10(val)

    im = ax[2].imshow(val, interpolation='none', origin='lower', extent=[y_edges[0], y_edges[-1], z_edges[0], z_edges[-1]],
                      aspect='equal', zorder = 10, cmap = cmap, vmin=cmap_min, vmax=cmap_max) #or rainbow
    ax[2].set_xlabel("$y$ [cpc]", labelpad=0)
    ax[2].set_ylabel("$z$ [cpc]", labelpad=0)
    divider = make_axes_locatable(ax[2])
    cax = divider.append_axes('right', size='5%', pad=0.05)
    fig.colorbar(im, cax=cax, orientation='vertical', label=label)

    if y_min is None:
        y_min = y_edges[0]
    if y_max is None:
        y_max = y_edges[-1]
    if z_min is None:
        z_min = z_edges[0]
    if z_max is None:
        z_max = z_edges[-1]

    ax[2].set_xlim([y_min, y_max])
    ax[2].set_ylim([z_min, z_max])
    #----------------------------------------------------------------------------

    #Save the plot
    plt.savefig(output_location + output_filename + "_" + z_map + "."+str(image_format), format=image_format, bbox_inches='tight', dpi=dpi)
    plt.close()

    return ax


# %% Parser options

def parse_option():
    description = """"
Plot the observables such as temperature, density, metallicity, etc in comoving space of the inputs.
    """
    epilog = """
Examples:
--------
madr_plot_observable.py --z_map "T" --log_cmap --n_bins 100 --particle_type "no_DM" snap_0020_UFD.hdf5 -o snap_0020
madr_plot_observable.py --z_map "rho" --log_cmap --n_bins 100 --particle_type "no_DM" snap_0020_UFD.hdf5 -o snap_0020 --cmap="viridis"
madr_plot_observable.py --z_map "mass" --n_bins 100 --particle_type "DM" snap_0020_UFD.hdf5
madr_plot_observable.py --z_map "mass" --n_bins 100 --particle_type "no_DM" --output_location "test" --cmap="terrain" snap/*
madr_plot_observable.py --z_map "mass" --n_bins 100 --particle_type "no_DM" --output_location "test" --cmap="terrain" snap/* --figsize 10 10
madr_plot_observable.py --z_map "stellarAge" --n_bins 500 --particle_type "stars" snap_0020_UFD.hdf5 -o snap_0020 --cmap="magma" --x_min=-500 --x_max=500 --y_min=-500 --z_min=-500 --z_max=500
"""
    parser = argparse.ArgumentParser(description=description, epilog=epilog,
                                     formatter_class=RawTextArgumentDefaultsHelpFormatter)

    parser.add_argument("files",
                        nargs="+",
                        type=str,
                        help="File name(s).")

    parser.add_argument("--output_location",
                        action="store",
                        type=str,
                        dest="output_location",
                        default=None,
                        help="Name of the output location")

    parser.add_argument("-o",
                        action="store",
                        type=str,
                        dest="output_filename",
                        default=None,
                        help="Name of the output file. Use it if you only give ONE input file. Otherwise, "
                        "all files will have the same name.")

    parser.add_argument("-t",
                        action="store",
                        type=str,
                        dest="ftype",
                        default='swift',
                        help="Type of the output file")

    parser.add_argument("--z_map",
                        action="store",
                        choices= [ "T", "rho", "density", "metallicity", "Z", "mass", "stellarAge", "XHI", "XHII", "XH2I", "XH2II", "XDI", "XDII", "XHDI", "XHM", "XHeI", "XHeII", "XHeIII", "Xe"],
                        type=str,
                        default="T",
                        help="Observable on which to display the map.")

    parser.add_argument("--log_cmap",
                        action="store_true",
                        help="Put the z map in log-scale.")

    parser.add_argument("--n_bins",
                        action="store",
                        type=int,
                        default=100,
                        help="Number of bins for the histogram IMF.")

    parser.add_argument("--particle_type",
                        action="store",
                        choices= ["no_DM", "DM", "stars", "gas"],
                        type=str,
                        default="no_DM",
                        help="Type of particles to compute the quantities.")

    parser.add_argument("--use_binned_statistic",
                        action="store_true",
                        help="Use scipy.stats.binned_statistic instead of numpy histogram (in 2D). You can "
                                  "specify the statistic with the option '--statistic'.")

    parser.add_argument("--statistic",
                        action="store",
                        choices= ["mean", "std", "median", "count", "sum", "min", "max"],
                        type=str,
                        default="mean",
                        help="Statistic to use with scipy.stats.binned_statistic_2D.")

    #Plot options
    parser.add_argument("--x_min",
                        action="store",
                        type=float,
                        default=None,
                        help="Minimal x-axis value (physical axis, not matplotlib).")

    parser.add_argument("--x_max",
                        action="store",
                        type=float,
                        default=None,
                        help="Maximal x-axis value (physical axis, not matplotlib).")

    parser.add_argument("--y_min",
                        action="store",
                        type=float,
                        default=None,
                        help="Minimal y-axis value (physical axis, not matplotlib).")

    parser.add_argument("--y_max",
                        action="store",
                        type=float,
                        default=None,
                        help="Maximal y-axis value. (physical axis, not matplotlib)")

    parser.add_argument("--z_min",
                        action="store",
                        type=float,
                        default=None,
                        help="Minimal z-axis value (physical axis, not matplotlib).")

    parser.add_argument("--z_max",
                        action="store",
                        type=float,
                        default=None,
                        help="Maximal z-axis value (physical axis, not matplotlib).")

    parser.add_argument("--cmap_min",
                        action="store",
                        type=float,
                        default=None,
                        help="Minimal colormap value.")

    parser.add_argument("--cmap_max",
                        action="store",
                        type=float,
                        default=None,
                        help="Maximal colormap value.")

    parser.add_argument("--image_format",
                        action="store",
                        type=str,
                        default="png",
                        help="Format of the output image file. This must be recognized by Matplotlib.")

    parser.add_argument("--dpi",
                        action="store",
                        type=float,
                        default=300,
                        help="Output image resolution.")

    parser.add_argument("--cmap",
                        action="store",
                        type=str,
                        default="magma",
                        help="Color map. It must be Matplolib compliant.")

    parser.add_argument("--figsize",
                        action=store_as_array,
                        nargs=2,
                        type=float,
                        default=np.array([5,10]),
                        help="Size of the figures (width, height)")

    parser.add_argument("--tight_layout",
                        action="store_true",
                        help="Use plt.tight_layout().")

    parser.parse_args()
    args = parser.parse_args()
    files = args.files

    for f in files:
        if not os.path.exists(f):
            raise FileNotFoundError("You need to provide one file")

    return args, files

# %% Main

if __name__ == "__main__":
    # Parse the input
    opt, files = parse_option()

    print("\n-------------------------------------")
    print("Welcome to Plot Observable script !\n")

    output_location = opt.output_location
    output_filename = opt.output_filename
    n_bins = opt.n_bins
    image_format = opt.image_format
    dpi = opt.dpi

    use_binned_statistic = opt.use_binned_statistic
    statistic = opt.statistic

    z_map = opt.z_map
    log_cmap = opt.log_cmap
    part_type = opt.particle_type
    bins_x = n_bins ; bins_y = bins_x ; bins_z = bins_x
    cmap = opt.cmap
    tight_layout = opt.tight_layout
    figsize = opt.figsize
    x_min = opt.x_min
    x_max = opt.x_max
    y_min = opt.y_min
    y_max = opt.y_max
    z_min = opt.z_min
    z_max = opt.z_max
    cmap_min = opt.cmap_min
    cmap_max = opt.cmap_max

    # If no output, exit
    if output_location is None:
        output_location = ""
    else:
        if not (os.path.exists(output_location)):
            print("Output directory does not exist. It will be created.")
            os.makedirs(output_location)

        # Adds the "/" at the end if needed
        if (output_location[-1] != "/"):
            output_location = output_location+"/"

    for filename in tqdm(files):

        # If we do not give output_filename
        # Notice that I keep opt.output_filename, otherwise this condition is not fullfilled for the next
        # iteration
        if opt.output_filename is None:
            # Keeps the base name (without /path/to/file) and without extension .hdf5
            output_filename = os.path.split(filename)[1]
            output_filename = os.path.splitext(output_filename)[0]

        # Load the data
        # print("Loading the data...")
        # Select your particle type
        nb_plot = choose_particle_type(part_type, filename)

        if np.sum(nb_plot.npart) == 0:
            print("No particle found in this file. Continue to the next file...")
            continue

        # Make plot
        title = r"$z=" + "{:.2f}".format(nb_plot.redshift) + "$"
        make_plot(nb_plot, z_map, output_location, output_filename, cmap=cmap, image_format=image_format,
                  figsize=figsize, tight_layout=tight_layout, log_cmap=log_cmap, x_min=x_min, x_max=x_max,
                  y_min=y_min, y_max=y_max, z_min=z_min, z_max=z_max, bins_x=bins_x, bins_y=bins_y,
                  bins_z=bins_z, dpi=dpi, use_binned_statistic=use_binned_statistic, statistic=statistic, title=title,
                  cmap_min=cmap_min, cmap_max=cmap_max)

    print("END-------------------------------------")
