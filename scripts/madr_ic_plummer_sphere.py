# !/usr/bin/env python3
import h5py
from astropy import units as u
from astropy import constants as cte
from pNbody.DF import DistributionFunction
from pNbody.mass_models import plummer
from pNbody import *
import numpy as np
import argparse


####################################################################
# option parser
####################################################################

description = "Generate a plummer model at equilibrium with gas particles"
epilog = """
Examples:
--------
"""

parser = argparse.ArgumentParser(description=description, epilog=epilog,
                                 formatter_class=argparse.RawDescriptionHelpFormatter)


parser.add_argument("-o",
                    action="store",
                    type=str,
                    dest="outputfilename",
                    default=None,
                    help="Name of the output file")

parser.add_argument("-t",
                    action="store",
                    type=str,
                    dest="ftype",
                    default='swift',
                    help="Type of the output file")

parser.add_argument("--irand",
                    action="store",
                    dest="irand",
                    metavar='INT',
                    type=int,
                    default=0,
                    help='random seed')

parser.add_argument("-N",
                    action="store",
                    dest="N",
                    metavar='INT',
                    type=int,
                    default=1e5,
                    help='total number of particles')

parser.add_argument("--mass",
                    action="store",
                    type=float,
                    dest="mass",
                    default=None,
                    help="Particle mass in solar mass")

# Gas parameters
# parser.add_argument("--rho",
#                     action="store",
#                     dest="rho",
#                     type=float,
#                     default=0.1,
#                     help="Mean gas density in atom/cm3")

parser.add_argument("--u_int",
                    action="store",
                    dest="u_int",
                    type=float,
                    default=None,
                    help="Internal energy.")

parser.add_argument("--h",
                    action="store",
                    dest="h",
                    type=float,
                    default=None,
                    help="Gas smoothing length.")

# Boxsize parameter
parser.add_argument("--boxsize",
                    action="store",
                    type=float,
                    dest="boxsize",
                    default=None,
                    help="Size of the cubic box")

# Plummer parameters
parser.add_argument("--a",
                    action="store",
                    type=float,
                    dest="a",
                    default=1,
                    help="a parameter")

parser.add_argument("--Mtot",
                    action="store",
                    type=float,
                    dest="Mtot",
                    default=1,
                    help="Total mass in Msol")

# DF integration parameters
parser.add_argument("--Rmin",
                    action="store",
                    type=float,
                    dest="Rmin",
                    default=1e-2,
                    help="Rmin")

parser.add_argument("--Rmax",
                    action="store",
                    type=float,
                    dest="Rmax",
                    default=100,
                    help="Rmax")

parser.add_argument("--NE",
                    action="store",
                    type=float,
                    dest="NE",
                    default=1e4,
                    help="number of energy bins")

parser.add_argument("--NR",
                    action="store",
                    type=float,
                    dest="NR",
                    default=1e4,
                    help="number of radius bins")

parser.add_argument("--Ndraw",
                    action="store",
                    type=int,
                    dest="Ndraw",
                    default=1e6,
                    help="number of particles to draw at each loop")

parser.add_argument("--R_inf",
                    action="store",
                    type=float,
                    dest="R_inf",
                    default=np.inf,
                    help="Infinity value for integral computations.")

# Other
parser.add_argument("--plot",
                    action="store_true",
                    dest="plot",
                    default=False,
                    help="plot dynamical time")

########################################
# main
########################################

opt = parser.parse_args()
ptype = 0  # Gas


# define units
u_Length = 1 * u.kpc
u_Mass = 10**10 * u.M_sun
u_Velocity = 1 * u.km/u.s
u_Time = u_Length/u_Velocity
toMsol = u_Mass.to(u.M_sun).value

u_dict = {}
u_dict["UnitLength_in_cm"] = u_Length.to(u.cm).value
u_dict["UnitMass_in_g"] = u_Mass.to(u.g).value
u_dict["UnitVelocity_in_cm_per_s"] = u_Velocity.to(u.cm/u.s).value
u_dict["Unit_time_in_cgs"] = u_Time.to(u.s).value

# Define constants values
G = cte.G.to(u_Length**3/(u_Mass*u_Time**2)).value

# Do with solar masses unit to avoid computations problems...
opt.Mtot = opt.Mtot/toMsol

# For plummer sphere, we can provide the analytical functions


def fctDensity(x): return plummer.Density(M=opt.Mtot, a=opt.a, r=x, G=G)
def fctPotential(x): return plummer.Potential(M=opt.Mtot, a=opt.a, r=x, G=G)
def fctCumulativeMass(x): return plummer.CumulativeMass(
    M=opt.Mtot, a=opt.a, r=x, G=G)


def fctVcirc(x): return plummer.Vcirc(M=opt.Mtot, a=opt.a, r=x, G=G)


# Note : for plummer, this is equal to opt.Mtot
TotalMass = plummer.TotalMass(M=opt.Mtot, a=opt.a, G=G)

# Set the number of particles
if opt.mass is not None:
    # Do with solar masses unit to avoid computations problems...
    opt.mass = opt.mass/toMsol
    opt.N = int(TotalMass/opt.mass)


DF = DistributionFunction(Rmin=opt.Rmin, Rmax=opt.Rmax, Rinf=opt.R_inf, NR=opt.NR, NE=opt.NE,
                          fctDensity=fctDensity,
                          fctPotential=fctPotential,
                          fctCumulativeMass=fctCumulativeMass,
                          TotalMass=TotalMass, G=G)

DF.computeDF()
DF.clean()
DF.computeMaxLikelihood()
DF.sample(opt.N, opt.Ndraw, irand=opt.irand)

if opt.outputfilename:
    nb = Nbody(status='new', p_name=opt.outputfilename, pos=DF.pos,
               # vel=DF.vel, mass=DF.mass/toMsol, ftype=opt.ftype) #Convert masses to the right units
               vel=DF.vel, mass=DF.mass, ftype=opt.ftype)  # Convert masses to the right units

    # add units
    nb.UnitLength_in_cm = u_dict["UnitLength_in_cm"]
    nb.UnitMass_in_g = u_dict["UnitMass_in_g"]
    nb.UnitVelocity_in_cm_per_s = u_dict["UnitVelocity_in_cm_per_s"]
    nb.Unit_time_in_cgs = u_dict["Unit_time_in_cgs"]

    if opt.boxsize is None:
        r_max_part = np.max(nb.rxyz())
        print(
            "The maximal distance of the particle is {:.2e} kpc".format(r_max_part))
        nb.boxsize = 2.5*r_max_part
    else:
        nb.boxsize = opt.boxsize
    print("The boxsize is {} kpc".format(nb.boxsize))
    print("The shift to set in swift is : {}".format(nb.boxsize/2))

    nb.setComovingIntegrationOff()

    # particle type
    nb.set_tpe(ptype)

    # cvcenter
    nb.cvcenter()

    # write
    # nb.write()

    # # Now add the missing fields to the gas
    N = int(opt.N)
    L = nb.boxsize

    # Mean density
    rho = fctDensity(nb.rxyz())*u.M_sun/u.kpc**3
    rho = rho.to(u.M_p/u.cm**3)

    # Smoothing length
    h = opt.h
    if h is None:
        h = np.ones(N) * 3 * L / N ** (1.0 / 3.0)
    else:
        h = h*np.ones(N)

    print("Smoothing length : h = {}".format(h[0]))

    # Internal energy 2T + 2U + E_pot = 0 <=> U = -T - 0.5*E_pot
    if opt.u_int is None:
        e_kin = nb.ekin()
        print("e_kin = {}".format(e_kin))
        e_pot = nb.epot(0.1)
        print("e_pot = {}".format(e_pot))
        u_int = -e_kin - 0.5 * e_pot
    else:
        u_int = opt.u_int
    print("Viralized internal energy : u_int = {}".format(u_int))
    u_int_per_part = u_int/N * np.ones(N)

    # Write in file
    with h5py.File(opt.outputfilename, "w") as fileOutput:
        # Header
        grp = fileOutput.create_group("/Header")
        grp.attrs["BoxSize"] = [L, L, L]
        grp.attrs["NumPart_Total"] = [N, 0, 0, 0, 0, 0]
        grp.attrs["NumPart_Total_HighWord"] = [0, 0, 0, 0, 0, 0]
        grp.attrs["NumPart_ThisFile"] = [N, 0, 0, 0, 0, 0]
        grp.attrs["Time"] = 0.0
        grp.attrs["NumFileOutputsPerSnapshot"] = 1
        grp.attrs["MassTable"] = [0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
        grp.attrs["Flag_Entropy_ICs"] = [0, 0, 0, 0, 0, 0]
        grp.attrs["Dimension"] = 3

        # Units
        grp = fileOutput.create_group("/Units")
        grp.attrs["Unit length in cgs (U_L)"] = u_dict["UnitLength_in_cm"]
        grp.attrs["Unit mass in cgs (U_M)"] = u_dict["UnitMass_in_g"]
        grp.attrs["Unit time in cgs (U_t)"] = u_dict["Unit_time_in_cgs"]
        grp.attrs["Unit current in cgs (U_I)"] = 1
        grp.attrs["Unit temperature in cgs (U_T)"] = 1

        # Particle group
        grp = fileOutput.create_group("/PartType0")
        grp.create_dataset("Coordinates", data=nb.pos, dtype="d")
        grp.create_dataset("Velocities", data=nb.vel, dtype="f")
        grp.create_dataset("Masses", data=nb.mass, dtype="f")
        grp.create_dataset("SmoothingLength", data=h, dtype="f")
        grp.create_dataset("InternalEnergy", data=u_int_per_part, dtype="f")
        grp.create_dataset("ParticleIDs", data=nb.num, dtype="L")
        grp.create_dataset("Densities", data=rho, dtype="f")

print("END-----")

if opt.plot:
    import matplotlib.pyplot as plt
    # plot DynTime
    vcirc = fctVcirc(DF.R)
    T = 2*np.pi*DF.R/vcirc
    plt.plot(DF.R, T)
    plt.loglog()
    plt.show()
