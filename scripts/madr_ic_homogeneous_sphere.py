#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import h5py
from astropy import units as u
from astropy import constants as cte
from pNbody.DF import DistributionFunction
from pNbody.mass_models import plummer
from pNbody import *
from pNbody import ic
from pNbody.misc import scripts_utils

try:
    from pNbody.power_spectra import PowerLawPowerSpectrum
    from pNbody.power_spectra.perturbation import PerturbationPowerSpectrum
except:
    print('Experimental modules not found')
    pass
from copy import deepcopy
import numpy as np
import argparse

from scipy.stats import median_abs_deviation

import secrets

# import mpi4py as mpi
try:				# all this is useful to read files
    from mpi4py import MPI
except BaseException:
    MPI = None

from pNbody import mpiwrapper as mpi			# maybe we should send mpi instead of MPI

####################################################################
# Class
####################################################################


class HomogeneousSphere:
    """Sphere of constant density"""

    def __init__(self, R_s: float, M_tot: float = None, rho_0: float = None, G:
                 float = 1.0):
        """R_s : radius of the sphere
           M_tot : total mass of the sphere
           rho_0 : density of the sphere"""
        if M_tot is not None:
            self.M_tot = M_tot
            self.rho_0 = M_tot / (4.0/3.0 * np.pi * R_s**3)
        elif (M_tot is None) and (rho_0 is not None):
            self.rho_0 = rho_0
            self.M_tot = 4.0/3.0*np.pi*R_s**3*rho_0
        else:
            raise ValueError("You have to provide enough information to"
                             " generate the model")
        self.G = G
        self.R_s = R_s

        # Define vectorize version of functions to work with numpy arrays
        self.Density = np.vectorize(self.__Density)
        self.CumulativeMass = np.vectorize(self.__CumulativeMass)
        self.Potential = np.vectorize(self.__Potential)

    def __Density(self, r: float) -> float:
        if r > self.R_s:
            return 0.0
        else:
            return self.rho_0

    def __CumulativeMass(self, r: float) -> float:
        if r > self.R_s:
            return self.M_tot
        else:
            return 4.0/3.0*np.pi*self.rho_0*r**3

    def __Potential(self, r: float) -> float:
        if r > self.R_s:
            return -self.G*self.M_tot/r
        else:
            return 2*np.pi*self.G*self.rho_0*(self.R_s**2 - r**2)

    def FreeFallTime(self):
        return np.sqrt(3*np.pi/(32*self.G*self.rho_0))

    def generate_IC(self, n, r_s, seed=1, name='homosphere.dat', ftype='swift'):
        """
        Return an Nbody object that contains n particles distributed
        in an homogeneous triaxial sphere of axis a,b,c.
        """

        if isinstance(n, np.ndarray):
            random1 = n[:, 0]
            random2 = n[:, 1]
            random3 = n[:, 2]
            n = len(n)
            ntot = mpi.mpi_allreduce(n)
        else:

            rng = np.random.default_rng(seed=seed + mpi.mpi_Rank())

            # set random seed
            np.random.seed(seed + mpi.mpi_Rank())

            # set the number of particles per procs
            n, ntot = ic.get_local_n(n)

            random1 = rng.random([n])
            random2 = rng.random([n])
            random3 = rng.random([n])

        xm = (random1)**(1. / 3.)
        phi = random2 * np.pi * 2.
        costh = 1. - 2. * random3

        sinth = np.sqrt(1. - costh**2)
        axm = r_s * xm * sinth
        bxm = r_s * xm * sinth
        x = axm * np.cos(phi)
        y = bxm * np.sin(phi)
        z = r_s * xm * costh

        pos = np.transpose(np.array([x, y, z]))
        vel = np.ones([n, 3]) * 0.0
        # mass = np.ones([n]) * 1. / ntot
        mass = np.ones([n])*self.M_tot/ntot

        nb = Nbody(status='new', p_name=name, pos=pos,
                   vel=vel, mass=mass, ftype=ftype)

        return nb

    def info(self):
        print("Homogeneous Sphere information")
        print("------------------------------")
        print("  M_tot = {:e}".format(self.M_tot))
        print("  R_s = {:e}".format(self.R_s))
        print("  rho_0 = {:e}".format(self.rho_0))
        print("  t_ff = {:e}".format(self.FreeFallTime()))


####################################################################
# Functions
####################################################################

def apply_rotational_velocity_field(nb, Omega, axis):
    v_rotational = Omega*np.cross(axis, nb.pos)
    nb.vel = v_rotational
    return nb


def boss_bodenheimer_density(r, phi, rho_0, amplitude=0.5, m=2):
    return rho_0*(1+amplitude*np.cos(m*phi))


def apply_azimuthal_density_perturbation(nb, centre, amplitude, m_pert, n_element_tab=2048):
    """The perturbation of Boss and Bodenheimer density
                    rho(phi) = rho_0*(1 + amplitude*cos(m_pert*phi)
        reads in coordinates as
                    phi_prime = phi + amplitude*cos(m_pert*phi)/m_pert
    """
    r = nb.rxyz()
    phi = np.arctan2(nb.pos[:, 1], nb.pos[:, 0])
    theta = np.arccos(nb.pos[:, 2]/r)
    phi_prime = phi + amplitude*np.cos(m_pert*phi)/m_pert

    # Transform to carthesian coordinates
    nb.pos[:, 0] = r * np.cos(phi_prime)*np.sin(theta)
    nb.pos[:, 1] = r * np.sin(phi_prime)*np.sin(theta)
    # Do not change z

    nb.translate(centre)
    return nb


def generate_seed(seed):
    if seed == -1:
        seed = secrets.randbits(128)
    else:
        seed = seed
    return seed


def cut_k_space(k, k_min, k_max):
    index = np.nonzero(np.abs(k) > k_max)  # select below k_min
    k[index] = k_min  # set to the min those components
    index = np.nonzero(np.abs(k) < k_min)  # select below k_min
    k[index] = k_max  # set to the max those components
    return k

#%%
####################################################################
# option parser
####################################################################


def parse_option():
    description = """"
Generate an homogeneous sphere model. A Boss and Bodenheimer density perturbation can
be applied, as well as a uniform rotation field or a velocity field following a power spectrum
(in Fourier space).
Also, a sink particle can be added.

The mass units are 10^-10 M_sun and km/s. The unit length can be chosen. With the velocity
units, it determines the time unit.

WARNING: Some parts of this scripts are highly experimental. They require to use pNbody with
the branch 'darwin' to work.
"""
    epilog = """
Examples:
--------
python3 ic_homogeneous_sphere.py -o test_h_sphere.hdf5 -N 10000 --R_s 2406 --M_tot 1 --unit_Length "AU" --apply_rotational_velocity_field --Omega=1.6e-12
python3 ic_homogeneous_sphere.py -o test_h_sphere.hdf5 -N 10000 --R_s 2406 --M_tot 1 --unit_Length "AU" --apply_rotational_velocity_field --Omega=1.6e-12 --add_sink
python3 ic_homogeneous_sphere.py -o test_h_sphere.hdf5 -N 10000 --R_s 2406 --M_tot 1 --unit_Length "AU" --apply_rotational_velocity_field --Omega=1.6e-12 --apply_density_Boss_Bodenheimer

Here is a realistic usage example to generate a collapsing molecular cloud:
    First, get an estimate of the potential energy of the cloud (to speed up computation  later for 4e6 particles):
        python3 ic_homogeneous_sphere.py -o cloud_few_part.hdf5 -N 40000 --R_s 20054 --M_tot 100 --unit_Length "AU" --U_int=8.4e3 --apply_velocity_power_spectrum_perturbation --N_mesh 256
    Now, to generate the cloud:
    python3 ic_homogeneous_sphere.py -o cloud.hdf5 -N 4000000 --R_s 20054 --M_tot 100 --unit_Length "AU" --U_int=8.4e5 --E_pot=-2.659213e-12 --apply_velocity_power_spectrum_perturbation --N_mesh 256 --velocity_field_type='solenoidal'
"""
    parser = argparse.ArgumentParser(description=description, epilog=epilog,
                                     formatter_class=scripts_utils.RawTextArgumentDefaultsHelpFormatter)

    parser.add_argument("-o",
                        action="store",
                        type=str,
                        dest="outputfilename",
                        default=None,
                        help="Name of the output file")

    parser.add_argument("-t",
                        action="store",
                        type=str,
                        dest="ftype",
                        default='swift',
                        help="Type of the output file")

    parser.add_argument("--unit_Length",
                        default="AU", type=str, choices=["kpc", "pc", "AU"],
                        help="Length units to generate the ICs.")

    parser.add_argument("--unit_Mass",
                        default="1e10M_sun", type=str, choices=["M_sun", "1e10M_sun"],
                        help="Mass units to generate the ICs.")

    parser.add_argument("-N",
                        action="store",
                        type=int,
                        default=None,
                        help='total number of particles')

    # Particle parameters
    parser.add_argument("--mass",
                        action="store",
                        type=float,
                        dest="mass",
                        default=None,
                        help="Particle mass in M_sun")

    parser.add_argument("--U_int_per_part",
                        action="store",
                        type=float,
                        default=None,
                        help="Internal energy per unit mass per particle.")

    # parser.add_argument("--U_int_tot",
    #                     action="store",
    #                     type=float,
    #                     default=None,
    #                     help="Total internal energy per unit mass.")

    parser.add_argument("--E_pot",
                        action="store",
                        type=float,
                        default=None,
                        help="Estimation of the potential energy for diverse computations in the script. "
                        "This does not change the positions of the particles. To get an estimation, "
                        "choose a small number of particles (like 1e4) without using this option. "
                        "Then, go to the target (higher) resolution and set the estimation. The "
                        "computations of the potential energy with a high number of particles "
                        "takes lot of time. ")

    parser.add_argument("--alpha",
                        action="store",
                        type=float,
                        default=0.26,
                        help="Ratio  E_therm/abs(E_pot).")

    parser.add_argument("--h",
                        action="store",
                        dest="h",
                        type=float,
                        default=None,
                        help="Gas smoothing length in given units.")

    # Sphere parameters
    parser.add_argument("--R_s",
                        action="store",
                        type=float,
                        default=3300,
                        help="Radius of the sphere in given units")

    parser.add_argument("--M_tot",
                        action="store",
                        type=float,
                        dest="Mtot",
                        default=1,
                        help="Total mass in Msol")

    # Boxsize parameter
    parser.add_argument("--boxsize",
                        action="store",
                        type=float,
                        dest="boxsize",
                        default=None,
                        help="Size of the cubic box in given units")

    # Perturbations and their parameters---------------------------
    # Boss and Bodenheimer perturbation
    parser.add_argument("--apply_density_Boss_Bodenheimer",
                        action="store_true",
                        help="Apply a Boss and Bodenheimer density (azimutal) perturbation : "
                        "rho(phi) = rho_0*(1 + amplitude*cos(m*phi)).")

    parser.add_argument("--density_amplitude",
                        action="store",
                        type=float,
                        default=0.5,
                        help="Density pertubation amplitude.")

    parser.add_argument("--m",
                        action="store",
                        type=int,
                        default=2,
                        help="Density pertubation m (cos(m*phi)).")

    # Rigid rotation field
    parser.add_argument("--apply_rotational_velocity_field",
                        action="store_true",
                        help="Apply a uniform rotation field along the z axis.")

    parser.add_argument("--Omega",
                        action="store",
                        type=float,
                        default=1.6e-12,
                        help="Rotational velocity (v_T = Omega*r) in s^-1.")

    # Velocity perturbation following a power spectrum perturbation
    parser.add_argument("--apply_velocity_power_spectrum_perturbation",
                        action="store_true",
                        help="Apply a perturbation to the velocity, which follows a power spectrum in "
                        "fourier space.")

    parser.add_argument("--velocity_field_type",
                        action="store",
                        type=str,
                        default="solenoidal",
                        choices=["solenoidal", "divergent_free",
                                 "compressive", "rotational_free", "mixed"],
                        help="Type of velocity field. Notice that solenoidal = divergent_free ; "
                        "compressive = rotational_free.")

    parser.add_argument("--N_mesh",
                        action="store",
                        type=int,
                        default=64,
                        help="Size of the Fourier grid used to compute the perturbation field. One should "
                        "have N_mesh >= N_sample.")

    parser.add_argument("--N_sample",
                        action="store",
                        type=int,
                        default=None,
                        help="Sets the maximum k vector that the code uses. It determines the"
                        "Nyquist frequency, k_nyquist = 2*pi/boxsize * N_sample/2."
                        "Normally, one chooses N_sample such taht N_tot = N_sample**3, with"
                        "N_tot the total number of particles. If the value is None, the script "
                        "determines N_sample from the number of particles.")

    parser.add_argument("--seed",
                        action="store",
                        type=int,
                        default=128951219801374517513834785014410262710,
                        help="Seed to draw random numbers for the perturbation. A value of -1 means "
                        "that the seed will be randomly chosen using the secret module of the "
                        "standard library.")

    parser.add_argument("--exponent",
                        action="store",
                        type=float,
                        default=-4,
                        help="Exponent of the power-law power spectrum. ")

    parser.add_argument("--E_kin_over_E_pot",
                        action="store",
                        type=float,
                        default=1.0,
                        help="Ratio of E_kin/E_pot to rescale the velocities.")

    parser.add_argument("--v_max_cut",
                        action="store",
                        type=float,
                        default=1e3,
                        help="Cut the velocities exceeding v_max_cut. This is to avoid velocities too high, "
                        "for example, bigger than the speed of light. Units: km/s")

    # Add a sink in the simulation sphere ------------------
    parser.add_argument("--add_sink",
                        action="store_true",
                        help="Add a sink particle")

    parser.add_argument("--sink_mass",
                        action="store",
                        type=float,
                        default=1,
                        help="Mass of the sink in M_sun")

    parser.add_argument("--sink_pos",
                        action=scripts_utils.store_as_array,
                        nargs=3,
                        type=float,
                        default=np.array([0, 0, 0]),
                        help="Position of the sink")

    parser.add_argument("--sink_vel",
                        action=scripts_utils.store_as_array,
                        nargs=3,
                        type=float,
                        default=np.array([0, 0, 0]),
                        help="Velocity of the sink")
    # Other
    parser.add_argument("--cvcenter",
                        action="store_true",
                        help="Center the center of velocities at the origin.")

    # parser.add_argument("--irand",
    #                     action="store",
    #                     dest="irand",
    #                     metavar='INT',
    #                     type=int,
    #                     default=0,
    #                     help='Random seed for the positions of the particles')

    return parser.parse_args()

#%%
########################################
# Get Simplified names for our stuff
########################################

if __name__ == "__main__":
    opt = parse_option()
    print("\n-------------------------------------")
    print("Welcome to ic_homogeneous_sphere script !\n")

    # If no output, exit
    if opt.outputfilename is None:
        raise ValueError("No output files has been provided...")

    ptype = 0  # Gas

    unit_Length = opt.unit_Length
    unit_Mass = opt.unit_Mass
    R_s = opt.R_s  # in unit_Length
    M_tot = opt.Mtot*u.M_sun  # in M_sun

    # Prepare the random generator
    seed = opt.seed
    seed = generate_seed(seed)
    rng = np.random.default_rng(seed=seed)

    # Boss and Bodenheimer default values for the density perturbation are
    density_amplitude = opt.density_amplitude
    m = opt.m
    sphere_centre = np.zeros(3)

    # Rotational velocity parameters
    Omega = opt.Omega

    # alpha=E_them/abs(E_pot)
    alpha = opt.alpha

    # Power spectrum choice
    exponent = opt.exponent
    # We perform a velocity rescaling, so it is useless to set an amplitude
    power_spectrum_amplitude = 1.0
    power_spectrum_type = "power_law"
    # power_spectrum_type = opt.power_spectrum_type
    power_spectrum = None
    N_mesh = opt.N_mesh
    epsilon = 1e-2
    E_kin_over_E_pot = opt.E_kin_over_E_pot
    velocity_field_type = opt.velocity_field_type
    v_max_cut = opt.v_max_cut


    # Define the power spectrum
    if power_spectrum_type == "power_law":
        power_spectrum = PowerLawPowerSpectrum(exponent, power_spectrum_amplitude)
    else:
        raise ValueError("Not yet implemented")

    ########################################
    #  Define units
    ########################################

    if unit_Length == "kpc":
        u_Length = 1 * u.kpc
    elif unit_Length == "pc":
        u_Length = 1 * u.pc
    elif unit_Length == "AU":
        u_Length = 1 * u.AU
    else:
        raise ValueError("Unit type {} not defined yet".format(unit_Length))

    if unit_Mass == "M_sun":
        u_Mass = 1 * u.M_sun
    elif unit_Mass == "1e10M_sun":
        u_Mass = 1e10*u.M_sun
    else:
        raise ValueError("Unit type {} not defined yet".format(unit_Length))

    u_Length = u_Length.to(u_Length)
    u_Mass = u_Mass.to(u_Mass)
    u_Velocity = 1 * u.km/u.s
    u_Velocity = u_Velocity.to(u_Velocity)
    u_Time = u_Length.to(u.km)/u_Velocity
    u_Time = u_Time.to(u_Time)

    u_dict = {}
    u_dict["UnitLength_in_cm"] = u_Length.to(u.cm).value
    u_dict["UnitMass_in_g"] = u_Mass.to(u.g).value
    u_dict["UnitVelocity_in_cm_per_s"] = u_Velocity.to(u.cm/u.s).value
    u_dict["Unit_time_in_cgs"] = u_Time.to(u.s).value

    # Define constants values
    G_unit = cte.G.to(u_Length**3/(u_Mass*u_Time**2))
    G = G_unit.value

    print(
        """The units are :
     UnitLength_in_cm = {:e} cm
     UnitMass_in_g = {:e} g
     UnitVelocity_in_cm_per_s = {:e} cm/s
     Unit_time_in_s = {:e} s
     G = {:e}
    """.format(u_dict["UnitLength_in_cm"], u_dict["UnitMass_in_g"],
               u_dict["UnitVelocity_in_cm_per_s"], u_dict["Unit_time_in_cgs"], G))


    # Convert M_tot to right units
    M_tot = M_tot.to(u_Mass).value

    # REVOIR CA en considérant tous les cas... avec M_tot, N et mass
    # Set the number of particles and the mass of individual particles in the right units
    if (opt.N is not None) and (opt.mass is None):
        N = opt.N
        opt.mass = M_tot/N
    elif (opt.N is None) and (opt.mass is not None):
        # Do with solar masses unit to avoid computations problems...
        opt.mass = (opt.mass*u.M_sun).to(u_Mass).value
        N = int(M_tot/opt.mass)
        print(N, M_tot, opt.mass)
    else:
        raise ValueError(
            "The options N or mass need to be set to determine the number of particles")


    # Determine N_sample
    if opt.apply_velocity_power_spectrum_perturbation:
        if opt.N_sample is not None:
            N_sample = opt.N_sample
        else:
            N_sample = int(np.ceil(np.cbrt(N)))

        print("N_sample = {}".format(N_sample))
        print("N_mesh = {}".format(N_mesh))

        if N_mesh < N_sample:
            print("\nWarning: We should have N_mesh > N_sample for an accurate interpolation of the perturbation from the Fourier grid.\n")

    ########################################
    # Generate the model
    ########################################

    # Create our model object (it is used to compute the density)
    h_sphere = HomogeneousSphere(R_s=R_s, M_tot=M_tot, G=G)

    # Generate the homogeneous sphere
    # nb = h_sphere.generate_IC(N, R_s, name=opt.outputfilename, seed=seed, ftype="swift")
    nb = h_sphere.generate_IC(
        N, R_s, name=opt.outputfilename, seed=0, ftype="swift")

    # Print free fall time as info
    t_ff = h_sphere.FreeFallTime()
    print(
        "\nThe free fall time ot the sphere is t_ff = {:.2e} unit_time.".format(t_ff))
    print(
        "The density of the sphere is rho = {:.2e} unit_mass/unit_length^3.".format(h_sphere.rho_0))

    # Set the particle type
    nb.set_tpe(ptype)

    # Set units in the object
    nb.UnitLength_in_cm = u_dict["UnitLength_in_cm"]
    nb.UnitMass_in_g = u_dict["UnitMass_in_g"]
    nb.UnitVelocity_in_cm_per_s = u_dict["UnitVelocity_in_cm_per_s"]
    nb.Unit_time_in_cgs = u_dict["Unit_time_in_cgs"]

    # Apply BB density
    if opt.apply_density_Boss_Bodenheimer:
        print("Applying density perturbation...")
        nb = apply_azimuthal_density_perturbation(
            nb, centre=sphere_centre, amplitude=density_amplitude, m_pert=m)
        print("End of density perturbation.")

    # Apply rotation
    axis = np.array([0, 0, 0])
    Omega = Omega/u.s
    if opt.apply_rotational_velocity_field:
        # Add units to the position and Omega to convert the velocities to u_Velocity
        pos = nb.pos << u_Length
        nb.pos = pos

        axis = np.array([0, 0, 1])

        print("Applying rotational velocity...")
        print("Omega = {:.2e}".format(Omega))
        nb = apply_rotational_velocity_field(nb, Omega, axis)

        nb.pos = nb.pos.value
        nb.vel = nb.vel.to(u_Velocity).value

    # Add velocity perturbation following a power spectrum
    # See below, we need the boxsize which is set below


    ########################################
    # Add a sink particle if necessary
    ########################################
    N_gas = nb.npart[0]
    N_sink = 0
    nb_gas = nb

    if opt.add_sink:
        print("Adding a sink at x = {} with velocity v = {}".format(
            opt.sink_pos, opt.sink_vel))
        sink_id = 3
        # reshape to get the right number of particles (i.e. 1)
        sink_pos = np.reshape(opt.sink_pos, (1, 3))
        sink_vel = np.reshape(opt.sink_vel, (1, 3))
        sink_mass = np.array([(opt.sink_mass*u.M_sun).to(u_Mass).value])
        nb_sink = Nbody(status="new", p_name=opt.outputfilename,
                        pos=sink_pos, vel=sink_vel, mass=sink_mass, ftype="swift")
        nb_sink.set_tpe(sink_id)
        nb_gas = deepcopy(nb)
        nb = nb_gas + nb_sink
        N_sink = nb_sink.npart[3]
        print("Adding a sink at {} with velocity {}.".format(
            sink_pos.flatten(), sink_vel.flatten()))


    ######################################################################
    # Simulation parameters (smoothing lenght, boxsize, etc)
    # Do not add more particles after here. Do add them above this line
    ######################################################################
    # print("\n")

    # Other stuff
    if opt.boxsize is None:
        r_max_part = np.max(nb.rxyz())
        print(
            "The maximal distance of particles is {:.2e} unit_Length".format(r_max_part))
        nb.boxsize = 2.5*r_max_part
    else:
        nb.boxsize = opt.boxsize

    # Now add the missing fields to the gas
    N = np.sum(nb.npart)
    L = nb.boxsize

    # Mean density
    r = nb.rxyz()
    phi = nb.phi_xy()
    if opt.apply_density_Boss_Bodenheimer:
        rho = boss_bodenheimer_density(
            r=r, phi=phi, rho_0=h_sphere.rho_0, amplitude=density_amplitude, m=m)
    else:
        # Homogeneous sphere density
        rho = h_sphere.Density(r)

    # Convert the density to atom/cm^3
    rho << (u.M_sun/u_Length**3).to(u.M_p/u.cm**3)

    # SPH smoothing length
    h = opt.h
    if h is None:
        h = np.ones(N) * 3 * L / N ** (1.0 / 3.0)
        h = np.ones(N)*R_s/N**(1.0/3.0)
    else:
        h = h*np.ones(N)

    if opt.apply_rotational_velocity_field:
        print("Computing rotation energy...")
        I = nb_gas.inertial_tensor() << u_Mass*u_Length**2  # 3x3 matrix
        Omega_vect = (Omega*axis).to(1/u_Time)
        E_rot = 0.5*np.matmul(np.matmul(Omega_vect, I), Omega_vect).value
        # angular_momentum_to = nb.Ltot()
        # I_inverse = np.linalg.inv(I)
        # E_rot = 0.5*np.matmul(np.matmul(Omega_vect, I_inverse), angular_momentum_to).value

    # I = nb_gas.inertial_tensor() << u_Mass*u_Length**2  # 3x3 matrix
    # angular_momentum = nb.Ltot() * u_Mass*u_Length**2/u_Time
    # I_inverse = np.linalg.inv(I)
    # E_rot = 0.5*np.matmul(np.matmul(angular_momentum, I_inverse), angular_momentum).value

    print("Computing potential energy...")
    # Compute the potential energy
    if opt.E_pot is None:
        # Estimation of the gravitational softening length based on the SPH smoothing length
        softening = np.mean(h)/10
        E_pot = G*nb.Epot(softening)  # the method does not multiply by G
    else:
        E_pot = opt.E_pot

    # Internal energy
    if (opt.U_int_per_part is not None):
        U_int_per_part = opt.U_int_per_part * np.ones(N)
        U_int = np.sum(U_int_per_part*nb.mass)
    else:  # case where alpha is given
        U_int = alpha * np.abs(E_pot)  # Multiply by masses to get true energies
        U_int_per_part = U_int/N / nb.mass

    # Add velocity perturbation following a power spectrum
    if opt.apply_velocity_power_spectrum_perturbation:
        print("\nApplying perturbation on velocities...")

        vec_size = (3, N)  # size of the samples to draw
        PPS = PerturbationPowerSpectrum(
            power_spectrum, L, N_sample, N_mesh, seed, epsilon)

        print("Generating k space grid...")
        mesh_index = PPS.generate_index_grid()
        k = PPS.generate_k(mesh_index)  # Vector k at each point in the grid

        # Cut the k space to only have k between k_min and k_nyquist
        k_min = 2*np.pi/L
        k_max = PPS.k_nyquist()
        print("k_min = {:.2e}\nk_max = k_nyquist = {:.2e}".format(k_min, k_max))
        k = PPS.cut_k_space(k, k_max=k_max, k_min=k_min)
        k_norm = np.linalg.norm(k, axis=0)

        # Compute the perturbation in Fourier space to get the Fourier components
        print("Computing the Fourier perturbation grid...")
        PPS.compute_fourier_perturbation_field(
            k, perturbation_field_type=velocity_field_type, a_L=1, a_T=1)

        # Now, compute the perturbation field in real space
        print("Computing the real space perturbation grid...")
        PPS.compute_perturbation_field()

        # Compute the displacement from the velocity grid of particles
        print("Computing the velocity perturbation field for the particles...")
        displacement = PPS.compute_displacement(N, nb.pos.transpose())
        vel = displacement.transpose()
        v_norm = np.linalg.norm(vel, axis=1)

        # Rescale the max to 1 first
        v_norm = np.linalg.norm(vel, axis=1)
        E_kin = 0.5*np.sum(nb.mass*v_norm**2)

        # Compute the actual E_kin over E_pot ratio
        E_kin_over_E_pot_current = E_kin/np.abs(E_pot)
        factor = 1/np.sqrt(E_kin_over_E_pot_current)
        vel *= factor

        # Now, rescale to the desired E_kin/E_pot ratio
        factor = np.sqrt(E_kin_over_E_pot)
        vel *= factor

        # Cut the velocites beyond v_max_cut
        v_norm = np.linalg.norm(vel, axis=1)
        I = np.argwhere(v_norm > v_max_cut)
        vel[I, :] = 0

        # Give the velocites to the Nbody object
        nb.vel += vel

    # To do:
    # Add an option for cosmological run ?
    nb.setComovingIntegrationOff()

    # cvcenter
    if opt.cvcenter:
        nb.cvcenter()

    # Print some information
    print("\n\nSome information about the simulation:")
    print(" The boxsize is {} unit_Length".format(nb.boxsize))
    print(" The shift to set in swift is : {} unit_Length".format(nb.boxsize/2))
    print(" Smoothing length : h = {}".format(h[0]))
    print(" Mass of the gas particles = {:e} unit_mass".format(nb_gas.mass[0]))
    print(" E_pot = {:e}".format(E_pot))
    print(" E_kin = {:e}".format(nb.Ekin()))
    print(" U_int = {:e}".format(U_int))
    print(" E_kin/E_pot = {:.2e}".format(nb.Ekin()/np.abs(E_pot)))
    print(" alpha = {:.2e}".format(U_int/np.abs(E_pot)))

    if opt.apply_rotational_velocity_field:
        # print(" L_tot = {} unit_mass*unit_length/unit_time^2".format(angular_momentum.value))
        print(" E_rot = {:.2e}".format(E_rot))
        print(" beta = {:.2e}".format(E_rot/np.abs(E_pot)))

    # Do not take into account the sink mass since it is bigger than the gas... U_int is essentially for
    # the gas, not the sink
    print(" U_int/mass = {:e}".format(U_int/nb_gas.mass_tot))
    print(" U_int_per_part = {:e}".format(U_int_per_part[0]*nb_gas.mass[0]))
    print(" U_int_per_part/mass= {:e}".format(U_int_per_part[0]))

    print("------------------------------")
    print("Homogeneous sphere properties: ")
    rho_0 = h_sphere.rho_0*u_Mass/u_Length**3
    print(" M_tot = {:e} unit_mass".format(M_tot))
    print(" R_s = {:e} unit_length".format(R_s))
    print(" rho_0 = {:e} unit_mass/unit_length^3".format(rho_0.value))
    print(" rho_0 = {:e} g/cm^3".format(rho_0.to(u.g/u.cm**3).value))
    print(" rho_0 = {:e} atom/cm^3".format(rho_0.to(cte.m_p.to(u.g)/u.cm**3).value))
    print("N", N)

    # Add Boss and Bodenheimer information
    # Add sink information

    if opt.apply_velocity_power_spectrum_perturbation:
        print("------------------------------")
        print("Velocity field stats: ")
        # Print info about the velocities
        v_norm = np.linalg.norm(nb.vel, axis=1)
        print(" max : {:.2e}".format(np.max(v_norm)))
        print(" min : {:.2e}".format(np.min(v_norm)))
        print(" mean : {:.2e}".format(np.mean(v_norm)))
        print(" RMS : {:.2e}".format(np.sqrt(np.mean(v_norm*2))))
        print(" std : {:.2e}".format(np.std(v_norm)))
        print(" median abs deviation : {:.2e}".format(
            median_abs_deviation(v_norm)))
        print(" 5 percentile : {:.2e}".format(np.percentile(v_norm, 5)))
        print(" 25 percentile : {:.2e}".format(np.percentile(v_norm, 25)))
        print(" median : {:.2e}".format(np.percentile(v_norm, 50)))
        print(" 75 percentile : {:.2e}".format(np.percentile(v_norm, 75)))
        print(" 95 percentile : {:.2e}".format(np.percentile(v_norm, 95)))
        print(" 99 percentile : {:.2e}".format(np.percentile(v_norm, 99)))

    ######################################################################
    # Write in file
    ######################################################################

    # find the sink index
    gas_id = 0
    sink_id = 3
    sink_index = np.argwhere(nb.tpe == sink_id).flatten()
    gas_index = np.argwhere(nb.tpe == gas_id).flatten()

    # Here, treat the gas and the sink separately when appropriate
    with h5py.File(opt.outputfilename, "w") as fileOutput:
        # Header
        grp = fileOutput.create_group("/Header")
        grp.attrs["BoxSize"] = [L, L, L]
        grp.attrs["NumPart_Total"] = [N_gas, 0, 0, N_sink, 0, 0]
        grp.attrs["NumPart_Total_HighWord"] = [0, 0, 0, 0, 0, 0]
        grp.attrs["NumPart_ThisFile"] = [N_gas, 0, 0, N_sink, 0, 0]
        grp.attrs["Time"] = 0.0
        grp.attrs["NumFileOutputsPerSnapshot"] = 1
        # Swift ignores this and take the mass of the individual particles
        grp.attrs["MassTable"] = [0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
        grp.attrs["Flag_Entropy_ICs"] = [0, 0, 0, 0, 0, 0]
        grp.attrs["Dimension"] = 3

        # Units
        grp = fileOutput.create_group("/Units")
        grp.attrs["Unit length in cgs (U_L)"] = u_dict["UnitLength_in_cm"]
        grp.attrs["Unit mass in cgs (U_M)"] = u_dict["UnitMass_in_g"]
        grp.attrs["Unit time in cgs (U_t)"] = u_dict["Unit_time_in_cgs"]
        grp.attrs["Unit current in cgs (U_I)"] = 1
        grp.attrs["Unit temperature in cgs (U_T)"] = 1

        # Particle group: gas
        grp = fileOutput.create_group("/PartType0")
        grp.create_dataset("Coordinates", data=nb.pos[gas_index], dtype="d")
        grp.create_dataset("Velocities", data=nb.vel[gas_index], dtype="f")
        grp.create_dataset("Masses", data=nb.mass[gas_index], dtype="f")
        grp.create_dataset("SmoothingLength", data=h[gas_index], dtype="f")
        grp.create_dataset(
            "InternalEnergy", data=U_int_per_part[gas_index], dtype="f")
        grp.create_dataset("ParticleIDs", data=nb.num[gas_index], dtype="L")
        grp.create_dataset("Densities", data=rho[gas_index], dtype="f")

        # Particle group: sink
        if opt.add_sink:
            grp = fileOutput.create_group("/PartType3")
            grp.create_dataset("Coordinates", data=nb.pos[sink_index], dtype="d")
            grp.create_dataset("Velocities", data=nb.vel[sink_index], dtype="f")
            grp.create_dataset("Masses", data=nb.mass[sink_index], dtype="f")
            grp.create_dataset("w", data=h[sink_index], dtype="f")
            grp.create_dataset(
                "InternalEnergy", data=U_int_per_part[sink_index], dtype="f")
            grp.create_dataset("ParticleIDs", data=nb.num[sink_index], dtype="L")
            grp.create_dataset("Densities", data=rho[sink_index], dtype="f")

    print("END-------------------------------------")
