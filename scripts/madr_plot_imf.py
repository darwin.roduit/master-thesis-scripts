#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Mar  7 10:50:00 2024

@author: darwin
"""

# import sys
import os
import argparse
import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit
from tqdm import tqdm
from pNbody import Nbody
from pNbody import *
from MaDarwinR.analysis_functions import compute_IMF
from MaDarwinR.scripts_utils import RawTextArgumentDefaultsHelpFormatter, store_as_array

# plt.style.use('lab_style_figure.mplstyle')

# %%
def imf_make_plot(m_discrete_bin_edges, count_m_discrete, m_bin_edges,
                  output_location, count_m, output_filename, **kwargs):

    image_format = kwargs.get("image_format", "pdf")
    dpi = kwargs.get("dpi", 300)
    figsize = kwargs.get("figsize", (10, 4))

    def __kroupa_IMF(x, a_1, a_2, a_3, a_4):
        """This is valid for x > 1 M_sun"""
        integrate = 1
        if x > 0.02 and x <= 0.08:
            return a_1*x**(-3+integrate)
        elif x > 0.08 and x <= 0.5:
            return a_2*x**(-1.3+integrate)
        if x > 0.5 and x <= 1:
            return a_3*x**(-2.3+integrate)
        elif x > 1:
            return a_4*x**(-2.7+integrate)
        else:
            return 0

    def kroupa_IMF(x, a_1, a_2, a_3, a_4):
        """This is valid for x > 1 M_sun"""
        output = np.zeros(x.shape)
        for i, element in enumerate(x):
            output[i] = __kroupa_IMF( element, a_1, a_2, a_3, a_4)
        return output

    popt, pcov = curve_fit(
        kroupa_IMF, xdata=m_discrete_bin_edges[:-1], ydata=count_m_discrete)
    a_1 = popt[0]
    a_2 = popt[1]
    a_3 = popt[2]
    a_4 = popt[3]
    mass_fit = np.linspace(
        m_discrete_bin_edges[0], m_discrete_bin_edges[-1], 100)
    count_fit_imf = kroupa_IMF(mass_fit, a_1, a_2, a_3, a_4)

    fig, ax = plt.subplots(num=1, ncols=2, nrows=1,
                           figsize=figsize, layout="tight")
    ax[0].cla()
    ax[0].set_title("IMF discrete part")
    ax[0].bar(m_discrete_bin_edges[:-1], count_m_discrete,
              width=np.diff(m_discrete_bin_edges))  # plot the bar plot
    ax[0].plot(mass_fit, count_fit_imf, color="tomato")  # plot the fit
    ax[0].set_ylabel("N$_\star$")
    ax[0].set_xlabel("M [M$_\odot$]")
    ax[0].set_xscale("log")
    ax[0].set_yscale("log")

    ax[1].cla()
    ax[1].set_title("Complete IMF")
    ax[1].bar(m_bin_edges[1:], count_m, width=np.diff(m_bin_edges))
    ax[1].set_ylabel("N$_\star$")
    ax[1].set_xlabel("M [M$_\odot$]")
    ax[1].set_xscale("log")
    ax[1].set_yscale("log")

    plt.savefig(output_location + output_filename +
                "_IMF", format=image_format, bbox_inches='tight', dpi=dpi)
    plt.close()

# %% Parser options


def parse_option():
    description = """"
Plot the IMF of the inputs.
    """
    epilog = """
Examples:
--------
madr_plot_imf.py snap_0020_UFD.hdf5 --n_bins=20
madr_plot_imf.py snap_0020_UFD.hdf5 --n_bins=20 -o snap_20

"""
    parser = argparse.ArgumentParser(description=description, epilog=epilog,
                                     formatter_class=RawTextArgumentDefaultsHelpFormatter)

    parser.add_argument("files",
                        nargs="+",
                        type=str,
                        help="File name(s).")

    parser.add_argument("--output_location",
                        action="store",
                        type=str,
                        dest="output_location",
                        default=None,
                        help="Name of the output location")

    parser.add_argument("-o",
                        action="store",
                        type=str,
                        dest="output_filename",
                        default=None,
                        help="Name of the output file. Use it if you only give ONE input file. Otherwise, "
                        "all files will have the same name.")

    parser.add_argument("-t",
                        action="store",
                        type=str,
                        dest="ftype",
                        default='swift',
                        help="Type of the output file")

    # Particle parameters
    parser.add_argument("--n_bins",
                        action="store",
                        type=int,
                        default=20,
                        help="Number of bins for the histogram IMF.")

    parser.add_argument("--image_format",
                        action="store",
                        type=str,
                        default="png",
                        help="Format of the output image file. This must be recognized by Matplotlib.")

    parser.add_argument("--dpi",
                        action="store",
                        type=float,
                        default=300,
                        help="Output image resolution.")

    parser.add_argument("--figsize",
                        action=store_as_array,
                        nargs=2,
                        type=float,
                        default=np.array([6.4,4.8]),
                        help="Size of the figures (width, height)")

    parser.add_argument("--stylesheet",
                        action="store",
                        type=str,
                        default=None,
                        help="Matplotlib stylesheet.")

    parser.parse_args()
    args = parser.parse_args()
    files = args.files

    for f in files:
        if not os.path.exists(f):
            raise FileNotFoundError("You need to provide one file")

    return args, files

# %% Main


if __name__ == "__main__":
    # Parse the input
    opt, files = parse_option()

    print("\n-------------------------------------")
    print("Welcome to IMF script !\n")

    output_location = opt.output_location
    output_filename = opt.output_filename
    n_bins = opt.n_bins
    image_format = opt.image_format
    dpi = opt.dpi
    figsize = opt.figsize
    stylesheet = opt.stylesheet

    if stylesheet is not None:
        plt.style.use(stylesheet)

    # If no output, exit
    if output_location is None:
        output_location = ""
    else:
        if not (os.path.exists(output_location)):
            print("Output directory does not exist. It will be created.")
            os.makedirs(output_location)

        # Adds the "/" at the end if needed
        if (output_location[-1] != "/"):
            output_location = output_location+"/"

    for filename in tqdm(files):

        # If we do not give output_filename
        # Notice that I keep opt.output_filename, otherwise this condition is not fullfilled for the next
        # iteration
        if opt.output_filename is None:
            # Keeps the base name (without /path/to/file) and without extension .hdf5
            output_filename = os.path.split(filename)[1]
            output_filename = os.path.splitext(output_filename)[0]

        # Select the object
        print("Loading the data...")
        nb_star = Nbody(p_name=filename, ptypes=[4])

        # Do not continue if you do not find stars.
        if nb_star.npart[4] == 0:
            print("No stars found in this file. Continue to the next file...")
            continue

        # Take the initial star mass
        mass_s = nb_star.InitialMass(units="Msol")

        # Compute IMF
        count_m_discrete, m_discrete_bin_edges, count_m_continuous, count_m, m_bin_edges = \
            compute_IMF(nb_star, mass_s, n_bins)

        print("Making plot...")
        imf_make_plot(m_discrete_bin_edges, count_m_discrete, m_bin_edges, output_location,
                      count_m, output_filename, image_format=image_format, dpi=dpi, figsize=figsize)

    print("END-------------------------------------")
