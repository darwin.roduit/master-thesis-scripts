#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Apr  6 11:30:03 2024

@author: darwinr
"""
import os
import argparse
import numpy as np
import matplotlib.pyplot as plt
from pNbody import Nbody
from pNbody import *
from tqdm import tqdm
from MaDarwinR.scripts_utils import RawTextArgumentDefaultsHelpFormatter, store_as_array


# plt.style.use(SCRIPT_DIR + "/"  + 'lab_style_figure.mplstyle')

#%%


def get_sink_data(sink, nb):
    """
    Extract data for a given sink particle at a given snapshot.

    Parameters
    ----------
    sink : int
        ID of the sink we want to extract the data.
    nb : Nbody object
        Nbody object of the given snapshot.

    Returns
    -------
    sink_mass : float
        Mass of sink. In M_sun.
    n_gas_swallowed : int
        Number of gas particles swallowed by sink.
    n_sink_swallowed : int
        Number of sink particles swallowed by sink..
    redshift : float
        Redshift of the current snapshot.
    time : float
        Time of the current snapshot. In Gyr.

    """
    # Look in the nb fo the position of the sink in the arrays
    sink_index = np.argwhere(nb.num == sink).ravel()

    # If sink is not found, end here
    if sink_index.size == 0:
        sink_mass = np.NaN
        n_gas_swallowed = np.NaN
        n_sink_swallowed = np.NaN
        redshift = np.NaN
        time = np.NaN
    else:
        # The [0] is to onvert the 1D array to scalar
        sink_mass = (nb.Mass(units="Msol")[sink_index])[0]
        n_gas_swallowed = (nb.ngasswallowed[sink_index])[0]
        n_sink_swallowed = (nb.nsinkswallowed[sink_index])[0]
        redshift = nb.redshift
        time = nb.Time(units='Gyr')
        # print(sink_mass, n_gas_swallowed, n_sink_swallowed, redshift)
    return sink_mass, n_gas_swallowed, n_sink_swallowed, redshift, time


def get_sink_data_all_files(filenames):
    """
    Retrieve mass, n_gas_swallowed, n_sink_swallowed, redshift, time for all sink in all given files.

    Parameters
    ----------
    filenames : list of string
        List of the snapshots to analyse.

    Returns
    -------
    list_of_sink_id : set of int
        Set containing all sink IDs in the simulation.
    data_all_sink : dictionary with key:sink_id, val=dictionary of data
        Contains the data of all sinks in the simulation, The 'val' dictionary contains the following
        keys: mass, n_gas_swallowed, n_sink_swallowed, redshift, time. Units are M_sun and Gyr.
    number_sink_per_snap : array of int
        Number of sinks per snapshot.

    """
    id_sink = 3
    n_files = len(filenames)

    # Set of all sinks
    list_of_sink_id = set()
    data_all_sink = dict()
    number_sink_per_snap = np.zeros(n_files)

    for i, file in enumerate(tqdm(filenames)):
        # print(file)
        nb = Nbody(file, ptypes=[id_sink])
        if nb.npart[id_sink] == 0:
            print("No sink particles found in this file. Continue to the next file")
            continue

        # Put the sink id (num) in a set so that we do not have duplicate sinks
        for sink_num in nb.num:
            number_sink_per_snap[i] = len(nb.num)
            list_of_sink_id.add(sink_num)

        # Get the data
        # Now, iterate over all sinks in the set to get the data contained in this snapshot
        for sink in list_of_sink_id:
            sink_mass, n_gas_swallowed, n_sink_swallowed, redshift, time = get_sink_data(
                sink, nb)

            # Store this in the data_all_sink[sink] dictionary
            if sink not in data_all_sink:
                # I prefer to put Nan so that we can disthinguish true 0 values from the case where the sink
                # does not exist and thus has no value
                sink_dict = {'mass': np.full(n_files, np.nan), 'n_gas_swallowed': np.full(n_files, np.nan),
                             'n_sink_swallowed': np.full(n_files, np.nan), 'redshift': np.full(n_files, np.nan),
                             'time': np.full(n_files, np.nan)}
                data_all_sink.setdefault(sink, sink_dict)

            # Store the data
            data_all_sink[sink]['mass'][i] = sink_mass
            data_all_sink[sink]['n_gas_swallowed'][i] = n_gas_swallowed
            data_all_sink[sink]['n_sink_swallowed'][i] = n_sink_swallowed
            data_all_sink[sink]['redshift'][i] = redshift
            data_all_sink[sink]['time'][i] = time

    return list_of_sink_id, data_all_sink, number_sink_per_snap


def make_plot(data_all_sink, number_sink_per_snap, sinks_id, **kwargs):
    '''
    Plot the mass vs time, the dM/dt vs time and the number of sink per snapshot. Units are M_sun, Gyr and M_sun/yr.
    '''
    dpi = kwargs.get("dpi", 300)
    image_format = kwargs.get("image_format", "png")
    figsize = kwargs.get("figsize", (5, 10))
    tight_layout = kwargs.get("tight_layout", False)
    x_min = kwargs.get("x_min", None)
    x_max = kwargs.get("x_max", None)
    y_min = kwargs.get("y_min", None)
    y_max = kwargs.get("y_max", None)

    if tight_layout:
        fig, ax = plt.subplots(num=1, nrows=1, ncols=2,
                               figsize=figsize, layout="tight")
    else:
        fig, ax = plt.subplots(num=1, nrows=1, ncols=2, figsize=figsize)

    # Get the object on which we will iterate. This avoids to iterate over all sink ID if only a subset is given
    if sinks_id is not None:
        a = sinks_id
    else:
        a = data_all_sink

    # for sink in data_all_sink:
    for sink in a:
        sink_data = data_all_sink[sink]
        mass = sink_data['mass']
        time = sink_data['time']*1e9  # Gyr conversion to yr
        accretion_rate = np.diff(mass) / np.diff(time)
        ax[0].plot(time, mass, label=sink)
        ax[1].plot(time[1:], accretion_rate, label=sink)

    ax[0].legend(ncol=1)

    if x_min is None:
        x_min = ax[0].get_xlim()[0]
    if x_max is None:
        x_max = ax[0].get_xlim()[1]

    if y_min is None:
        y_min = ax[0].get_ylim()[0]
    if y_max is None:
        y_max = ax[0].get_ylim()[1]

    ax[0].set_xlabel("Time [Gyr]")
    ax[0].set_ylabel("M [M$_\odot$]", labelpad=0)
    ax[0].set_xlim([x_min, x_max])
    ax[0].set_ylim([y_min, y_max])

    ax[1].set_xlabel("Time [Gyr]")
    ax[1].set_ylabel(
        "$\mathrm{d M}/ \mathrm{d t}$ [M$_\odot$ yr$^{-1}$]", labelpad=0)
    ax[1].set_xlim([x_min, x_max])
    # ax[1].set_yscale('symlog')

    # Save the plot
    plt.savefig(output_location + output_filename + "_sink_acretion",
                format=image_format, bbox_inches='tight', dpi=dpi)
    plt.close()

    ax_1 = ax

    #----------------------------------------------
    # Number of sinks per snapshot
    if tight_layout:
        fig, ax = plt.subplots(num=1, nrows=1, ncols=1, layout="tight")
    else:
        fig, ax = plt.subplots(num=1, nrows=1, ncols=1)

    ax.plot(time, number_sink_per_snap)
    ax.set_xlabel("Time [Gyr]")
    ax.set_ylabel("Number of sinks", labelpad=0)

    plt.savefig(output_location + output_filename + "_number_sink",
                format=image_format, bbox_inches='tight', dpi=dpi)
    plt.close()

    ax_2 = ax

    return ax_1, ax_2


# %%
def parse_option():
    description = """"
Plot the sink mass vs time, the dM/dt vs time (i.e accretion rate) and the number of sink per snapshot. Units are M_sun, Gyr and M_sun/yr.
    """
    epilog = """
Examples:
-----------
madr_plot_sink_accretion_rate.py snapshot_*.hdf5 --output_location 'SFR' -o 'h177l11'
madr_plot_sink_accretion_rate.py snapshot_*.hdf5 --sinks_id 5027975 3893234 3893229 3687238 --output_location 'SFR' -o 'h177l11'
"""
    parser = argparse.ArgumentParser(description=description, epilog=epilog,
                                     formatter_class=RawTextArgumentDefaultsHelpFormatter)

    parser.add_argument("files",
                        nargs="+",
                        type=str,
                        help="File name(s).")

    parser.add_argument("--output_location",
                        action="store",
                        type=str,
                        dest="output_location",
                        default=None,
                        help="Name of the output location")

    parser.add_argument("-o",
                        action="store",
                        type=str,
                        dest="output_filename",
                        default='',
                        help="Name of the output file. Use it if you only give ONE input file. Otherwise, "
                        "all files will have the same name.")

    parser.add_argument("-t",
                        action="store",
                        type=str,
                        dest="ftype",
                        default='swift',
                        help="Type of the output file")

    parser.add_argument("--sinks_id",
                        action=store_as_array,
                        nargs='*',
                        type=int,
                        default=None,
                        help="ID of the sink particles to plot. If nothing is given, all sinks particles are plotted.")

    # Plot options
    parser.add_argument("--x_min",
                        action="store",
                        type=float,
                        default=None,
                        help="Minimal x-axis value (physical axis, not matplotlib).")

    parser.add_argument("--x_max",
                        action="store",
                        type=float,
                        default=None,
                        help="Maximal x-axis value (physical axis, not matplotlib).")

    parser.add_argument("--y_min",
                        action="store",
                        type=float,
                        default=None,
                        help="Minimal y-axis value (physical axis, not matplotlib).")

    parser.add_argument("--y_max",
                        action="store",
                        type=float,
                        default=None,
                        help="Maximal y-axis value. (physical axis, not matplotlib)")

    parser.add_argument("--image_format",
                        action="store",
                        type=str,
                        default="png",
                        help="Format of the output image file. This must be recognized by Matplotlib.")

    parser.add_argument("--dpi",
                        action="store",
                        type=float,
                        default=300,
                        help="Output image resolution.")

    parser.add_argument("--figsize",
                        action=store_as_array,
                        nargs=2,
                        type=float,
                        default=np.array([12, 5]),
                        help="Size of the figures (width, height)")

    parser.add_argument("--tight_layout",
                        action="store_true",
                        help="Use plt.tight_layout().")

    parser.parse_args()
    args = parser.parse_args()
    files = args.files

    for f in files:
        if not os.path.exists(f):
            raise FileNotFoundError("You need to provide one file")

    return args, files


# %%

if __name__ == "__main__":
    # Parse the input
    opt, files = parse_option()

    print("\n-------------------------------------")
    print("Welcome to Plot Observable script !\n")

    output_location = opt.output_location
    output_filename = opt.output_filename

    sinks_id = opt.sinks_id

    image_format = opt.image_format
    dpi = opt.dpi
    tight_layout = opt.tight_layout
    figsize = opt.figsize
    x_min = opt.x_min
    x_max = opt.x_max
    y_min = opt.y_min
    y_max = opt.y_max

    # If no output, exit
    if output_location is None:
        output_location = ""
    else:
        if not (os.path.exists(output_location)):
            print("Output directory does not exist. It will be created.")
            os.makedirs(output_location)

        # Adds the "/" at the end if needed
        if (output_location[-1] != "/"):
            output_location = output_location+"/"

    list_of_sink_id, data_all_sink, number_sink_per_snap = get_sink_data_all_files(
        files)

    print('Here is the list of all sink particles ID. You may want to use this to choose a subset of '
          'sinks to plot. List of ID: \n {}\n'.format(list_of_sink_id))

    # Make plot
    print('Making plots...')
    make_plot(data_all_sink, number_sink_per_snap, sinks_id, dpi=dpi, image_format=image_format,
              figsize=figsize, tight_layout=tight_layout, x_min=x_min, x_max=x_max, y_min=y_min, y_max=y_max)

    print("END-------------------------------------")
