#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Mar 18 14:18:58 2024

@author: darwin
"""
import os
from mpl_toolkits.axes_grid1 import make_axes_locatable
from pNbody import *
from pNbody import Nbody
import matplotlib.pyplot as plt
import numpy as np
import argparse
from tqdm import tqdm
from MaDarwinR.scripts_utils import RawTextArgumentDefaultsHelpFormatter, store_as_array

#Ignore division errors and log(0) errors
np.seterr(divide='ignore')

# plt.style.use(SCRIPT_DIR + "/" + 'lab_style_figure.mplstyle')

# %%


def make_histogram(log_rho, log_T, weights, n_bins):
    mass_hist, log_rho_edges, log_T_edges = np.histogram2d(
        log_rho, log_T, weights=weights, density=False, bins=n_bins)
    log_mass_hist = np.log10(mass_hist).transpose()
    return log_mass_hist, log_rho_edges, log_T_edges


def make_phase_space_diagram_gas_phase(nb_gas, log_rho_edges_whole, log_T_edges_whole, **kwargs):
    title = kwargs.get("title", None)
    cmap = kwargs.get("cmap", "viridis")
    dpi = kwargs.get("dpi", 300)
    image_format = kwargs.get("image_format", "pdf")
    figsize = kwargs.get("figsize", (15, 10))
    x_min = kwargs.get("x_min", np.floor(log_rho_edges_whole[0]))
    x_max = kwargs.get("x_max", np.ceil(log_rho_edges_whole[-1]))
    y_min = kwargs.get("y_min", np.floor(log_T_edges_whole[0]))
    y_max = kwargs.get("y_max", np.ceil(log_T_edges_whole[-1]))
    v_min = kwargs.get("v_min",  None)
    v_max = kwargs.get("v_max", None)
    cut_choice = kwargs.get("cut_choice",  "percentile")
    percentile_value = kwargs.get("percentile_value",  50)
    cut_array = kwargs.get("cut_array",  None)
    T_threshold = kwargs.get("T_threshold",  None)  # K
    rho_threshold = kwargs.get("rho_threshold",  None)  # atom/cm^3

    #If running with grackle mode 0, do not do the phase space by species
    if not nb_gas.has_array("XHI"):
        return

    # Physical things
    c_array = [nb_gas.XHI, nb_gas.XHII, nb_gas.XH2I, nb_gas.XH2II,
               nb_gas.XHDI, nb_gas.XDI, nb_gas.XDII, nb_gas.XHM,
               nb_gas.XHeI, nb_gas.XHeII, nb_gas.XHeIII, nb_gas.Xe]
    subplot_title = ["HI", "HII", "H$_2$I", "H$_2$II", "HDI",
                     "DI", "DII", "H-", "HeI", "HeII", "HeIII", "e"]

    ncols = 4
    nrows = 3
    fig, axes = plt.subplots(
        num=2, ncols=ncols, nrows=nrows, figsize=figsize, layout="tight")

    # Set figure title
    if title is not None:
        fig.suptitle(title, fontsize=26)

    for i, ax_array in enumerate(axes):
        for j, ax in enumerate(ax_array):
            ax.cla()

            # Get the right 1D index from a 2D indexing
            index = np.ravel_multi_index((i, j), dims=(3, 4))

            # 2D Histogram
            c_cut = c_array[index]

            if cut_choice == "custom":
                pass
                # cut_indices = c_cut >= cut_array[index]
            elif cut_choice == "percentile":
                cut_array[index] = np.percentile(c_array[index], percentile_value)

            cut_indices = (c_cut >= cut_array[index])

            c_cut = c_cut[cut_indices]
            weights = mass_part[cut_indices]
            hist, log_rho_edges, log_T_edges = np.histogram2d(
                log_rho[cut_indices], log_T[cut_indices], weights=weights, density=False, bins=n_bins)
            log_hist = np.log10(hist).transpose()

            # Plot all 2D histgrams
            im = ax.imshow(log_hist, interpolation='none', origin='lower',
                           extent=[log_rho_edges[0], log_rho_edges[-1], log_T_edges[0], log_T_edges[-1]],
                           aspect='auto', zorder=10, cmap=cmap,
                           vmin=v_min, vmax=v_max)  # or rainbow

            # Set the limits
            ax.set_xlim([x_min, x_max])
            ax.set_ylim([y_min, y_max])

            # Add colorbar for each plot
            divider = make_axes_locatable(ax)
            cax = divider.append_axes('right', size='5%', pad=0.05)
            fig.colorbar(im, cax=cax, orientation='vertical')

            if rho_threshold is not None:
                ax.vlines(x=np.log10(rho_threshold), ymin=y_min,
                          ymax=y_max, color="royalblue")

            if T_threshold is not None:
                ax.hlines(y=np.log10(T_threshold), xmin=x_min,
                          xmax=x_max, color="royalblue")

            # Set title of each subplot
            title_final = subplot_title[index] + " (X{} ".format(
                subplot_title[index]) + "$ > {:.2e}$)".format(cut_array[index])
            ax.set_title(title_final, y=1.0, pad=-14,  fontsize=16)

            # Add the axis labels to the left and bottom plots
            if j == 0:
                ax.set_ylabel(r"$\log_{10}(\mathrm{T})$ [K]")
            else:
                ax.set_yticklabels([])
            if i == 2:
                ax.set_xlabel(r"$\log_{10}(\rho)$ [atom/cm$^3$]")
            else:
                ax.set_xticklabels([])

    # Adjust spacing between subplots
    # fig.subplots_adjust(wspace=0.0)
    fig.subplots_adjust(hspace=0.01)

    # Save figure
    plt.savefig(output_location + output_filename +
                "_phase_space_diagram_gas_phases." + image_format, format=image_format, bbox_inches='tight', dpi=dpi)
    plt.close()
    return ax

def make_plot_phase_space(log_mass_hist, log_rho_edges, log_T_edges, **kwargs):
    title = kwargs.get("title", None)
    cmap = kwargs.get("cmap", "viridis")
    dpi = kwargs.get("dpi", 300)
    image_format = kwargs.get("image_format", "pdf")
    figsize = kwargs.get("figsize", (5, 5))
    x_min = kwargs.get("x_min", np.floor(log_rho_edges[0]))
    x_max = kwargs.get("x_max", np.ceil(log_rho_edges[-1]))
    y_min = kwargs.get("y_min", np.floor(log_T_edges[0]))
    y_max = kwargs.get("y_max", np.ceil(log_T_edges[-1]))
    v_min = kwargs.get("v_min",  None)
    v_max = kwargs.get("v_max", None)

    fig, ax = plt.subplots(ncols=1, nrows=1, figsize=figsize, layout="tight")
    ax.cla()
    im = ax.imshow(log_mass_hist, interpolation='none', origin='lower',
                   extent=[log_rho_edges[0], log_rho_edges[-1], log_T_edges[0], log_T_edges[-1]],
                   aspect='auto', zorder=10, cmap=cmap,
                   vmin=v_min, vmax=v_max)  # or rainbow

    if rho_threshold is not None:
        ax.vlines(x=np.log10(rho_threshold), ymin=y_min,
                  ymax=y_max, color="royalblue")

    if T_threshold is not None:
        ax.hlines(y=np.log10(T_threshold), xmin=x_min,
                  xmax=x_max, color="royalblue")

    ax.set_ylabel(r"$\log_{10}(\mathrm{T})$ [K]")
    ax.set_xlabel(r"$\log_{10}(\rho)$ [atom/cm$^3$]")
    ax.set_xlim([x_min, x_max])
    ax.set_ylim([y_min, y_max])

    if title is not None:
        ax.set_title(title)

    # Add colorbar to ax[0]
    divider = make_axes_locatable(ax)
    cax = divider.append_axes('right', size='5%', pad=0.05)
    fig.colorbar(im, cax=cax, orientation='vertical',
                 label=r"$\log_{10}(M)$ [M$_\odot$]")

    # Save figure
    plt.savefig(output_location + output_filename +
                "_phase_space_diagram." + image_format, format=image_format, bbox_inches='tight', dpi=dpi)
    plt.close()
    return ax


# %%

def parse_option():
    description = """"
Plot the log(rho) log(T) diagram
    - for the entire gas,
    - for each gas specie.

The gas specie order is the following:HI, HII, H2I, H2II, HDI, DI, DII, H-, HeI, HeII, HeIII, e.
"""
    epilog = """
Examples:
--------
madr_plot_phase_diagram.py snapshot_*.hdf6 --output_location h177l11_UVB --cut_choice="percentile" --percentile_value=75 --dpi=300

"""
    parser = argparse.ArgumentParser(description=description, epilog=epilog,
                                     formatter_class=RawTextArgumentDefaultsHelpFormatter)

    parser.add_argument("files",
                        nargs="+",
                        type=str,
                        help="File name(s).")

    parser.add_argument("--output_location",
                        action="store",
                        type=str,
                        dest="output_location",
                        default=None,
                        help="Name of the output location")

    parser.add_argument("-o",
                        action="store",
                        type=str,
                        dest="output_filename",
                        default=None,
                        help="Name of the output file. Use it if you only give ONE input file. Otherwise, "
                        "all files will have the same name.")

    parser.add_argument("-t",
                        action="store",
                        type=str,
                        dest="ftype",
                        default='swift',
                        help="Type of the output file")

    # Particle parameters
    parser.add_argument("--n_bins",
                        action="store",
                        type=int,
                        default=100,
                        help="Number of bins for the histogram IMF.")

    #Plot parameters
    parser.add_argument("--image_format",
                        action="store",
                        type=str,
                        default="png",
                        help="Format of the output image file. This must be recognized by Matplotlib.")

    parser.add_argument("--dpi",
                        action="store",
                        type=float,
                        default=300,
                        help="Output image resolution.")

    parser.add_argument("--x_min",
                        action="store",
                        type=float,
                        default=-7,
                        help="Minimal x-axis value.")

    parser.add_argument("--x_max",
                        action="store",
                        type=float,
                        default=7,
                        help="Maximal x-axis value.")

    parser.add_argument("--y_min",
                        action="store",
                        type=float,
                        default=0,
                        help="Minimal y-axis value.")

    parser.add_argument("--y_max",
                        action="store",
                        type=float,
                        default=8,
                        help="Maximal y-axis value.")

    parser.add_argument("--v_min",
                        action="store",
                        type=float,
                        default=None,
                        help="Minimal colormap value.")

    parser.add_argument("--v_max",
                        action="store",
                        type=float,
                        default=None,
                        help="Maximal colorbar value.")

    parser.add_argument("--cmap",
                        action="store",
                        type=str,
                        default="magma",
                        help="Color map. It must be Matplolib compliant.")

    parser.add_argument("--T_threshold",
                        action="store",
                        type=float,
                        default=None,
                        help="Threshold temperature set in the simulations for star formation. In Kelvin.")

    parser.add_argument("--rho_threshold",
                        action="store",
                        type=float,
                        default=None,
                        help="Threshold density set in the simulations for star formation. In atom/cm^3.")

    parser.add_argument("--cut_choice",
                        action="store",
                        choices=["custom", "percentile"],
                        type=str,
                        default="custom",
                        help="Type of selection cut to apply to the phase space diagram by gas elements. "
                        "If 'custom' is selected, you must provide an array with 12 elements. If '"
                        "percentile' is chosen, you must specify the percentile (e.g. 50 for the median) "
                        "above which gas fraction of all species will be considered for plotting.")

    parser.add_argument("--cut_array",
                        action=store_as_array,
                        nargs=12,
                        type=float,
                        default=np.array(
                            [0.75, 1e-1, 1e-4, 1e-8, 1e-5, 1e-5, 1e-5, 1e-8, 0.24, 1e-5, 0.1,  1e-1]),
                        help="Custo cut of the fraction for each gas specie above which we should plot this gas specie. "
                        "The gas species order is the following:HI, HII, H2I, H2II, HDI, DI, DII, H-, HeI, HeII, HeIII, e.")

    parser.add_argument("--percentile_value",
                        action="store",
                        type=float,
                        default=50,
                        help="Percentile fraction of the gas specie above which we should plot all gas species.")

    parser.add_argument("--stylesheet",
                        action="store",
                        type=str,
                        default=None,
                        help="Matplotlib stylesheet.")

    parser.parse_args()
    args = parser.parse_args()
    files = args.files

    for f in files:
        if not os.path.exists(f):
            raise FileNotFoundError("You need to provide one file")

    return args, files


# %% Main

if __name__ == "__main__":
    # Parse the input
    opt, files = parse_option()

    print("\n-------------------------------------")
    print("Welcome to Phase space script !\n")

    output_location = opt.output_location
    output_filename = opt.output_filename
    n_bins = opt.n_bins
    image_format = opt.image_format
    dpi = opt.dpi
    x_min = opt.x_min
    x_max = opt.x_max
    y_min = opt.y_min
    y_max = opt.y_max
    v_min = opt.v_min
    v_max = opt.v_max
    cmap = opt.cmap
    T_threshold = opt.T_threshold
    rho_threshold = opt.rho_threshold
    cut_choice = opt.cut_choice
    cut_array = opt.cut_array
    percentile_value = opt.percentile_value
    stylesheet = opt.stylesheet

    if stylesheet is not None:
        plt.style.use(stylesheet)


    # If no output, exit
    if output_location is None:
        output_location = ""
    else:
        if not (os.path.exists(output_location)):
            print("Output directory does not exist. It will be created.")
            os.makedirs(output_location)

        # Adds the "/" at the end if needed
        if (output_location[-1] != "/"):
            output_location = output_location+"/"

    for filename in tqdm(files):

        # If we do not give output_filename
        # Notice that I keep opt.output_filename, otherwise this condition is not fullfilled for the next
        # iteration
        if opt.output_filename is None:
            # Keeps the base name (without /path/to/file) and without extension .hdf5
            output_filename = os.path.split(filename)[1]
            output_filename = os.path.splitext(output_filename)[0]

        # Select the object
        print("Loading the data...")
        nb_gas = Nbody(p_name=filename, ptypes=[0])

        # Do not continue if you do not find stars.
        if nb_gas.npart[0] == 0:
            print("No gas found in this file. Continue to the next file...")
            continue

        # Prepare the first phase space diagram
        log_rho = np.log10(nb_gas.Rho(units="acc"))
        log_T = np.log10(nb_gas.T())

        # Plot phase space diagram colored by mass
        print("Computing phase space diagram for the entire gas...")
        mass_part = nb_gas.Mass(units="Msol")
        title = r"$z=" + "{:.2f}".format(nb_gas.redshift) + "$"
        log_mass_hist, log_rho_edges, log_T_edges = make_histogram(
            log_rho, log_T, mass_part, n_bins)
        make_plot_phase_space(log_mass_hist, log_rho_edges, log_T_edges, cmap=cmap, x_min=x_min,
                              x_max=x_max, y_min=y_min, y_max=y_max, image_format=image_format,
                              title=title, v_min=v_min, v_max=v_max)

        # Plot the phase space diagram colored by mass, for each gas species
        print("Computing phase space diagram for each gas species...")
        make_phase_space_diagram_gas_phase(nb_gas, log_rho_edges, log_T_edges, cmap=cmap, x_min=x_min,
                                           x_max = x_max, y_min = y_min, y_max = y_max, image_format=image_format,
                                           title=title, cut_choice=cut_choice, percentile_value=percentile_value, dpi=dpi,
                                           cut_array=cut_array, T_threshold=T_threshold, rho_threshold=rho_threshold,
                                           v_min=v_min, v_max=v_max)

    print("END-------------------------------------")
