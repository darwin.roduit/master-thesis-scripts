#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jun  3 09:16:10 2024
This script produces a projected density plot at different times of the Boss & Bodenheimer test.
It is used fir my master thesis.

The content should be improved to be more modularised.

@author: Darwin Roduit
"""

import os
import swiftsimio as sw
import numpy as np
import matplotlib.pyplot as plt
import unyt
from swiftsimio.visualisation.projection import project_gas
from unyt import msun, kpc
from matplotlib.ticker import MaxNLocator
from tqdm import tqdm

plt.style.use('../lab_style_figure.mplstyle')

#%%

directory = "/run/media/darwinr/Seagate/Documents/new_sim"
simulation = ["h177l11_old_fb", "h177l11_new_fb", "h177l11_sn_efficiency_0_2" ,"h177l11_no_popIII" ]

#Old feedback sim alone
# simulation = ["h177l11_old_fb"]
snap_dir = "snap"

snapshot = [
["snapshot_0170.hdf5", "snapshot_0175.hdf5", "snapshot_0200.hdf5", "snapshot_0350.hdf5"],
["snapshot_0130.hdf5", "snapshot_0152.hdf5", "snapshot_0250.hdf5", "snapshot_0525.hdf5"],
["snapshot_0200.hdf5", "snapshot_0220.hdf5", "snapshot_0350.hdf5", "snapshot_0450.hdf5"],
["snapshot_0270.hdf5", "snapshot_0290.hdf5", "snapshot_0400.hdf5", "snapshot_0425.hdf5"]
]

N_sim = len(simulation)

#%%

projected_density_list = []
time_list = []
redshift_list = []
N_star_list = []
x_star_list = []
y_star_list = []

v_min = np.inf
v_max = - np.inf

for i, sim in enumerate(tqdm(simulation)):
    projected_density_sim = []
    time_sim = []
    redshift_sim = []
    N_star_sim = []
    x_star_sim = []
    y_star_sim = []
    for j, snap in enumerate(tqdm(snapshot[i])):
        filename = os.path.join(directory, sim, snap_dir, snap)

        print(filename)

        #Create a mask
        mask = sw.mask(filename)
        load_region = [[2400*kpc, 2750*kpc], [2200*kpc, 2600*kpc], [2400*kpc, 2700*kpc]]

        #Old feedback sim alone
        # load_region = [[2450*kpc, 2650*kpc], [2300*kpc, 2400*kpc], [2400*kpc, 2700*kpc]]

        # Constrain the mask
        mask.constrain_spatial(load_region)

        #Maks the data
        data = sw.load(filename, mask=mask)
        # data = sw.load(filename)

        boxsize = data.metadata.boxsize
        boxsize.convert_to_units(kpc)

        #Compute projected density
        projected_density = project_gas(
            data,
            resolution=8000,
            project="masses",
            parallel=True,
            periodic=False,
        ).T

        #Convert to cgs
        projected_density.convert_to_cgs()

        #Units conversion
        data.gas.densities.convert_to_cgs()
        data.gas.coordinates.convert_to_units(unyt.au)
        # data.sinks.coordinates.convert_to_units(unyt.au)

        #Get simulation time
        time = data.metadata.t
        time.convert_to_units(unyt.kyr)
        redshift = data.metadata.redshift


        projected_density_sim.append(projected_density)
        time_sim.append(time)
        redshift_sim.append(redshift)
        N_star_sim.append(data.metadata.n_stars)

        x_star_sim.append(data.stars.coordinates[:, 0])
        y_star_sim.append(data.stars.coordinates[:, 1])

        # v_min = min(projected_density, v_min)

    #Append to the list
    projected_density_list.append(projected_density_sim)
    time_list.append(time_sim)
    redshift_list.append(redshift_sim)
    N_star_list.append(N_star_sim)
    x_star_list.append(x_star_sim)
    y_star_list.append(y_star_sim)

#%%
#All sim together
x_min = 2350
x_max = 2750
y_min = 2200
y_max = 2600 #*unyt.au

figsize = (10,12)
figsize = (10,15.0/4.0*N_sim)
# fig, axes = plt.subplots(ncols=4, nrows=N_sim, figsize=figsize, layout="tight", num=2)
fig, axes = plt.subplots(ncols=4, nrows=N_sim, figsize=figsize, num=2)

# Calculate the min and max of all projected_density values
v_min = -9
v_max = -5
for i, sim in enumerate(simulation):
    projected_density_sim = projected_density_list[i]
    time_sim = time_list[i]
    redshift_sim = redshift_list[i]
    N_star_sim = N_star_list[i]
    x_star_sim = x_star_list[i]
    y_star_sim = y_star_list[i]

    for j, snap in enumerate(snapshot):
        if axes.ndim == 1:
            ax = axes[j]
        elif axes.ndim == 2:
            ax = axes[i, j]
        projected_density = np.log10(projected_density_sim[j].value)
        time = time_sim[j]
        redshift = redshift_sim[j]
        N_star = N_star_sim[j]
        x_star = x_star_sim[j].value
        y_star = y_star_sim[j].value

        #Plot...
        im = ax.imshow(projected_density, origin='lower',  zorder = 1, #interpolation='None',
                            extent=[0, boxsize[0], 0, boxsize[1]], aspect='equal',
                            cmap = 'inferno', vmin=v_min, vmax=v_max)

        ax.scatter(x_star, y_star, c='limegreen', zorder=1, marker='.')

        #Add some limits for better visualisation
        ax.set_xlim([x_min, x_max])
        ax.set_ylim([y_min, y_max])

        #Add labels
        #Add y labels only for the left subplots
        if j == 0:
            ax.set_ylabel("$y$ [ckpc]", labelpad=0.2)
            ax.yaxis.set_major_locator(MaxNLocator(nbins=3))
        else:
            ax.set_yticks([])

        #Add x labels only for the bottom subplots
        if i ==3:
            ax.set_xlabel("$x$ [ckpc]", labelpad=0.2)
            ax.xaxis.set_major_locator(MaxNLocator(nbins=3))
        else:
            ax.set_xticks([])


        #Add some text annotations
        ax.text(0.1, 0.9, "$z = {:.2f}".format(redshift) +"$", transform=ax.transAxes, fontsize=12, bbox=dict(facecolor='white', alpha=0.8))
        ax.text(0.55, 0.9, "$N_{\mathrm{star}}" + " = {}$".format(N_star), transform=ax.transAxes, fontsize=10, bbox=dict(facecolor='white', alpha=0.8))


plt.subplots_adjust(wspace=0.01, hspace=0.01)

# Add a colorbar for all subplots
cbar_ax = fig.add_axes([0.125, 0.85, 0.77, 0.05])  # Adjust these values to position the colorbar
cbar = fig.colorbar(im, cax=cbar_ax, orientation='horizontal')
cbar.set_label('$\log_{10}(\Sigma$ [g/cm$^2$]$)$', labelpad=2)
cbar.ax.xaxis.set_label_position('top')
cbar.ax.xaxis.set_ticks_position('top')

plt.subplots_adjust(top=0.85)  # Adjust the top to make space for the colorbar

#%%
# plt.savefig("zoom_density_evolution.pdf", format='pdf', bbox_inches='tight', dpi=300)

