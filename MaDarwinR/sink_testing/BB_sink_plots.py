#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jun  3 14:15:17 2024

@author: Darwin Roduit
"""

import os
import swiftsimio as sw
import numpy as np
import matplotlib.pyplot as plt
import unyt
from swiftsimio.visualisation.projection import project_gas
from unyt import msun, au
from tqdm import tqdm
import matplotlib.cm as cm

from MaDarwinR.snapshots_utils import get_all_snapshot_in_directory

plt.style.use('../lab_style_figure.mplstyle')

# %%


def get_sink_data(sink, data):
    """
    Extract data for a given sink particle at a given snapshot.

    Parameters
    ----------
    sink : int
        ID of the sink we want to extract the data.
    sinks : swiftsimio.reader.SinksDataset
        Sink object of the given snapshot.

    Returns
    -------
    sink_mass : float
        Mass of sink. In M_sun.
    n_gas_swallowed : int
        Number of gas particles swallowed by sink.
    n_sink_swallowed : int
        Number of sink particles swallowed by sink..
    redshift : float
        Redshift of the current snapshot.
    time : float
        Time of the current snapshot. In Gyr.

    """
    sinks = data.sinks

    # Look in the nb fo the position of the sink in the arrays
    sink_index = np.argwhere(sinks.particle_ids == sink).ravel()

    # If sink is not found, end here
    if sink_index.size == 0:
        sink_mass = np.NaN
        n_gas_swallowed = np.NaN
        n_sink_swallowed = np.NaN
        redshift = np.NaN
        time = np.NaN
    else:
        # The [0] is to onvert the 1D array to scalar
        sink_mass = sinks.masses.to(unyt.msun)[sink_index][0]
        n_gas_swallowed = sinks.number_of_gas_swallows[sink_index][0]
        n_sink_swallowed = sinks.number_of_sink_swallows[sink_index][0]
        redshift = data.metadata.redshift
        time = data.metadata.t.to(unyt.kyr)
    return sink_mass, n_gas_swallowed, n_sink_swallowed, redshift, time


def get_sink_data_all_files(filenames):
    """
    Retrieve mass, n_gas_swallowed, n_sink_swallowed, redshift, time for all sink in all given files.

    Parameters
    ----------
    filenames : list of string
        List of the snapshots to analyse.

    Returns
    -------
    list_of_sink_id : set of int
        Set containing all sink IDs in the simulation.
    data_all_sink : dictionary with key:sink_id, val=dictionary of data
        Contains the data of all sinks in the simulation, The 'val' dictionary contains the following
        keys: mass, n_gas_swallowed, n_sink_swallowed, redshift, time. Units are M_sun and Gyr.
    number_sink_per_snap : array of int
        Number of sinks per snapshot.

    """
    # id_sink = 3
    n_files = len(filenames)

    # Set of all sinks
    list_of_sink_id = set()
    data_all_sink = dict()
    number_sink_per_snap = np.zeros(n_files)
    time_array = np.zeros(n_files, dtype=unyt.unyt_quantity)

    for i, file in enumerate(tqdm(filenames)):
        # print(file)
        # nb = Nbody(file, ptypes=[id_sink])

        # Maks the data
        data = sw.load(file)

        data.sinks.masses.convert_to_physical()

        N_sink = data.metadata.n_sinks
        time_array[i] = data.metadata.t.to(unyt.kyr)
        # print(data.metadata.t)

        if N_sink == 0:
            print("No sink particles found in this file. Continue to the next file")
            continue

        # Put the sink id (num) in a set so that we do not have duplicate sinks

        for sink_num in data.sinks.particle_ids.value:
            number_sink_per_snap[i] = N_sink
            list_of_sink_id.add(sink_num)

        # Get the data
        # Now, iterate over all sinks in the set to get the data contained in this snapshot
        for sink in list_of_sink_id:
            sink_mass, n_gas_swallowed, n_sink_swallowed, redshift, time = get_sink_data(
                sink, data)

            # Store this in the data_all_sink[sink] dictionary
            if sink not in data_all_sink:
                # I prefer to put Nan so that we can disthinguish true 0 values from the case where the sink
                # does not exist and thus has no value
                sink_dict = {'mass': np.full(n_files, np.nan), 'n_gas_swallowed': np.full(n_files, np.nan),
                             'n_sink_swallowed': np.full(n_files, np.nan), 'redshift': np.full(n_files, np.nan),
                             'time': np.full(n_files, np.nan)}
                data_all_sink.setdefault(sink, sink_dict)

            # Store the data
            # print(sink_mass)
            data_all_sink[sink]['mass'][i] = sink_mass
            data_all_sink[sink]['n_gas_swallowed'][i] = n_gas_swallowed
            data_all_sink[sink]['n_sink_swallowed'][i] = n_sink_swallowed
            data_all_sink[sink]['redshift'][i] = redshift
            data_all_sink[sink]['time'][i] = time

    return list_of_sink_id, data_all_sink, number_sink_per_snap, time_array


# %%
BB_directory = "/run/media/darwinr/Seagate/Documents/BB_test"
simulations = ["S01", "S02", "S03", "S04", "S05", "S06", "S07", "S08", "S09"]
snap_dir = "snap"

# snapshot = ["snapshot_0006.hdf5", "snapshot_0012.hdf5", "snapshot_0018.hdf5", "snapshot_0023.hdf5"]
# files = [os.path.join(BB_directory, simulation, snap_dir, snap) for snap in snapshot]

# files = get_all_snapshot_in_directory(directory=os.path.join(BB_directory, simulation, snap_dir), snap_basename='snapshot')

t_ff = 17.492*unyt.kyr

# %% Get the data

time_list = []
N_sink_list = []
list_of_sink_id_list = []
data_all_sink_list = []

for i, sim in enumerate(simulations):
    files = get_all_snapshot_in_directory(directory=os.path.join(
        BB_directory, sim, snap_dir), snap_basename='snapshot')
    list_of_sink_id, data_all_sink, number_sink_per_snap, time = get_sink_data_all_files(
        files)

    list_of_sink_id_list.append(list_of_sink_id)
    data_all_sink_list.append(data_all_sink)
    N_sink_list.append(number_sink_per_snap)
    time_list.append(time)
    # print('\nHere is the list of all sink particles ID. You may want to use this to choose a subset of '
          # 'sinks to plot. List of ID: \n {}\n'.format(list_of_sink_id))


# %% Number of sinks per snapshot
import random

figsize = (15, 5)
linestyles = ['-', ':', '-.', '--']

# cmap = cm.get_cmap('rainbow', 9)
# colors = [cmap(i) for i in range(cmap.N)]
colors = ['tomato', 'sandybrown', 'gold', 'limegreen', 'seagreen', 'lightskyblue', 'royalblue', 'blueviolet', 'hotpink']

# colors.reverse()

# Randomly shuffle the list of colors
# random.shuffle(colors)



fig, axes = plt.subplots(num=1, nrows=1, ncols=3, figsize=figsize)

for i, sim in enumerate(simulations):
    time = time_list[i]
    N_sink = N_sink_list[i]
    axes[0].plot(time, N_sink, label=sim, color=colors[i], linestyle='-')

axes[0].set_xlabel("Time [kyr]")
axes[0].set_ylabel("Number of sinks", labelpad=0)
axes[0].set_yscale('symlog')

# Accretion rate

id_list_all_sim = []  # For all simulations

for i, sim in enumerate(simulations):
    data_all_sink = data_all_sink_list[i]
    id_list = []  # For one sim
    for sink in data_all_sink:
        sink_data = data_all_sink[sink]
        mass = sink_data['mass']
        if (np.any(mass > 4e-2)):
            id_list.append(sink)

    id_list_all_sim.append(id_list)

# sinks_id = [967533, 620183]

# Get the object on which we will iterate. This avoids to iterate over all sink ID if only a subset is given

for i, sim in enumerate(simulations):
    sinks_id = id_list_all_sim[i]
    data_all_sink = data_all_sink_list[i]
    if sinks_id is not None:
        a = sinks_id
    else:
        a = data_all_sink

    for j, sink in enumerate(a):
        sink_data = data_all_sink[sink]
        mass = sink_data['mass']
        time = sink_data['time']  # Gyr conversion to yr
        accretion_rate = np.diff(mass) / np.diff(time)
        axes[1].plot(time, mass, label=sink, color=colors[i],
                     linestyle=linestyles[j])
        axes[2].plot(time[1:], accretion_rate, label=sink,
                     color=colors[i], linestyle=linestyles[j])


axes[1].set_xlabel("Time [kyr]")
axes[1].set_ylabel("M [M$_\odot$]", labelpad=0)
axes[2].set_xlabel("Time [kyr]")
axes[2].set_ylabel(
    "$\mathrm{d M}/ \mathrm{d t}$ [M$_\odot$ yr$^{-1}$]", labelpad=0)


# Create custom legend handles for colors
color_handles = [plt.Line2D([0], [0], color=color, lw=2) for color in colors]
color_labels = simulations

# Create custom legend handles for linestyles
linestyle_handles = [plt.Line2D(
    [0], [0], color='black', linestyle=linestyle, lw=2) for linestyle in linestyles]
linestyle_labels = ['1', '2']

# Add legends to the plot
axes[0].legend(handles=color_handles, labels=color_labels,
               title="Simulations", loc='upper left')
axes[1].legend(handles=linestyle_handles, labels=linestyle_labels,
               title="Sinks", loc='lower right')

#%%
plt.savefig("BB_sink_plots.pdf", format='pdf', bbox_inches='tight', dpi=300)
