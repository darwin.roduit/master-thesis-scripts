#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jun  3 09:16:10 2024
This script produces a projected density plot at different times of the Boss & Bodenheimer test.
It is used fir my master thesis.

The content should be improved to be more modularised.

@author: Darwin Roduit
"""

import os
import swiftsimio as sw
import numpy as np
import matplotlib.pyplot as plt
import unyt
from swiftsimio.visualisation.projection import project_gas
from unyt import msun, au
from matplotlib.ticker import MaxNLocator

plt.style.use('../lab_style_figure.mplstyle')
#%%

def get_gas_mean_weight(data):
    '''
    Return the mean molecular weight of the gas
    '''

    from unyt.physical_constants import mh

    #Get the cooling model
    cooling = data.metadata.subgrid_scheme["Cooling Model"]

    #Use a variable for better readability
    gas = data.gas

    #Get rho
    gas.densities.convert_to_cgs()
    rho =  gas.densities #self.Rho(units='g/cm3')

    # hydrogen mass in gram
    mh.convert_to_cgs()
    mH_in_g = mh

    print(mH_in_g)

    if cooling == b'Grackle3':                                           #grackle3
        print("... found info for grackle mode=3")
        nHI = gas.hi * rho / (mH_in_g)
        nHII = gas.hii * rho / (mH_in_g)
        nHeI = gas.he_i * rho / (4 * mH_in_g)
        nHeII = gas.he_ii * rho / (4 * mH_in_g)
        nHeIII = gas.he_iii * rho / (4 * mH_in_g)
        nH2I = gas.h2_i * rho / (2 * mH_in_g)
        nH2II = gas.h2_ii * rho / (2 * mH_in_g)
        nHDI = gas.hdi * rho / (3 * mH_in_g)
        nel = nHII + nHeII + 2 * nHeIII + nH2II
        mu = ((nHI + nHII) + (nHeI + nHeII + nHeIII) * 4 + (nH2I + nH2II) * 2 + nHDI * 3) / (nHI + nHII + nHeI + nHeII + nHeIII + nH2I + nH2II + nHDI + nel)
        return mu

    elif cooling == b'Grackle2':                                         #grackle2
        print("... found info for grackle mode=2")
        nHI = self.XHI * rho / (mH_in_g)
        nHII = self.XHII * rho / (mH_in_g)
        nHeI = self.XHeI * rho / (4 * mH_in_g)
        nHeII = self.XHeII * rho / (4 * mH_in_g)
        nHeIII = self.XHeIII * rho / (4 * mH_in_g)
        nH2I = self.XH2I * rho / (2 * mH_in_g)
        nH2II = self.XH2II * rho / (2 * mH_in_g)

        nHI = gas.hi * rho / (mH_in_g)
        nHII = gas.hii * rho / (mH_in_g)
        nHeI = gas.he_i * rho / (4 * mH_in_g)
        nHeII = gas.he_ii * rho / (4 * mH_in_g)
        nHeIII = gas.he_iii * rho / (4 * mH_in_g)
        nH2I = gas.h2_i * rho / (2 * mH_in_g)
        nH2II = gas.h2_ii * rho / (2 * mH_in_g)
        mu = ((nHI + nHII) + (nHeI + nHeII + nHeIII) * 4 + (nH2I + nH2II) * 2) / (nHI + nHII + nHeI + nHeII + nHeIII + nH2I + nH2II + nel)
        return mu

    elif cooling == b'Grackle1':                                           #grackle1
        print("... found info for grackle mode=1")
        nHI = gas.hi * rho / (mH_in_g)
        nHII = gas.hii * rho / (mH_in_g)
        nHeI = gas.he_i * rho / (4 * mH_in_g)
        nHeII = gas.he_ii * rho / (4 * mH_in_g)
        nHeIII = gas.he_iii * rho / (4 * mH_in_g)
        nel = nHII + nHeII + 2 * nHeIII
        mu = ((nHI + nHII) + (nHeI + nHeII + nHeIII) * 4) / (nHI + nHII + nHeI + nHeII + nHeIII + nel)
        return mu

    else: #Grackle0
            return 1.2 #Don't know yet how to do in this case
    #     if ionized:
    #         mu = 4. / (8. - 5. * (1. - Xi))
    #     else:
    #         mu = 4. / (1. + 3. * Xi)
    #     return mu

# def get_gas_temperature(data):
#     # def T(self,a=None,h=None,units=None):
#       '''
#       u does not depends on a nor h
#       '''
#       from astropy import units as u
#       from astropy import constants as c

#       k_in_erg_K = c.k_B.to(u.erg/u.K).value

#       mh         = ctes.PROTONMASS.into(self.localsystem_of_units)
#       gamma      = self.unitsparameters.get('gamma')

#       if self.has_array("XHI"):
#         mu = self.GasMeanWeight()
#         u = self.InternalEnergy(a=a,h=h,units='erg')
#         T = mu * (gamma - 1.0) * u * mh / k_in_erg_K
#         return T

#       else:

#         gamma      = self.unitsparameters.get('gamma')
#         xi         = self.unitsparameters.get('xi')
#         ionisation = self.unitsparameters.get('ionisation')
#         mu         = thermodyn.MeanWeight(xi,ionisation)
#         mh         = ctes.PROTONMASS.into(self.localsystem_of_units)
#         k          = ctes.BOLTZMANN.into(self.localsystem_of_units)


#         thermopars = {"k":k,"mh":mh,"mu":mu,"gamma":gamma}

#         # compute internal energy
#         u = self.InternalEnergy(a=a,h=h,units=units)

#         # this is the old implementation, avoid the computation of the ionization state
#         #T = where((u>0),(gamma-1.)* (mu*mh)/k * u,0)

#         # this is the new implementation, but may take much more time
#         T = np.where((u>0),thermodyn.Tru(u,thermopars),0)

#         # return T


#     return 1.0



#%%

BB_directory = "/run/media/darwinr/Seagate/Documents/BB_test"
simulation = "S01"
snap_dir = "snap"

snapshot = ["snapshot_0009.hdf5", "snapshot_0012.hdf5", "snapshot_0018.hdf5", "snapshot_0023.hdf5"]

t_ff =  17.492*unyt.kyr

#%%


projected_density_list = []
time_list = []
N_sink_list = []
x_sink_list = []
y_sink_list = []

for i, snap in enumerate(snapshot):
    filename = os.path.join(BB_directory, simulation, snap_dir, snap)

    print(filename)

    #Create a mask
    mask = sw.mask(filename)
    boxsize = mask.metadata.boxsize
    boxsize.convert_to_units(au)
    load_region = [[8000*au, 12000*au], [8000*au, 12000*au], [9000*au, 11000*au]]

    # Constrain the mask
    mask.constrain_spatial(load_region)

    #Maks the data
    data = sw.load(filename, mask=mask)

    #Compute projected density
    projected_density = project_gas(
        data,
        resolution=10000,
        project="masses",
        parallel=True,
        periodic=False,
    ).T

    #Convert to cgs
    projected_density.convert_to_cgs()

    #Units conversion
    data.gas.densities.convert_to_cgs()
    data.gas.coordinates.convert_to_units(unyt.au)
    # data.sinks.coordinates.convert_to_units(unyt.au)

    #Get simulation time
    time = data.metadata.t
    time.convert_to_units(unyt.kyr)

    #Get number of sinks
    N_sink = data.metadata.n_sinks

    projected_density_list.append(projected_density)
    time_list.append(time)
    N_sink_list.append(N_sink)

    x_sink_list.append(data.sinks.coordinates[:, 0])
    y_sink_list.append(data.sinks.coordinates[:, 1])


#%%

x_min = 9250
x_max = 10750
y_min = 9250
y_max = 10750 #*unyt.au

figsize = (15,5)
# fig, axes = plt.subplots(ncols=4, nrows=1, figsize=figsize, layout="tight", num=1)
fig, axes = plt.subplots(ncols=4, nrows=1, figsize=figsize, num=1)

# Calculate the min and max of all projected_density values
v_min = -1 #min(density.min() for density in projected_density_list).value
v_max = None  # max(density.max() for density in projected_density_list).value

for i, snap in enumerate(snapshot):
    ax = axes[i]
    projected_density = np.log10(projected_density_list[i].value)
    time = time_list[i]
    N_sink = N_sink_list[i]

    #Plot...
    im = ax.imshow(projected_density, origin='lower',  zorder = 1, interpolation='None',
                        extent=[0, boxsize[0], 0, boxsize[1]], aspect='equal',
                        cmap = 'inferno', vmin=v_min, vmax=v_max)

    ax.scatter(x_sink_list[i].value, y_sink_list[i].value, c='limegreen', zorder=1, marker='.')

    #Add some limits for better visualisation
    ax.set_xlim([x_min, x_max])
    ax.set_ylim([y_min, y_max])

    #Add labels
    ax.set_xlabel("$x$ [au]")

    #Add y labels only for the left subplot
    if i == 0:
        ax.set_ylabel("$y$ [au]", labelpad=0.2)
        ax.yaxis.set_major_locator(MaxNLocator(nbins=3))
    else:
        ax.set_yticks([])

    #Put less ticks
    ax.xaxis.set_major_locator(MaxNLocator(nbins=3))


    #Add some text annotations

    ax.text(0.1, 0.95, "$t = {:.2f}".format(time.value/t_ff.value) +" t_{\mathrm{ff}}$", transform=ax.transAxes, fontsize=12, bbox=dict(facecolor='white', alpha=0.8))
    ax.text(0.65, 0.95, "$N_{\mathrm{sink}}" + " = {}$".format(N_sink), transform=ax.transAxes, fontsize=12, bbox=dict(facecolor='white', alpha=0.8))


plt.subplots_adjust(wspace=0.01, hspace=0.01)

# Add a colorbar for all subplots
cbar_ax = fig.add_axes([0.125, 0.9, 0.775, 0.05])  # Adjust these values to position the colorbar
cbar = fig.colorbar(im, cax=cbar_ax, orientation='horizontal')
cbar.set_label('$\log_{10}(\Sigma$ [g/cm$^2$]$)$', labelpad=1)
cbar.ax.xaxis.set_label_position('top')
cbar.ax.xaxis.set_ticks_position('top')

plt.subplots_adjust(top=0.85)  # Adjust the top to make space for the colorbar

plt.savefig("BB_1_density_evolution.pdf", format='pdf', bbox_inches='tight', dpi=300)

