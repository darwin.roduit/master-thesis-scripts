#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jun  3 09:16:10 2024
This script produces a projected density plot at different times of the Cloud collapse test.
It is used for my master thesis.

The content should be improved to be more modularised so that it is not duplicated from the
BB_density_plot.py

@author: Darwin Roduit
"""

import os
import swiftsimio as sw
import numpy as np
import matplotlib.pyplot as plt
import unyt
from swiftsimio.visualisation.projection import project_gas
from unyt import msun, au
from matplotlib.ticker import MaxNLocator

plt.style.use('../lab_style_figure.mplstyle')
#%%

directory = "/run/media/darwinr/Seagate/Documents/cloud_collapse"
simulation = "S01"
snap_dir = "snap"

snapshot = ["snapshot_0002.hdf5", "snapshot_0006.hdf5", "snapshot_0010.hdf5", "snapshot_0014.hdf5"]

t_ff = 50.2*unyt.kyr

#%%


projected_density_list = []
time_list = []
N_sink_list = []
x_sink_list = []
y_sink_list = []

for i, snap in enumerate(snapshot):
    filename = os.path.join(directory, simulation, snap_dir, snap)

    print(filename)

    #Create a mask
    mask = sw.mask(filename)
    boxsize = mask.metadata.boxsize
    boxsize.convert_to_units(au)
    load_region = [[15000*au, 45000*au], [15000*au, 45000*au], [15000*au, 45000*au]]

    # Constrain the mask
    mask.constrain_spatial(load_region)

    #Maks the data
    data = sw.load(filename, mask=mask)
    # data = sw.load(filename)

    #Compute projected density
    projected_density = project_gas(
        data,
        resolution=5000,
        project="masses",
        parallel=True,
        periodic=False,
    ).T

    #Convert to cgs
    projected_density.convert_to_cgs()

    #Units conversion
    data.gas.densities.convert_to_cgs()
    data.gas.coordinates.convert_to_units(unyt.au)
    # data.sinks.coordinates.convert_to_units(unyt.au)

    #Get simulation time
    time = data.metadata.t
    time.convert_to_units(unyt.kyr)

    #Get number of sinks
    N_sink = data.metadata.n_sinks

    projected_density_list.append(projected_density)
    time_list.append(time)
    N_sink_list.append(N_sink)

    x_sink_list.append(data.sinks.coordinates[:, 0])
    y_sink_list.append(data.sinks.coordinates[:, 1])


#%%

x_min = [12000, 16000, 18000, 19000]
x_max = [38000, 34000, 32000, 31000]
y_min = x_min
y_max = x_max #*unyt.au

figsize = (15,5)
# fig, axes = plt.subplots(ncols=4, nrows=1, figsize=figsize, layout="tight", num=1)
fig, axes = plt.subplots(ncols=4, nrows=1, figsize=figsize, num=1)

# Calculate the min and max of all projected_density values
v_min = -1 #min(density.min() for density in projected_density_list).value
v_max = None #max(density.max() for density in projected_density_list).value

for i, snap in enumerate(snapshot):
    ax = axes[i]
    projected_density = np.log10(projected_density_list[i].value)
    time = time_list[i]
    N_sink = N_sink_list[i]

    #Plot...
    im = ax.imshow(projected_density, origin='lower',  zorder = 1, #interpolation='None',
                        extent=[0, boxsize[0], 0, boxsize[1]], aspect='equal',
                        cmap = 'inferno', vmin=v_min, vmax=v_max)

    ax.scatter(x_sink_list[i].value, y_sink_list[i].value, c='limegreen', zorder=1, marker='.')

    #Add some limits for better visualisation
    ax.set_xlim([x_min[i], x_max[i]])
    ax.set_ylim([y_min[i], y_max[i]])

    #Add labels
    ax.set_xlabel("$x$ [au]")

    #Add y labels only for the left subplot
    if i == 0:
        ax.set_ylabel("$y$ [au]", labelpad=0.2)
        ax.yaxis.set_major_locator(MaxNLocator(nbins=3))
    else:
        ax.set_yticks([])

    #Put less ticks
    ax.xaxis.set_major_locator(MaxNLocator(nbins=3))


    #Add some text annotations

    ax.text(0.1, 0.95, "$t = {:.2f}".format(time.value/t_ff.value) +" t_{\mathrm{ff}}$", transform=ax.transAxes, fontsize=12, bbox=dict(facecolor='white', alpha=0.8))
    ax.text(0.65, 0.95, "$N_{\mathrm{sink}}" + " = {}$".format(N_sink), transform=ax.transAxes, fontsize=12, bbox=dict(facecolor='white', alpha=0.8))


plt.subplots_adjust(wspace=0.01, hspace=0.01)

# Add a colorbar for all subplots
cbar_ax = fig.add_axes([0.125, 0.77, 0.775, 0.05])  # Adjust these values to position the colorbar
cbar = fig.colorbar(im, cax=cbar_ax, orientation='horizontal')
cbar.set_label('$\log_{10}(\Sigma$ [g/cm$^2$]$)$', labelpad=1)
cbar.ax.xaxis.set_label_position('top')
cbar.ax.xaxis.set_ticks_position('top')

plt.subplots_adjust(top=0.85)  # Adjust the top to make space for the colorbar

plt.savefig("cc_projected_density_evolution.pdf", format='pdf', bbox_inches='tight', dpi=300)

