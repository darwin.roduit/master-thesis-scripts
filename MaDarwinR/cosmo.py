#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Oct 27 13:48:02 2024

@author: darwinr
"""

def get_redshift(scale_factor):
    return 1/scale_factor -1
