#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""Utility functions  """

import os
import numpy as np

import h5py

#%%
def generate_snapshots_filenames(n_snapshot, snap_basename, file_location=""):
    """Chooses the n_snapshot first snapshots in the directory given by file_location. 
    

    Parameters
    ----------
    n_snapshot : int
        Number of snapshot to use. It can be all snapshots in a directory or a 
        subset of them.
    snap_basename : str
        Basename of the snapshots.
    file_location : str, optional
        Path to the directory containing the snaphots. The default is "".

    Returns
    -------
    files : list of str
        List of the snapshots that we want to study.

    """
    #Adds the "/" at the end of the folder path if it is absent
    if (file_location != "") and (file_location[-1] != "/"):
        file_location=file_location+"/"
    
    str_hdf5 = ".hdf5"
    files = [""] * n_snapshot
    for i in range(n_snapshot):        
        #Trouver comment écrire cela de façon plus générique (si possibel sans les if else)
        if (i < 10):
            number = "000"+str(i)
        elif (i < 100):
            number = "00"+str(i)
        elif (i < 1000):
            number = "0"+str(i)
        else:
            number = str(i)
        files[i] = file_location+snap_basename+"_"+number+str_hdf5
    return files

def choose_n_filenames(n_snapshot, n_profile, snap_basename, file_location=""):
    """Chooses n_profile snapshots separated by n_snapshot/n_profile. It allows
    to choose a subset of snapshots to analyze them. 
    

    Parameters
    ----------
    n_snapshot : int
        Number of snapshot to use. It can be all snapshots in a directory or a 
        subset of them.
    n_profile : int
        Number of profiles that one wants to get informations about.
    snap_basename : str
        Basename of the snapshots.
        Examples: snap, snapshot
    file_location : str, optional
        ath to the directory containing the snaphots. The default is "".

    Returns
    -------
    files : list of str
        List of the snapshots that we want to study.

    """
    #Adds the "/" at the end of the folder path if it is absent
    if (file_location != "") and (file_location[-1] != "/"):
        file_location=file_location+"/"
    
    str_hdf5 = ".hdf5"
    files = [""] * (n_profile+1) #We add the snapshot 0
    n_start = n_snapshot/n_profile
    snap_number = np.zeros(n_profile+1) #add the snapshot 0
    snap_number[1:] = np.ceil(np.linspace(n_start, n_snapshot-1, n_profile))    
    for i, snap in enumerate(snap_number): #snap_number contains snap_0000 until snap_(snap_number-1)
        #Trouver comment écrire cela de façon plus générique (si possibel sans les if else)
        if (snap < 10):
            number = "000"+str(int(snap))
        elif (snap < 100):
            number = "00"+str(int(snap))
        elif (snap < 1000):
            number = "0"+str(int(snap))
        else:
            number = str(int(snap))
        files[i] = file_location+snap_basename+"_"+number+str_hdf5
    return files

def get_all_snapshot_in_directory(directory, snap_basename):
    """
    Finds all snapshot in the give directory. 

    Parameters
    ----------
    directory : str
        Path to the directory containing the snaphots.
    snap_basename : str
        Basename of the snapshots.
        Examples: snap, snapshot

    Returns
    -------
    files : list of str
        List of all snapshots in the directory.

    """
    if (directory[-1] != "/"):
        directory=directory+"/"
    all_dir_files = os.listdir(directory) #gets all files in the directory
    files = [file for file in all_dir_files if file.endswith(".hdf5")] #finds all .hdf5 files
    files = [directory+file for file in files if file.startswith(snap_basename)] #keeps only the files corresponding to snapshots
    files.sort()
    return files

def get_number_snapshot_directory(directory, snap_basename):
    return len(get_all_snapshot_in_directory(directory, snap_basename))

def get_snapshot_time(filename):
    """
    Gets the time from the given snapshot at filename. 

    Parameters
    ----------
    filename : str, optional
        Path to the the snaphot. 

    Returns
    -------
    float
        The time in the simulation of the given snapshot.

    """
    data = h5py.File(filename, "r")
    return data["Header"].attrs["Time"][0]