#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Aug  9 10:08:16 2024
Concatenate data from two different outputs in one file for convenient data analysis.

The tidal stripping data analysis files may be separated into two folders: data_analysis and
data_analysis_stars. The reason is that for som simulations, the stellar part is not centered with
the DM anymore. Hence, stellar data from data_analysis folder suffer from this lack of centering.

This script aims at concatenating both files for easier data analysis and archiving.

@author: darwinr
"""
import numpy as np
import os

#%%
# Add the "/" at the end of the directory names
main_directory = "/home/darwinr/mnt/lesta/projects/tidal_stripping/NFW_0025_0_8/Tucana_3"

directory_stars = "data_analysis_stars/"
directory_dm = "data_analysis_DM/"

# Must be different than folder_dm and folder_stars to avoid overwritting data
output_directory = "data_analysis/"


#%%
if (output_directory == directory_dm) or (output_directory == directory_stars):
    raise ValueError(
        "The output directory must be different from the two input directories to avoid overwritting data.")

# Create the full path
folder_stars = os.path.join(main_directory, directory_stars)
folder_dm = os.path.join(main_directory, directory_dm)
output_directory = os.path.join(main_directory, output_directory)

# Creates the output directory
if not (os.path.exists(output_directory)):
    print("Output directory does not exist. It will be created.")
    os.makedirs(output_directory)

# %%
# Density dm
data_stars = np.load(
    folder_stars+"density_profile_data.npz", allow_pickle=True)
data_dm = np.load(folder_dm+"density_profile_data.npz", allow_pickle=True)
np.savez_compressed(output_directory+"density_profile_data", r_stars=data_stars["r_stars"], r_DM=data_dm["r_DM"],
                    density_stars=data_stars['density_stars'], density_DM=data_dm['density_DM'], t=data_stars["t"])


# %%
# Half light radiuss
data_stars = np.load(folder_stars+"radius_of_half_mass.npz", allow_pickle=True)
data_dm = np.load(folder_dm+"radius_of_half_mass.npz", allow_pickle=True)
np.savez_compressed(output_directory+"radius_of_half_mass", mass_half_stars=data_stars["mass_half_stars"],
                    mass_half_DM=data_dm['mass_half_DM'], r_half_stars=data_stars[
                        "r_half_stars"], D_r_half_stars=data_stars["D_r_half_stars"],
                    r_half_DM=data_dm["r_half_DM"], D_r_half_DM=data_dm["D_r_half_DM"], t=data_stars["t"])

# %%
# Half light radiuss
data_stars = np.load(folder_stars+"velocity_dispersion.npz", allow_pickle=True)
data_dm = np.load(folder_dm+"velocity_dispersion.npz", allow_pickle=True)
np.savez_compressed(output_directory+"velocity_dispersion", r_stars=data_stars["r_stars"],
                    r_DM=data_dm["r_DM"], sigma_stars=data_stars["sigma_stars"], sigma_DM=data_dm["sigma_DM"],
                    D_sigma_stars=data_stars["D_sigma_stars"], D_sigma_DM=data_dm["D_sigma_DM"],
                    t=data_stars["t"])
