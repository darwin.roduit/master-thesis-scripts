#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Aug  6 15:48:10 2024

@author: darwinr
"""

import os
import numpy as np
import matplotlib.pyplot as plt
import logging, sys
import astropy.units as u
from astropy.constants import m_p
from pNbody import *

plt.style.use("/home/darwinr/Desktop/Scripts/MaDarwinR/"  + 'paper_style_figure.mplstyle')


#%%
def radial_density_profile(m, r, r_min, r_max, n_bins=30):
    if r_min is None:
        r_min = r.min()

    if r_max is None:
        r_max = r.max()

    # Bin the radii using np.digitize to get the bin indices
    r_bins = np.logspace(np.log10(r_min), np.log10(r_max), n_bins + 1)
    bin_indices = np.digitize(r, bins=r_bins) - 1

    # Calculate the centers of the bins for plotting
    r_centers = 0.5 * (r_bins[:-1] + r_bins[1:])

    # Loop through each bin index and accumulate masses and volumes
    masses = np.zeros(n_bins)
    volumes = np.zeros(n_bins)
    for i in range(n_bins):
        in_bin = bin_indices == i
        masses[i] = np.sum(m[in_bin])  # Total mass in bin
        volumes[i] = (4/3) * np.pi * (r_bins[i+1]**3 - r_bins[i]**3)  # Volume of shell

    rho = masses/volumes

    return r_centers, rho


#%%
# Configure the debugging level. Useful to print debugging info

do_debug = False

if do_debug:
    #Debug information
    logging.basicConfig(stream=sys.stderr, level=logging.DEBUG)
else:
    # Normal information
    logging.basicConfig(stream=sys.stderr, level=logging.INFO)


#%%
#Get the paths to the files

main_location = "/home/darwinr/mnt/lesta/projects/tidal_stripping/isolated_cases"
folders = ["IC=nfw_0025_0_8+plummer", "IC=2s0_3_012_05+plummer", "IC=2s0_3_012_1_5+plummer"]
files = ["IC=nfw_0025_0_8+plummer_launched.hdf5", "IC=2s0_3_012_05+plummer_launched.hdf5",  "IC=2s0_3_012_1_5+plummer_launched.hdf5"]
sim_name = ['Fiducial NFW', 'Small cored halo', 'Large cored halo']
colors_base = ['tomato', 'sandybrown', 'gold', 'limegreen', 'seagreen', 'lightskyblue', 'royalblue', 'blueviolet', 'hotpink']
colors = ['tomato', 'royalblue', 'blueviolet']


# Parameters
smoothing_length =0.0125
n_bins = 30

# Convenient variable for unit conversions
unit_atom_per_cm_3 = m_p.to(u.g)/u.cm**3

#%%

figsize = (6.4, 4.8)
fig, ax = plt.subplots(num=1, nrows=1, ncols=1, figsize=figsize, layout="tight")
ax.clear()
# x = np.linspace(-10, 10)
# ax.plot(x, x)
# ax.plot(x, x**2)
# ax.plot(x, x**3)
# ax.plot(x, x**4)
# ax.plot(x, x**5)
# ax.plot(x, x**6)
# ax.plot(x, x**7)
# ax.plot(x, x**8)
# ax.set_yscale('symlog')

#%%

for i in range(len(folders)):
# i = 0

    # Create the snapshot path
    snap = os.path.join(main_location, folders[i], files[i])
    logging.debug('The snapshot file is {}'.format(snap))

    # Open the dark matter data with pNbody
    logging.info("Opening the data. This can take time...")
    nb_dm = Nbody(snap, ptypes=[1])

#%%
    # Units of the data

    #Get the units from the data
    unit_length_in_cm = nb_dm.UnitLength_in_cm[0]*u.cm
    unit_mass_in_g = nb_dm.UnitMass_in_g[0]*u.g
    unit_velocity_in_cm_per_s = nb_dm.UnitVelocity_in_cm_per_s*u.cm/u.s
    unit_time_in_cgs = nb_dm.Unit_time_in_cgs[0]*u.s


    mass = nb_dm.mass
    r = nb_dm.rxyz()
    r_min = r.min()*2
    r_max = r.max()

    # Do the density profile
    r_centers, rho = radial_density_profile(mass, r, r_min, r_max, n_bins)

    rho =  rho * unit_mass_in_g / unit_length_in_cm**3
    rho = rho.to(unit_atom_per_cm_3).value

    #Plot
    ax.plot(r_centers, rho, color=colors[i], label=sim_name[i])


#%%
# Plot the region of the smoothing lenght for visual clarity
logging.info("Finalizing plot...")

ax.legend()
ax.set_xscale('log')
ax.set_yscale('log')
ax.set_xlabel(r'$r$ [kpc]')
ax.set_ylabel(r'$\rho_{\mathrm{DM}}$ [atom/cm$^3$]')
ax.axvspan(ax.get_xlim()[0], smoothing_length, color='grey', zorder=10, alpha=0.3)

plt.savefig('density_profile_dm_isolated_cases.pdf', format='pdf', bbox_inches='tight', dpi=300)

