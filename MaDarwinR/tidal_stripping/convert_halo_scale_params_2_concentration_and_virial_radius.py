#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Aug  7 15:19:44 2024

@author: darwinr
"""

import numpy as np
import astropy.units as u
from astropy.constants import G as G_a
from scipy.optimize import bisect


def getr200(r, nb1, G, Hubble, Omega_0):
    """This function is used by a root finding algorithm to find r_200"""
    rho_crit =  3 * Hubble * Hubble / (8 * np.pi * G);
    nbs = nb1.selectc(nb1.rxyz()<r)
    r_2 = nbs.rxyz()
    r_2 = np.max(r_2)
    M = np.sum(nbs.mass)
    V = 4/3.*np.pi*r_2**3
    return (M/V - 200*rho_crit*Omega_0)

def get_cored_concentration(c, rho_0, rho_c):
    """This function is used by a root finding algorithm to find the concentration"""

    denom = (4*c + 3) / (2 * (c+1)**2) + np.log(1 + c) - 3/2
    RHS = 200/3 * rho_c * c**3 / denom
    LHS = rho_0
    return LHS - RHS

def get_NFW_concentration(c, rho_0, rho_c):
    """This function is used by a root finding algorithm to find the concentration"""

    denom = np.log(1 + c) - c / (1 + c)
    # print("denom = ", denom)
    RHS = 200/3 * rho_c * c**3 / denom
    # print("RHS = ", RHS)
    LHS = rho_0
    return LHS - RHS

def get_r_200(nb_dm, G, Hubble):
    """Determine r_200 for a halo. The nb_dm object must be centered on the halo of interest."""
    r = nb_dm.rxyz()
    r_max = np.max(r)
    r_min =1.01 * np.min(r)
    Omega_0 = nb_dm.omega0
    r_200 = bisect(getr200, r_min, r_max, args = (nb_dm, G, Hubble, Omega_0), xtol = 1e-4, maxiter = 400)
    return r_200


#%%
H = 67.3 * u.km/u.s/u.Mpc ; H = H.to(1/ u.s)
G = G_a
critical_density = 3*H**2 / (8*np.pi*G)

print("Critical density = {}".format(critical_density))



#%%

unit_mass =  1e10*u.Msun
unit_length = u.kpc

# Halo scale parameters
r_s = 0.8*unit_length ; r_s = r_s.value
rho_0 = 0.012 * unit_mass / unit_length**3 ; rho_0= rho_0.to(unit_mass / unit_length**3).value
#%%
# NFW halo

c_min = 0.1 ; c_max = 200

#Convert rho_c to internal units
rho_c = critical_density.to(1e10*u.Msun / u.kpc**3).value

# f_min =  get_NFW_concentration(c_min, rho_0, rho_c)
# f_max =  get_NFW_concentration(c_max, rho_0, rho_c)

# print(f_min, f_max)

c_NFW = bisect(get_NFW_concentration, c_min, c_max, args = (rho_0, rho_c), xtol = 1e-3, maxiter = 400)
c_cored = bisect(get_cored_concentration, c_min, c_max, args = (rho_0, rho_c), xtol = 1e-3, maxiter = 400)


print("c_NFW = {:.2f}".format(c_NFW))
print("c_cored = {:.2f}".format(c_cored))

r_200_NFW = c_NFW * r_s
r_200_cored = c_cored * r_s

print("r_200_NFW = {:.2f}".format(r_200_NFW))
print("r_200_cored = {:.2f}".format(r_200_cored))
