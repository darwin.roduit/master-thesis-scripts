#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Aug  8 10:18:42 2024

@author: darwinr
"""

import os
import numpy as np
import matplotlib.pyplot as plt
import logging, sys
import astropy.units as u
from astropy.constants import m_p
from pNbody import *
from matplotlib.colors import LinearSegmentedColormap
import matplotlib.cm as cm

plt.style.use("/home/darwinr/Desktop/Scripts/MaDarwinR/"  + 'paper_style_figure.mplstyle')


#%%


#%%

# Configure the debugging level. Useful to print debugging info
do_debug = False

if do_debug:
    #Debug information
    logging.basicConfig(stream=sys.stderr, level=logging.DEBUG)
else:
    # Normal information
    logging.basicConfig(stream=sys.stderr, level=logging.INFO)

#%%
#Get the paths to the files
colors_base = ['tomato', 'sandybrown', 'gold', 'limegreen', 'seagreen', 'lightskyblue', 'royalblue', 'blueviolet', 'hotpink']


#Isolated cases plot
# main_location = "/home/darwinr/mnt/lesta/projects/tidal_stripping/isolated_cases"
# folders = ["IC=nfw_0025_0_8+plummer", "IC=2s0_3_012_05+plummer", "IC=2s0_3_012_1_5+plummer"]
# data_analysis_folder = "data_analysis"
# output_png_name = 'isolated_cases'

# sim_name = ['Fiducial NFW', 'Small core', 'Large core']
# # colors = ['red', 'royalblue', 'blueviolet']
# colors = ['red', 'darkorange', 'blue', '#03a303', '#b200e3']
# linestyle = ["-", "-", "-"]
# density_plot_maximal_radius_DM = 30 #kpc
# density_plot_maximal_radius_stars = 0.5 #kpc
# velocity_dispersion_maximal_radius = 0.15#kpc
# half_light_maximal_radius = 0.3 #kpc
# velocity_dispersion_max = 7 #km/s


# Fiducial NFW -----------------------------
# main_location = "/home/darwinr/mnt/lesta/projects/tidal_stripping/NFW_0025_0_8"
# folders = ["Bootes_1", "Tucana_2", "Hercules", "Bootes_3", "Tucana_3"]
# data_analysis_folder = "data_analysis"
# output_png_name = "fiducial_nfw"

# sim_name = ['Bootes 1', 'Tucana 2', 'Hercules', 'Bootes 3', "Tucana 3"]
# colors = ['red', 'darkorange', 'blue', '#03a303', '#b200e3']
# linestyle = ["-", "-", "-", "-", "-"]
# density_plot_maximal_radius_DM = 30 #kpc
# density_plot_maximal_radius_stars = 0.5 #kpc
# velocity_dispersion_maximal_radius = 0.15#kpc
# half_light_maximal_radius = 0.3 #kpc
# velocity_dispersion_max = 7 #km/s

# # Small cored halo ---------------------------
# main_location = "/home/darwinr/mnt/lesta/projects/tidal_stripping/2s0_3_012_05"
# output_png_name = "small_core"


# # Large cored halo ------------------------
main_location = "/home/darwinr/mnt/lesta/projects/tidal_stripping/2s0_3_012_1_5"
output_png_name = "large_core"

# Parameters common to small and large cored halo
folders = ["Bootes_1", "Tucana_2", "Hercules"]
data_analysis_folder = "data_analysis"
sim_name = ['Bootes 1', 'Tucana 2', 'Hercules']
colors = ['red', 'darkorange', 'blue', '#03a303', '#b200e3']
linestyle = ["-", "-", "-", "-", "-"]
density_plot_maximal_radius_DM = 30 #kpc
density_plot_maximal_radius_stars = 0.5 #kpc
velocity_dispersion_maximal_radius = 0.15#kpc
half_light_maximal_radius = 0.3 #kpc
velocity_dispersion_max = 7 #km/s



# Parameters
N_curves = 3
smoothing_length =0.0125

#%%

# plt.clf()

# figsize = (16, 4)
figsize = (8, 8)
fig, axes = plt.subplots(num=1, nrows=2, ncols=2, figsize=figsize, layout="tight")
ax_1 = axes[0, 0] ; ax_2 = axes[0, 1] ; ax_3 = axes[1, 0] ; ax_4 = axes[1, 1]
ax_1.clear() ; ax_2.clear() ; ax_3.clear() ; ax_4.clear()

#%%
# DM density plot
data_file = "density_profile_data.npz"
for i in range(len(folders)):
    logging.info('Processing folder {}'.format(folders[i]))

    # Retrieve the data
    file = os.path.join(main_location, folders[i], data_analysis_folder, data_file)
    logging.info('The data file is {}'.format(file))
    data = np.load(file, allow_pickle=True)

    r_DM = data["r_DM"]
    density_DM = data["density_DM"]
    time = data["t"]

    # Generate four equally spaced indices
    indices = np.linspace(0, len(r_DM) - 1, N_curves, dtype=int)

    # Select the elements at those indices
    r_DM = r_DM[indices]
    density_DM = density_DM[indices]
    time = time[indices]



    # Create a colormap that transitions from white to the base color
    cmap = LinearSegmentedColormap.from_list("custom_cmap", ["white", colors[i]])
    colors_plot = [cmap(i / (N_curves)) for i in range(N_curves+1)]
    colors_plot = colors_plot[1:]
    colors_plot.reverse()


    for j in range(len(r_DM)):
        index = np.argwhere(r_DM[j] < density_plot_maximal_radius_DM)
        r_DM[j] = r_DM[j][index]
        density_DM[j] = density_DM[j][index]

        ax_1.plot(r_DM[j], density_DM[j], color=colors_plot[j], linestyle=linestyle[i], zorder=10-i)


logging.info("Finalizing plot 1...")

# ax.legend()
# ax_1.set_xlim(1e-2, 30)
ax_1.set_xscale('log')
ax_1.set_yscale('log')
ax_1.set_xlabel(r'$r$ [kpc]')
ax_1.set_ylabel(r'$\rho_{\mathrm{DM}}$ [atom/cm$^3$]')
# ax_1.axvspan(ax_1.get_xlim()[0], smoothing_length, color='grey', zorder=10, alpha=0.3)

#%%
# Stars density plot
data_file = "density_profile_data.npz"
for i in range(len(folders)):
    logging.info('Processing folder {}'.format(folders[i]))

    # Retrieve the data
    file = os.path.join(main_location, folders[i], data_analysis_folder, data_file)
    logging.info('The data file is {}'.format(file))
    data = np.load(file, allow_pickle=True)

    r_stars = data["r_stars"]
    density_stars = data["density_stars"]
    time = data["t"]

    # Generate four equally spaced indices
    indices = np.linspace(0, len(r_stars) - 1, N_curves, dtype=int)

    # Select the elements at those indices
    r_stars = r_stars[indices]
    density_stars = density_stars[indices]
    time = time[indices]

    # Create a colormap that transitions from white to the base color
    cmap = LinearSegmentedColormap.from_list("custom_cmap", ["white", colors[i]])
    colors_plot = [cmap(i / (N_curves)) for i in range(N_curves+1)]
    colors_plot = colors_plot[1:]
    colors_plot.reverse()

    for j in range(len(r_stars)):
        index = np.argwhere(r_stars[j] < density_plot_maximal_radius_stars)
        r_stars[j] = r_stars[j][index]
        density_stars[j] = density_stars[j][index]
        ax_2.plot(r_stars[j], density_stars[j], color=colors_plot[j], linestyle=linestyle[i], zorder=10-i)


logging.info("Finalizing plot 2...")

# ax.legend()
# ax_2.set_xscale('log')
ax_2.set_yscale('log')
ax_2.set_xlabel(r'$r$ [kpc]')
ax_2.set_ylabel(r'$\rho_{\star}$ [atom/cm$^3$]')
# ax_2.axvspan(ax_2.get_xlim()[0], smoothing_length, color='grey', zorder=10, alpha=0.3)

#%%
# Stars R_12 plot
data_file = "radius_of_half_mass.npz"

for i in range(len(folders)):
    logging.info('Processing folder {}'.format(folders[i]))

    # Retrieve the data
    file = os.path.join(main_location, folders[i], data_analysis_folder, data_file)
    logging.info('The data file is {}'.format(file))
    data = np.load(file, allow_pickle=True)

    r_half_stars = data["r_half_stars"]
    time = data["t"]

    # For the simulations with 500 snapshots, only plot a 1/10 to match the simulations with 50 snapshots
    if r_half_stars.size > 55:
        r_half_stars = r_half_stars[::10]
        time = time[::10]

    ax_3.plot(time, r_half_stars, color=colors[i], linestyle=linestyle[i], zorder=10-i)

logging.info("Finalizing plot 3...")

ymin, ymax = ax_3.get_ylim()
ax_3.set_ylim(1e-2, 0.3)
ax_3.set_xlabel(r'Time [Gyr]')
ax_3.set_ylabel(r'$R_{1/2}$ [kpc]')

#%%
# Stars velocity dispersion plot
data_file = "velocity_dispersion.npz"
for i in range(len(folders)):
    logging.info('Processing folder {}'.format(folders[i]))

    # Retrieve the data
    file = os.path.join(main_location, folders[i], data_analysis_folder, data_file)
    logging.info('The data file is {}'.format(file))
    data = np.load(file, allow_pickle=True)

    r_stars = data["r_stars"]
    sigma_stars = data["sigma_stars"]
    time = data["t"]

    # Generate four equally spaced indices
    indices = np.linspace(0, len(r_stars) - 1, N_curves, dtype=int)

    # Select the elements at those indices
    r_stars = r_stars[indices]
    sigma_stars = sigma_stars[indices]
    time = time[indices]

    # Create a colormap that transitions from white to the base color
    cmap = LinearSegmentedColormap.from_list("custom_cmap", ["white", colors[i]])
    colors_plot = [cmap(i / (N_curves)) for i in range(N_curves+1)]
    colors_plot = colors_plot[1:]
    colors_plot.reverse()

    for j in range(len(r_stars)):
        index = np.argwhere(r_stars[j] < velocity_dispersion_maximal_radius)
        r_stars[j] = r_stars[j][index]
        sigma_stars[j] = sigma_stars[j][index]

        ax_4.plot(r_stars[j], sigma_stars[j], color=colors_plot[j], linestyle=linestyle[i], zorder=10-i)


logging.info("Finalizing plot 4...")

ymin, ymax = ax_4.get_ylim()
xmin, xmax = ax_4.get_xlim()

ax_4.set_ylim(0, velocity_dispersion_max)
# ax_4.set_xlim(0, 0.75)
ax_4.set_xlabel(r'$r$ [kpc]')
ax_4.set_ylabel(r'$\sigma_{\star}$ [km/s]')
# ax_4.axvspan(ax_2.get_xlim()[0], smoothing_length, color='grey', zorder=10, alpha=0.3)

time_legend = time

#%%
from matplotlib.patches import Patch

# Create legend for simulations
legend_elements_sim = [Patch(facecolor=base_color, edgecolor='k', label=sim) for sim, base_color in zip(sim_name, colors)]

# Create legend for the colormap
cmap = LinearSegmentedColormap.from_list("custom_cmap", ["white", "black"])
colors_plot = [cmap(i / (N_curves)) for i in range(N_curves+1)]
colors_plot = colors_plot[1:]
colors_plot.reverse()
cmap_legend = [Patch(facecolor=color, edgecolor='k', label=f'{sim_time:.1f} Gyr') for color, sim_time in zip(colors_plot, time_legend)]

# Add the legends to the first subplot
ax_1.legend(handles=legend_elements_sim, title="Simulations", loc='lower left')
ax_2.legend(handles=cmap_legend, title="Time", loc='lower left')

#%%
# Save the figure
plt.savefig(output_png_name + '.pdf', format='pdf', bbox_inches='tight', dpi=300)
