#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Nov 13 09:45:04 2024

@author: darwinr
"""

import numpy as np
from MaDarwinR.stars_utils import mdf
from scipy.optimize import curve_fit


def mdf_function(Fe_H, A, p):
    return A * 10**Fe_H * np.exp(-10**Fe_H / p)


def get_galaxy_FeH(nb, n_bins=100):
    # Compute the MDF
    Fe_H = nb.AbRatio("Fe", "H")
    n_star = len(Fe_H)
    Fe_H_counts, Fe_H_stellar_fraction, Fe_H_edges = mdf(Fe_H, n_star, n_bins)
    x_data = (Fe_H_edges[:-1] + Fe_H_edges[1:]) / 2  # Midpoints of bins

    # Fit the MDF to the function with parameters A and p
    try:
        # Initial guess for A and p
        # Use max value as guess for A
        A_initial = np.max(Fe_H_stellar_fraction)

        # Guess for p
        peak_idx = np.argmax(Fe_H_stellar_fraction)
        FeH_peak = x_data[peak_idx]
        p_initial = 10**FeH_peak  # Since MDF peaks at log10(p)

        # Fit
        popt, _ = curve_fit(mdf_function, x_data, Fe_H_stellar_fraction, p0=[
                            A_initial, p_initial])
        A_fit, p_fit = popt

        # Calculate the mode of the MDF from p
       # Not the formula in Sanati et al. 2023 (which is incorrect). This is found by maximising the function
        FeH_gal = np.log10(p_fit)

    except RuntimeError:
        print("Error: Fitting did not converge.")
        FeH_gal = np.nan  # Return NaN if fitting fails

    return FeH_gal


def get_galaxy_L_V(nb):
    # Get the luminosity of the stars
    L_v = nb.Luminosity()  # in M_sun

    # Sum the luminosities
    L_v_gal = np.sum(L_v)
    return L_v_gal
