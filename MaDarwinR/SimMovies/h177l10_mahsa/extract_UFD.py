#!/usr/bin/env python3
import Ptools as pt

from optparse import OptionParser

from pNbody import *
import sys

from scipy import optimize
from numpy import *

import numpy as np


def parse_options():

  usage = "usage: %prog [options] file"
  parser = OptionParser(usage=usage)

  parser = pt.add_ftype_options(parser)

  parser.add_option('-o', "--outputdirectory",
                    action="store",
                    dest="outputdirectory",
                    type="string",
                    default=None,
                    help="outputdirectory name",
                    metavar=" STRING")

  parser.add_option("--ref",
                    action="store",
                    type="string",
                    dest="ref",
                    default=None,
                    help="ref")

  parser.add_option("--Rmax",
           action="store",
           dest="Rmax",
           type="float",
           default = 50,
           help="extraction radius",
           metavar=" FLOAT")

  (options, args) = parser.parse_args()

  files = args

  return files, options


###################################################
# main
###################################################


files, opt = parse_options()

# check if ref file exists
if not os.path.isfile(opt.ref):
  print("%s does not exists !!!" % (opt.ref))
  sys.exit()

if len(files) > 1:
  # check if output directory  exists
  if not os.path.isdir(opt.outputdirectory):
    print("%s does not exists !!!" % (opt.outputdirectory))
    print("creating it.")
    os.makedirs(opt.outputdirectory)


# Get the particles ids
nb = Nbody(opt.ref, ftype=opt.ftype)
num = nb.num


for file in files:
  print(32*"#")
  print(file)
  print(32*"#")

  # open the model and add units
  nb = Nbody(file, ftype=opt.ftype)

  # extract particles
  nb_selected = nb.selectp(num)

  # Get their CM
  center = nb_selected.cm()

  # Get the particles around the selected cm
  nb = nb.selectc(nb.rxyz(center=center) < opt.Rmax )

  nb.potcenter()

  ######################
  # save the model

  if len(files) > 1:
    bname = os.path.basename(file)
    outputname = os.path.join(opt.outputdirectory, bname)
  else:
    outputname = opt.outputdirectory

  nb.rename(outputname)
  # nb.nzero=None
  # nb.massarr=None
  # nb.MassTable=None

  nb.write()
