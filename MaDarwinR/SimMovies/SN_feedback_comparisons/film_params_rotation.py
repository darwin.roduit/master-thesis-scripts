import numpy as np

# global film
width = 10
height = 10
component_number = 1
component_type = ['gas', 'stars']
palette = ['cyan', 'yellow', 'light']
frame_size = 1000
ouptut_format = 'png'  # or png
view_point = ['xz']
smoothing = [20, 5, 7.5]
xp = [-2.30347361, -4.01523326,  2.31890152]  #Observing position, i.e where we are looking at

r = 10
theta = np.pi/2
phi_array = np.linspace(0, 2*np.pi, 1000)
# theta_array = np.linspace(0, 2*np.pi, 10)

film = {}
film['ftype'] = 'swift'
film['time'] = 'nb.atime'
film['exec'] = ''

film['format'] = ouptut_format  # fits
film['frames'] = []


for k, phi in enumerate(phi_array):
    frame = {}
    frame['width'] = frame_size
    frame['height'] = frame_size
    frame['size'] = (width, height)
    frame['tdir'] = None
    frame['pfile'] = None
    frame['exec'] = None
    frame['macro'] = None
    frame['components'] = []
    frame['xp'] = xp
    frame['alpha'] = 0 #Remove 'view'

    x_0 = r*np.array([np.cos(phi)*np.sin(theta), np.sin(phi)*np.sin(theta), np.cos(theta)])
    frame['x0'] = x_0 #Position of the observer
    print(x_0)
    for i in range(component_number):
        # Component of the nb object (gas, stars, sink, halo)
        component = {}
        component['id'] = component_type[i]

        # For DM, compute a SPH based smoothing length to smooth the image
        if component_type[i] == 'halo':
            component['exec'] = 'nbfc.rsp = nbfc.get_rsp_approximation() ; nbfc.Hsml = nbfc.rsp ; nbfc.ComputeSph(DesNumNgb = 10, MaxNumNgbDeviation=10) ; nbfc.rsp = nbfc.Hsml'

        component['frsp'] = smoothing[i]
        frame['components'].append(component)

    # append frame to film
    film['frames'].append(frame)

