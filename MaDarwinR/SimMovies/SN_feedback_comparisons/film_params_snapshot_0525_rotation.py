import numpy as np

# global film
width = 0.1
height = 0.1
component_number = 1
# component_type = ['gas', 'stars']
component_type = ['stars', 'gas']
palette = ['cyan', 'yellow', 'light']
frame_size = 1000
ouptut_format = 'png'  # or png
view_point = ['xz']
smoothing = [10, 5, 7.5]
smoothing = [1, 10, 7.5]
xp = [2427.54775157, 2410.63951125, 2602.63899174] #Observing position

r = 8000
theta = np.pi/4
phi_array = np.linspace(0, 2*np.pi, 1)
# theta_array = np.linspace(0, 2*np.pi, 10)

film = {}
film['ftype'] = 'swift'
film['time'] = 'nb.atime'
film['exec'] = ''

film['format'] = ouptut_format  # fits
film['frames'] = []


for k, phi in enumerate(phi_array):
    frame = {}
    frame['width'] = frame_size
    frame['height'] = frame_size
    frame['size'] = (width, height)
    frame['tdir'] = None
    frame['pfile'] = None
    frame['exec'] = None
    frame['macro'] = None
    frame['components'] = []
    frame['xp'] = xp
    frame['alpha'] = 0 #Remove 'view'
    frame['palette'] = 'yellow'
    print(frame)

    x_0 = r*np.array([np.cos(phi)*np.sin(theta), np.sin(phi)*np.sin(theta), np.cos(theta)])
    frame['x0'] = x_0
    print(x_0)
    for i in range(component_number):
        # Component of the nb object (gas, stars, sink, halo)
        component = {}
        component['id'] = component_type[i]

        # For DM, compute a SPH based smoothing length to smooth the image
        if component_type[i] == 'halo':
            component['exec'] = 'nbfc.rsp = nbfc.get_rsp_approximation() ; nbfc.Hsml = nbfc.rsp ; nbfc.ComputeSph(DesNumNgb = 10, MaxNumNgbDeviation=10) ; nbfc.rsp = nbfc.Hsml'

        component['frsp'] = smoothing[i]
        frame['components'].append(component)

    # append frame to film
    film['frames'].append(frame)

