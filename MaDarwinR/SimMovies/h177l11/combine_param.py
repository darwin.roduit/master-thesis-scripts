# path_to_cmap = "/home/astro/roduit/Documents/cmap/" #lesta
path_to_cmap = "/home/roduit/cmap/" #jed

##########
# dm
##########
args[0]['scale'] = 'log'
args[0]['mn'] = 0
args[0]['mx'] = 6.03391e-07
args[0]['cd'] = 8.8688e-09

args[0]['palette'] = path_to_cmap + 'light'
# args[0]['palette'] = './dm'
args[0]['fc'] = 1.0


##########
# gas
##########
args[1]['scale'] = 'log'
args[1]['mn'] = 0
args[1]['mx'] = 1.54261e-07
args[1]['cd'] = 5.60066e-11

args[1]['palette'] = path_to_cmap + 'sunburst'
# args[1]['palette'] = './gs'
args[1]['fc'] = 1
# args[1]['smode'] = "add"
# args[1]['qs2'] = 10
args[1]['smode'] = "alpha_gamma"
# Lower values makes the gas more transparent. A value of 7.5 to 10 is good.
args[1]['qs2'] = 7.5
args[1]['gamma'] = 1.0

##########
# stars
##########
args[2]['scale'] = 'log'
args[2]['mn'] = 0
args[2]['mx'] = 5.42772e-06
args[2]['cd'] = 8.28046e-12

args[2]['palette'] = path_to_cmap + 'jungle'
# args[2]['palette'] = './st'
args[2]['fc'] = 1.0
args[2]['smode'] = "alpha_gamma"
args[2]['qs2'] = -0.5
args[2]['gamma'] = 1.0
