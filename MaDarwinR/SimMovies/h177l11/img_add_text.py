#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
02.09.2024

Add some texts to simulation movies:
    - time
    - EPFL logo
    - Scale-length
    - Paper name

@author: Darwin Roduit
"""

import matplotlib.pyplot as plt
import matplotlib.image as mpimg
import matplotlib.patches as patches
import argparse
import numpy as np
import os
import glob
from pNbody import *
from tqdm import tqdm

# %% Functions

def parse_option():
    description = """"
Add some text to images
    """
    epilog = """
Examples:
--------
python3 img_add_text.py --logo_file="EPFL_Logo_Digital_RGB_PROD.png" --time_file="times.npz" --stylesheet="movie_stylesheet.mplstyle" --scale=20 --scale_text="20 ckpc" -o test png_combined_frame_0/*.png
python3 img_add_text.py --logo_file="EPFL_Logo_Digital_RGB_PROD.png" --time_file="times.npz" --stylesheet="movie_stylesheet.mplstyle" --scale=20 --scale_text="20 ckpc" -o test png_combined_frame_0/*.png --output_img_dpi=310

Then, to combine the images, use ffmpeg: 
    ffmpeg -framerate 30 -i test/%08d.png -c:v libx264  output.mp4
"""
    parser = argparse.ArgumentParser(description=description, epilog=epilog)

    parser.add_argument("files",
                        nargs="+",
                        type=str,
                        help="File name(s).")

    parser.add_argument("-o",
                        action="store",
                        type=str,
                        dest="output_folder",
                        default="png_images_processed",
                        help="Name of the output folder.")

    parser.add_argument("--logo_file",
                        action="store",
                        type=str,
                        dest="filename_logo",
                        default=None,
                        help="Image containing the logo to display, e.g. EPFL logo")

    parser.add_argument("--time_file",
                        action="store",
                        type=str,
                        dest="filename_time",
                        help="File containing the  simulation times.")

    parser.add_argument("--stylesheet",
                        action="store",
                        type=str,
                        dest="stylesheet",
                        default=None,
                        help="Matplotlib stylesheet.")

    parser.add_argument("--output_img_dpi",
                        action="store",
                        type=int,
                        dest="output_img_dpi",
                        default=None,
                        help="DPI of the output images. This should be close to the original one.")

    parser.add_argument("--scale",
                        action="store",
                        type=float,
                        dest="scale_length",
                        help="Scale length to display")

    parser.add_argument("--scale_text",
                        action="store",
                        type=str,
                        dest="scale_text",
                        help="Scale text to display")

    parser.parse_args()
    args = parser.parse_args()
    files = args.files

    for f in files:
        if not os.path.exists(f):
            raise FileNotFoundError("You need to provide one file")

    return args, files


# %% Main script

args, files = parse_option()


###########################
# Files and units
###########################

filename_time = args.filename_time
list_image = files
n_image = len(list_image)
output_folder = args.output_folder # "../snap/outputs/png_movie_processed"
filename_logo = args.filename_logo
stylesheet = args.stylesheet

dpi = args.output_img_dpi

# Get the scale data and create the text
scale_length = args.scale_length
scale_text = args.scale_text

###########################
# Images
###########################
# Set the ratio of the image and the figsize
ratio = 16/9
frame_size_y = 50  # kpc
frame_size_x = frame_size_y*ratio  # kpc

img_width = 1920  # px
img_height = int(img_width/ratio)  # px

figsize = (8, 4.5)

# Load matplotlib stylesheet
if stylesheet is not None:
    plt.style.use('movie_stylesheet.mplstyle')

# Create the output directory
os.makedirs(output_folder, exist_ok=True)


# Load the time data
data_time = np.load(filename_time)
time = data_time["Time"]
redshift = data_time["ScaleFactor"]
n_time= len(time)

if n_image != n_time:
    raise RuntimeError(
        f"The number of images is not the same as the number of timestamps. It must be the same to properly get meaningful physical information, e.g. simulation time. n_image = {n_image}, n_time = {n_time}")


# Get the size of the first image. All images must have the same size
if dpi is None:
    img = mpimg.imread(list_image[0])
    dpi = img.shape[0] // figsize[0]
    print(dpi)

###########################
# Logo
###########################
# Open the logo file

if filename_logo is not None:
    logo = mpimg.imread(filename_logo)
    logo_height, logo_width, _ = logo.shape

    # Resize the logo
    logo_height, logo_width = logo_height/70, logo_width/70

    # Logo position in physical units (e.g., bottom right corner)
    logo_position_x = frame_size_x - logo_width
    logo_position_y = - 0.99*frame_size_y

###########################
# Now compose your image
###########################
for i in tqdm(range(n_image)):
    # Open the image
    filename_image = list_image[i]
    img = mpimg.imread(filename_image)

    # Get the physical limits
    x_min = - frame_size_x
    x_max = frame_size_x
    y_min = - frame_size_y
    y_max = frame_size_y

    fig, ax = plt.subplots(ncols=1, nrows=1, figsize=figsize, num=1, dpi=300)
    im = ax.imshow(img,  zorder=1, extent=[
                   x_min, x_max, y_min, y_max], aspect='auto')

    # Overlay the logo
    if filename_logo is not None:
        ax.imshow(logo, extent=[logo_position_x, logo_position_x + logo_width,
                                logo_position_y, logo_position_y + logo_height],
                  aspect='auto', zorder=10)  # Higher zorder to ensure it overlays

    # Add time information
    time_img = time[i]
    ax.text(0.80, 0.94, "t = {:.2f} Gyr".format(time_img),
            transform=ax.transAxes, fontsize=10, color='white')

    # Add my name
    # ax.text(0.01, 0.04, "Roduit et al 2025",
            # transform=ax.transAxes, fontsize=7, color='white', fontstyle='italic')

    # Remove the axis labels
    ax.axis('off')  # Hide axis and labels

    # Set a length scale--------------
    # Add length scale (e.g., in the bottom left corner)
    x_range = x_max - x_min
    y_range = y_max - y_min

    # Set position for the scale bar in physical units
    scale_start_x = x_min + 0.01 * x_range  # 10% from the left
    scale_end_x = scale_start_x + scale_length  # Add 1 kpc to the start position
    scale_y = y_min + 0.99 * y_range  # 99% from the bottom

    ax.plot([scale_start_x, scale_end_x], [
            scale_y, scale_y], color='white', lw=1)

    # Center the text with respect to the scale length
    # Midpoint between scale_start_x and scale_end_x
    text_x = (scale_start_x + scale_end_x) / 2
    ax.text(text_x, scale_y - 0.095*frame_size_y,
            scale_text, color='white', fontsize=10, ha="center")
    # ------------------

    # Finally save the image
    output_path = os.path.join(
        output_folder, f"{os.path.basename(filename_image)}")
    plt.savefig(output_path, bbox_inches='tight', pad_inches=0,
                dpi=dpi)

    # Close the figure to prevent overlap in the next loop. This also speeds up the script by a factor of 10
    plt.close(fig)
