# Film size properties
ratio = 16/9
resolution = 1920

# Size of the simulation region to capture (in internal units)
sim_height = 50
sim_width = ratio*sim_height

# Frame of the
frame_heigth = int(resolution/ratio)
frame_witdh = resolution #int(ratio*frame_heigth)

# Outputs format of the images (png or fits)
ouptut_format = 'fits'

# What do we what to image
component_type = ['gas', 'stars', 'halo']
component_number = len(component_type)
palette = ['cyan', 'yellow', 'light']
frame_size = 1920
view_point = ['xz'] #, 'xy', 'yz']
smoothing = [10, 5, 7.5]

film = {}
film['ftype'] = 'swift'
film['time'] = 'nb.atime'
film['exec'] = ''

film['format'] = ouptut_format  # fits
film['frames'] = []

for k in range(len(view_point)):
    frame = {}
    frame['width'] = frame_witdh
    frame['height'] = frame_heigth
    frame['size'] = (sim_width, sim_height)
    frame['tdir'] = None
    frame['pfile'] = None
    frame['exec'] = None
    frame['macro'] = None
    frame['components'] = []
    frame['xp'] = None
    frame['x0'] = None
    frame['view'] = view_point[k]

    # frame['palette'] = palette[i]
    for i in range(component_number):
        # Component of the nb object (gas, stars, sink, halo)
        component = {}
        component['id'] = component_type[i]

        # For DM, compute a SPH based smoothing length to smooth the image
        if component_type[i] == 'halo':
            component['exec'] = 'nbfc.rsp = nbfc.get_rsp_approximation() ; nbfc.Hsml = nbfc.rsp ; nbfc.ComputeSph(DesNumNgb = 10, MaxNumNgbDeviation=10) ; nbfc.rsp = nbfc.Hsml'

        component['frsp'] = smoothing[i]
        frame['components'].append(component)

    # append frame to film
    film['frames'].append(frame)
