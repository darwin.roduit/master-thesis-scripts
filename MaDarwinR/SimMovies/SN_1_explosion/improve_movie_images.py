import matplotlib.pyplot as plt
import matplotlib.image as mpimg
import matplotlib.patches as patches
import numpy as np
import os
import glob
from pNbody import *
from tqdm import tqdm

# %% Functions


# %% Main script

snap_folder = "snap_movie"
image_folder = "png_movie"
output_folder = "png_movie_processed2"

frame_size_x = 25  # kpc
frame_size_y = 25  # kpc

scale_length_kpc = 1  # kpc
scale_text = '1 ckpc'

# Create the output directory
if not os.path.exists(output_folder):
    os.makedirs(output_folder)

# Get the list of snapshot in the snapshot folder
pattern_snap = os.path.join(snap_folder, 'snapshot_*.hdf5')
list_snap = glob.glob(pattern_snap)
n_snap = len(list_snap)

# Get the list of all images
pattern = os.path.join(image_folder, '*_0000-gas-000000.png')
list_image = glob.glob(pattern)
n_images = len(list_image)

figsize = (10, 10)


if n_images != n_snap:
    raise RuntimeError(
        "The number of images is not the same as the number of snapshot. It must be the same to properly get meaningful physical information, e.g. simulation time")

for i in tqdm(range(n_snap)):
    # Open the image
    filename_image = list_image[i]
    img = mpimg.imread(filename_image)
    dpi=img.shape[0] // figsize[0]

    # Open the snapshot, only load the star
    filename_snap = list_snap[i]
    nb = Nbody(filename_snap)  # units are kpc
    nb_star = Nbody(filename_snap, ptypes=[4])

    # Get the physical limits
    x_min = - frame_size_x
    x_max =  frame_size_x
    y_min = - frame_size_y
    y_max =  frame_size_y

    fig, ax = plt.subplots(ncols=1, nrows=1, figsize=figsize, num=1, dpi=300)
    im = ax.imshow(img,  zorder = 1, extent=[x_min, x_max, y_min, y_max], aspect='equal')

    # Add time information
    time = nb.Time(units='Myr')
    ax.text(0.45, 0.95, "$t = {:.2f}".format(time) + "$",
            transform=ax.transAxes, fontsize=12, color='white')

    # Remove the axis labels
    ax.axis('off')  # Hide axis and labels

    # Set a length scale--------------
    # Add length scale (e.g., in the bottom left corner)
    x_range = x_max - x_min
    y_range = y_max - y_min

    # Set position for the scale bar in physical units
    scale_start_x = x_min + 0.1 * x_range  # 10% from the left
    scale_end_x = scale_start_x + scale_length_kpc  # Add 1 kpc to the start position
    scale_y = y_min + 0.1 * y_range  # 10% from the bottom

    ax.plot([scale_start_x, scale_end_x], [
            scale_y, scale_y], color='white', lw=3)
    ax.text(scale_start_x, scale_y + 1,
            scale_text, color='white', fontsize=12)
    # ------------------

    # Plot the position of the star
    has_star = nb.npart[4] != 0

    if has_star:
        # Recall that this movie is in the xz plane! So the y axis is the z axis. So the right coordinate is z
        pos_star = nb_star.pos.flatten()
        x_star = pos_star[0]
        y_star = pos_star[2]
        ax.plot(x_star, y_star, 'y*', markersize=5)

    # Finally save the image
    output_path = os.path.join(
        output_folder, f"{os.path.basename(filename_image)}")
    plt.savefig(output_path, bbox_inches='tight', pad_inches=0,
                dpi=dpi)

    # Close the figure to prevent overlap in the next loop. This also speeds up the script by a factor of 10
    plt.close(fig)
