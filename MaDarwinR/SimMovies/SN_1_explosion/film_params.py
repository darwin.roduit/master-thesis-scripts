# global film
width = 25
height = 25
frame_size = 1920
component_type = ['gas']
component_number = len(component_type)
palette = ['light', 'yellow', 'light']
ouptut_format = 'png'  # or png
view_point = ['xz', 'xy', 'yz']
smoothing = [10, 5, 7.5]


film = {}
film['ftype'] = 'swift'
film['time'] = 'nb.atime'
film['exec'] = ''

film['format'] = ouptut_format  # fits
film['frames'] = []


for k in range(len(view_point)):
    frame = {}
    frame['width'] = frame_size
    frame['height'] = frame_size
    frame['size'] = (width, height)
    frame['tdir'] = None
    frame['pfile'] = None
    frame['exec'] = None
    frame['macro'] = None
    frame['components'] = []
    frame['xp'] = None
    frame['x0'] = None
    frame['view'] = view_point[k]

    # frame['palette'] = palette[i]
    for i in range(component_number):
        # Component of the nb object (gas, stars, sink, halo)
        component = {}
        component['id'] = component_type[i]

        # For DM, compute a SPH based smoothing length to smooth the image
        if component_type[i] == 'halo':
            component['exec'] = 'nbfc.rsp = nbfc.get_rsp_approximation() ; nbfc.Hsml = nbfc.rsp ; nbfc.ComputeSph(DesNumNgb = 10, MaxNumNgbDeviation=10) ; nbfc.rsp = nbfc.Hsml'

        component['frsp'] = smoothing[i]
        frame['components'].append(component)

    # append frame to film
    film['frames'].append(frame)
