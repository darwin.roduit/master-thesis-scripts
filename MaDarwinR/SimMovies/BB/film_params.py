# Film size properties
ratio = 16/9
resolution = 1920

# Size of the simulation region to capture (in internal units)
sim_width = 2000
sim_height = ratio*sim_width

# Frame of the
frame_witdh = resolution
frame_heigth = int(ratio*frame_witdh)

# Outputs format of the images (png or fits)
ouptut_format = 'png'

# What do we what to image
component_type = ['gas']
component_number = len(component_type)

# How (colors, point of view, distance, etc)
palette = ['cyan', 'yellow', 'light']
view_point = ['xy']
smoothing = [0.075, 0]
x_p = [1e4, 1e4, 1e4] #Observing position, i.e where we are looking at
x_0 = [1e4, 1e4, 12000] #Position of the observer


# Global film ----------------
film = {}
film['ftype'] = 'swift'
film['time'] = 'nb.Time(units="Myr")*1000'
film['exec'] = ''

film['format'] = ouptut_format  # fits
film['frames'] = []

# Frame ----------------------

for k in range(len(view_point)):
    frame = {}
    frame['width'] = frame_heigth
    frame['height'] = frame_witdh
    frame['size'] = (sim_height, sim_width)
    frame['tdir'] = None
    frame['pfile'] = None
    frame['exec'] = None
    frame['macro'] = None
    frame['components'] = []
    frame['xp'] = x_p
    frame['x0'] = x_0
    frame['view'] = view_point[k]

    for i in range(component_number):
        # Component of the nb object (gas, stars, sink, halo)
        component = {}
        component['id'] = component_type[i]
        component['exec'] = 'nbfc.hsml = nbfc.rsp'
        component['frsp'] = smoothing[i]
        frame['components'].append(component)

    # append frame to film
    film['frames'].append(frame)
