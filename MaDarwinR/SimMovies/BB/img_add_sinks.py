#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Aug 29 10:02:07 2024

@author: darwinr
"""

import matplotlib.pyplot as plt
import matplotlib.image as mpimg
import matplotlib.patches as patches
from skimage.transform import resize
import numpy as np
import os
import glob
from pNbody import *
from tqdm import tqdm

# %% Functions


# %% Main script
###########################
# Files and units
###########################

snap_folder = "snap"
image_folder = "png"
output_folder = "png_movie_processed"
filename_logo = "EPFL_Logo_Digital_RGB_PROD.png"

###########################
# Images
###########################
# Set the ratio of the image and the figsize
ratio = 16/9
frame_size_x = 2000  # UA
frame_size_y = ratio*frame_size_x  # UA
frame_size_x,frame_size_y = frame_size_y, frame_size_x  #
figsize = (8, 4.5)

# "Ruler"  = scale-lenght bar
scale_length_kpc = 1000  # UA
scale_text = '1000 AU'

# Load matplotlib stylesheet
plt.style.use('movie_stylesheet.mplstyle')

# Create the output directory
os.makedirs(output_folder, exist_ok=True)

# Get the list of snapshot in the snapshot folder
pattern_snap = os.path.join(snap_folder, 'snapshot_*.hdf5')
list_snap = glob.glob(pattern_snap)
n_snap = len(list_snap)

# Get the list of all images
pattern = os.path.join(image_folder, '*_0000-gas-000000.png')
list_image = glob.glob(pattern)
n_images = len(list_image)

if n_images != n_snap:
    raise RuntimeError(
        "The number of images is not the same as the number of snapshot. It must be the same to properly get meaningful physical information, e.g. simulation time")

# Sort the two lists
list_snap.sort()
list_image.sort()

###########################
# Logo
###########################
#Open the logo file
logo = mpimg.imread(filename_logo)

# Resize the logo
scale_factor = 0.5  # Adjust this factor to reduce or increase the logo size
logo_resized = resize(logo,
                      (int(logo.shape[0] * scale_factor), int(logo.shape[1] * scale_factor)),
                      anti_aliasing=True)
logo = logo_resized
logo_height, logo_width, _ = logo.shape

# Logo position
logo_position_x = frame_size_x - logo_width
logo_position_y = -0.99*frame_size_y

###########################
# Now compose your image
###########################
# n_snap = 1
for i in tqdm(range(n_snap)):
    # Open the image
    filename_image = list_image[i]
    img = mpimg.imread(filename_image)
    dpi=img.shape[0] // figsize[0]

    # Open the snapshot, only load the star
    filename_snap = list_snap[i]
    print(filename_snap)
    nb = Nbody(filename_snap)  # units are kpc
    nb_sink = Nbody(filename_snap, ptypes=[3])

    # Get the physical limits
    x_min = - frame_size_x
    x_max =  frame_size_x
    y_min = - frame_size_y
    y_max =  frame_size_y

    fig, ax = plt.subplots(ncols=1, nrows=1, figsize=figsize, num=1, dpi=300)
    im = ax.imshow(img,  zorder = 1, extent=[x_min, x_max, y_min, y_max], aspect='auto')

    # Overlay the logo
    ax.imshow(logo, extent=[logo_position_x, logo_position_x + logo_width,
                            logo_position_y, logo_position_y + logo_height],
               aspect='auto', zorder=10)  # Higher zorder to ensure it overlays

    # Add time information
    time = nb.Time(units='Myr')*1000
    ax.text(0.80, 0.94, "t = {:.2f} kyr".format(time) ,
            transform=ax.transAxes, fontsize=10, color='white')

    # Remove the axis labels
    ax.axis('off')  # Hide axis and labels

    # Set a length scale--------------
    # Add length scale (e.g., in the bottom left corner)
    x_range = x_max - x_min
    y_range = y_max - y_min

    # Set position for the scale bar in physical units
    scale_start_x = x_min + 0.01 * x_range  # 10% from the left
    scale_end_x = scale_start_x + scale_length_kpc  # Add 1 kpc to the start position
    scale_y = y_min + 0.99 * y_range  # 99% from the bottom

    ax.plot([scale_start_x, scale_end_x], [
            scale_y, scale_y], color='white', lw=1)

    # Center the text with respect to the scale length
    text_x = (scale_start_x + scale_end_x) / 2  # Midpoint between scale_start_x and scale_end_x
    ax.text(text_x , scale_y - 0.095*frame_size_y,
            scale_text, color='white', fontsize=10, ha="center")
    # ------------------

    # Plot the position of the star
    has_sink = nb.npart[3] != 0

    if has_sink:
        # Recall that this movie is in the xz plane! So the y axis is the z axis. So the right coordinate is z
        pos_sink = nb_sink.pos
        x_sink = pos_sink[:, 0]
        y_sink = pos_sink[:, 2]
        ax.plot(x_sink, y_sink, 'y*', markersize=5)

    # Finally save the image
    output_path = os.path.join(
        output_folder, f"{os.path.basename(filename_image)}")
    plt.savefig(output_path, bbox_inches='tight', pad_inches=0,
                dpi=dpi)

    # Close the figure to prevent overlap in the next loop. This also speeds up the script by a factor of 10
    plt.close(fig)
