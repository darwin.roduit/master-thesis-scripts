#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jan 30 14:54:35 2024
Determines the resolution of the SPH simulation to avoid artificial fragmentation

@author: darwin
"""

import numpy as np
from astropy import constants as cte
import astropy.units as u
from matplotlib import pyplot as plt
import argparse
from pNbody.misc import scripts_utils


#%%

def isothermal_sound_speed(mu, T, k_B, m_p):
    return np.sqrt(k_B*T/(mu*m_p))

def lambda_jeans(c_s, rho, G):
    return (np.pi*c_s**2/(G*rho))**0.5

def mass_jeans(c_s, rho, G):
    r = lambda_jeans(c_s, rho, G)/2
    return 4/3 * np.pi * rho * r**3

def rho_critic(c_s, G, N_tot, N_neigh, M_tot, f=2):
    prefactor = np.pi**5/(36*G**3) * c_s**6
    result = prefactor * (N_tot/(f*N_neigh) * 1/M_tot)**2
    return result

#%%

def parse_option():
    description = """
This script determines the minimal resolvable mass and density for SPH simulations. It also
provides the minimal Jeans length.
Then it advises how to choose the minimal SPH smoothing length h_min to avoid the creation of
artificial clumps, which are caused by underestimating the gas density if h_min is too large. Also,
for simulations with constant gravitational softening length, the script helps to choose a
softening length that avoids (for a given expected density).

More details are given in the cited papers.

The first part of the script relies on the same computations than Bate and Burkert 1997, but with
the alternate definition of the Jeans length:
                                              lambda_J = sqrt(np.pi*c_s**2/(G*rho)).
Note: This part assumes that all gas particles have the same mass.

This part prints:
    - rho_crit : maximal density the SPH simulation can resolve. Densities beyond that cannot
      resolve collapse and will rely on numerical implementations details. Notice that if above such
      densities there are sink particles, the collapse is not a problem anymore
      (See Bate and Burkert 1997).
    - lambda_J_min : minimal resolvable Jeans length (at rho_crit). It allows to determine the sink
      accretion radius, which should be a multiple of this Jeans length.
    - M_J_min : minimal resolvable mass.
    - h : The SPH smoothing length corresponding the M_J_min. It is the lambda_J_min / 2.
    - epsilon : Advised gravitational softening length (=h). Notice that the SPH smoothing length
      should not be smaller than epsilon ((See Bate and Burkert 1997).


The second part of the script relies on the reasoning of Ploeckinger et al. (2024).
This part prints:
    - h_min : The min SPH smoothing length to avoid artificial clumps.
    - h_min_ratio : For SWIFT, this the ratio between the kernel support (h*gamma_k) and the
      length-scale above which gravity is Newtonian (H=3 epsilon).
    - epsilon_min_subk: Minimum constant softening length to avoid subkernel clumping.

Note: The computations performed in this part assume the use of the Wendland C2 kernel for
SPH and gravity.


References:
    Bate and Burkert (1997) : https://ui.adsabs.harvard.edu/abs/1997MNRAS.288.1060B
    Ploeckinger et al. (2024) : https://ui.adsabs.harvard.edu/abs/2024MNRAS.528.2930P
"""
    epilog = """
Examples:
--------
python3 artificial_fragmentation.py -N 4000000
python3 artificial_fragmentation.py -N 4000000 --c_s 0.268
python3 artificial_fragmentation.py -N 4000000 -T 20 --mu 2.3
"""
    parser = argparse.ArgumentParser(description=description, epilog=epilog,
                                     formatter_class=scripts_utils.RawTextArgumentDefaultsHelpFormatter)

    parser.add_argument("--unit_Length",
                        default="AU", type=str, choices=["kpc", "pc", "AU"],
                        help="Length units to generate the ICs.")

    parser.add_argument("--unit_Mass",
                        default="1e10M_sun", type=str, choices=["M_sun", "1e10M_sun"],
                        help="Mass units to generate the ICs.")

    parser.add_argument("-N", dest="N", type=int, default=1e5,
                        help='Total number of gas particles')

    parser.add_argument("--N_neighbour", type=int, default=48,
                        help='Number of Neighbour for SPH computations.')

    # Parameters
    parser.add_argument("--M_tot",
                                             action="store",
                                             type=float,
                                             dest="Mtot",
                                             default=100,
                                             help="Total mass of the gas component in Msol.")

    parser.add_argument("--c_s",
                                             action="store",
                                             type=float,
                                             default=None,
                                             help="Sound speed of the gas in km/s.")

    parser.add_argument("--mu",
                                            type=float,
                                            default=2.3,
                                            help="Mean molecular weight of the gas.")

    parser.add_argument("-T",
                                            type=float,
                                            default=20,
                                            help="Mean temperature of the gas.")

    parser.add_argument("--m_B",
                                            type=float,
                                            default=10,
                                            help="Mass of the baryon particles in M_sun.")

    parser.add_argument("--n_sim_max",
                                            type=float,
                                            default=100,
                                            help="Highest (expected) density in a simulation in cm^(-3).")

    parser.add_argument("--n_max_subk",
                                            type=float,
                                            default=100,
                                            help="Maximal allowed subkernel density in cm^(-3).")

    return parser.parse_args()



#%%

opt = parse_option()
print("\n-------------------------------------")
print("Welcome to artifical_fragmentation script !\n")

unit_Length = opt.unit_Length
unit_Mass = opt.unit_Mass

########################################
#  Define units
########################################

#Get length units
if unit_Length == "kpc":
    unit_length_in_cgs = 1 * u.kpc
elif unit_Length == "pc":
    unit_length_in_cgs = 1 * u.pc
elif unit_Length == "AU":
    unit_length_in_cgs = 1 * u.AU
else:
    raise ValueError("Unit type {} not defined yet".format(unit_Length))

#Get mass units
if unit_Mass == "M_sun":
    unit_mass_in_cgs = 1 * u.M_sun
elif unit_Mass == "1e10M_sun":
    unit_Mass =  1e10*u.M_sun
    unit_mass_in_cgs = 1e10*u.M_sun
else:
    raise ValueError("Unit type {} not defined yet".format(unit_Length))

#Define units
u_Length = unit_length_in_cgs.to(unit_length_in_cgs)
unit_mass_in_cgs = unit_mass_in_cgs.to(unit_mass_in_cgs)
unit_velocity_in_cgs = 1 * u.km/u.s ; unit_velocity_in_cgs = unit_velocity_in_cgs.to(unit_velocity_in_cgs)
unit_time_in_cgs = unit_length_in_cgs/unit_velocity_in_cgs ; unit_time_in_cgs = unit_time_in_cgs.to(unit_time_in_cgs)

u_dict = {}
u_dict["UnitLength_in_cm"] = unit_length_in_cgs.to(u.cm).value
u_dict["UnitMass_in_g"] = unit_mass_in_cgs.to(u.g).value
u_dict["UnitVelocity_in_cm_per_s"] = unit_velocity_in_cgs.to(u.cm/u.s).value
u_dict["Unit_time_in_cgs"] = unit_time_in_cgs.to(u.s).value

# Define constants values
G_unit = cte.G.to(unit_length_in_cgs**3/(unit_mass_in_cgs*unit_time_in_cgs**2))
# G = G_unit.value
G = G_unit

print(
    """The units are :
 UnitLength_in_cm = {:e} cm
 UnitMass_in_g = {:e} g
 UnitVelocity_in_cm_per_s = {:e} cm/s
 Unit_time_in_s = {:e} s
 G = {:e}
""".format(u_dict["UnitLength_in_cm"], u_dict["UnitMass_in_g"],
           u_dict["UnitVelocity_in_cm_per_s"], u_dict["Unit_time_in_cgs"], G))


########################################
#  Parameters preparation
########################################


#Number of particles
N_part = opt.N

#Number of Neighbours
N_neigh = opt.N_neighbour

#Factor for the Neighbour particles
f = 2

# Convert M_tot to right units
M_tot = opt.Mtot*u.M_sun  ; M_tot = M_tot.to(unit_mass_in_cgs)

# Convert c_s to right units
T = opt.T
mu = opt.mu
m_p = cte.m_p
k_B = cte.k_B.value *u.kg*u.meter**2 /u.s**2

########################################
#  Computations
########################################


if opt.c_s is None:
    c_s = isothermal_sound_speed(mu, T, k_B, m_p)
    c_s = c_s.to(unit_velocity_in_cgs)
else:
    c_s = opt.c_s*u.km/u.s ; c_s = c_s.to(unit_velocity_in_cgs)

rho_crit = rho_critic(c_s, G, N_part, N_neigh, M_tot, f)
lambda_J_min = lambda_jeans(c_s, rho_crit, G)
M_J_min = mass_jeans(c_s, rho_crit, G)
h_min = lambda_J_min/2

print("------------------------------------------------------------------------------------------")
print("Information based on Bate and Burkert (1997)")
print("c_s = {:.2e} km/s".format(c_s.to(u.km/u.s).value))
print("rho_crit = {:e}".format(rho_crit.to(u.g/u.cm**3)))
print("rho_crit = {:e}".format(rho_crit.to(u.g/u.cm**3)/m_p.to(u.g)))
print("rho_crit = {:e} unit_mass_in_cgs/unit_length_in_cgs^3".format(rho_crit.to(unit_mass_in_cgs/unit_length_in_cgs**3).value))
print("lambda_J min = {:e}".format(lambda_J_min.to(u.cm)))
print("lambda_J min = {:e} unit_length_in_cgs".format(lambda_J_min.to(unit_length_in_cgs).value))
print("M_J min = {:e}".format(M_J_min.to(u.M_sun)))
print("M_J min = {:e} unit_mass_in_cgs".format(M_J_min.to(unit_mass_in_cgs).value))
print("The corresponding SPH smoothing length is h = lambda_J_min / 2 = {:e} unit_length_in_cgs".format(h_min.to(unit_length_in_cgs).value))
print("Thus the gravitational softening length must be epsilon = h = {:e} unit_length_in_cgs".format(h_min.to(unit_length_in_cgs).value))
print("------------------------------------------------------------------------------------------")

#Now, compute the criteria based on Ploeckinger et al (2024)
#This worls for the Wendland C2 kernel
mass_baryon = opt.m_B*u.M_sun
n_sim_max = opt.n_sim_max/u.cm**3 #density in cm^(-3)
h_min_upper_bound = 8.3*u.pc*(mass_baryon/(10**5*u.M_sun))**(1/3) * (n_sim_max/(10**4*u.cm**(-3)))**(-1/3)

#Now, compute the softening length (plummer equivalent) bound to avoid subkernel clumping
n_max_subk = opt.n_max_subk/u.cm**3
epsilon_min_subk = 160/1.5*u.pc*(mass_baryon/(10**5*u.M_sun))**(1/3) * (n_max_subk/(10*u.cm**(-3)))**(-1/3)


#Put a note on the resolution problems if we are under the resolution limit, etc.
print("Information based on Ploeckinger et al. (2024)")
print("h_min < {:.2e} pc".format(h_min_upper_bound.value))
print("h_min < {:.2e} unit_length_in_cgs".format(h_min_upper_bound.to(unit_length_in_cgs).value))
print("epsilon_min_subk > {:.2e} pc".format(epsilon_min_subk.value))
print("epsilon_min_subk > {:.2e} unit_length_in_cgs".format(epsilon_min_subk.to(unit_length_in_cgs).value))


#Now, to compute the h_min_ratio, the user enters values
h_min_desired = float(input("Enter the target h_min:   "))
epsilon_desired = float(input("Enter the target epsilon:   "))
h_min_ratio = h_min_desired/(epsilon_desired*1.55)
print("h_min_ratio = {:.2e}".format(h_min_ratio))
print("------------------------------------------------------------------------------------------")


print("\nEND-------------------------------------")
