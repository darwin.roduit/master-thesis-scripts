import sys
import os

#Those two lines allows to run this script from whatever directory, ex : in TP4b :  python3 Scripts/data_analysis_module/mass_given_radius.py -h
SCRIPT_DIR = os.path.dirname(os.path.abspath(__file__))
sys.path.append(os.path.dirname(SCRIPT_DIR))


import numpy as np
from pNbody import *
from pNbody import Nbody
from pNbody import pychem

from scipy.stats import binned_statistic

def star_has_exploded(mass_init, mass_current):
    """
    Determines the stars that have exploded


    Parameters
    ----------
    mass_init : array of float with shape (N, )
        Mass at birth of the stars.
    mass_current : array of float with shape (N,)
        Current mass of the stars.

    Returns
    -------
    has_exploded : array of Bool, shape (N,)
        Array telling which star has exploded.

    """
    #Compute the mass difference
    Delta_m = mass_init - mass_current

    #If the stars have exploded, Delta_m > 0 (because m_current <= m_init, stars do not get more massive)
    has_exploded = Delta_m > 0
    return has_exploded

def get_exploded_stars(nb):
    """
    Separate exploded stars from non exploded particles

    Parameters
    ----------
    nb : Nbody
        Nbody object containing the data. It can contains other particles than stars.

    Returns
    -------
    nb_non_exploded : Nbody
        Nbody object containing all particles that have not exploded, including gas, DM, stars, etc.
    nb_exploded : Nbody
        Nbody object containing all stars that have exploded,.

    """
    has_exploded = star_has_exploded(nb.minit, nb.mass)
    indices_exploded = np.argwhere(has_exploded).ravel()
    indices_non_exploded = np.argwhere(np.logical_not(has_exploded)).ravel()

    #Return an nBody object with the exploded stars and one with the non exploded particles
    nb_exploded = nb.selectp(nb.num[indices_exploded])
    nb_non_exploded = nb.selectp(nb.num[indices_non_exploded])
    return nb_non_exploded, nb_exploded


def is_IMF_continuous_part(nb_star):
    """
    Determines which stars within nb_star are from the continuous part of the IMF.

    Parameters
    ----------
    nb_star : Nbody object containing *only* stars


    Returns
    -------
    is_IMF_continuous_part: array of bool
        Array determining if a given star is from the continuous part of the IMF or not.

    """
    return np.equal(nb_star.sp_type, 1)

def compute_IMF(nb_star, mass_s, n_bins):
    # is_IMF_continuous_part = np.isclose(mass_s, m_star_part_continuous, rtol=1e-3)
    is_IMF_continuous_part = np.equal(nb_star.sp_type, 1)
    m_continuous = mass_s[np.argwhere(is_IMF_continuous_part)]
    m_discrete = mass_s[np.argwhere(np.logical_not(is_IMF_continuous_part))]

    #Compute bins and counts
    count_m_discrete, m_discrete_bin_edges = np.histogram(m_discrete, bins=n_bins)
    count_m_continuous = np.array(len(m_continuous))
    count_m_continuous = count_m_continuous.reshape((1))
    m_continuous_bin_edges = np.zeros((1))

    count_m = np.concatenate((count_m_continuous, count_m_discrete))
    m_bin_edges = np.concatenate((m_continuous_bin_edges, m_discrete_bin_edges))
    return count_m_discrete, m_discrete_bin_edges, count_m_continuous, count_m, m_bin_edges

def get_expected_imf_masses(M_star_continuous, M_cont_max =8, imf_Mmax=30, imf_Mmin=0.05, imf_a_coeff = [0.7,-0.8,-1.7,-1.3], imf_m_s = [0.08,0.5,1.0]     ):

    imf_params = {}
    imf_params["Mmax"] = imf_Mmax
    imf_params["Mmin"] = imf_Mmin
    imf_params["as"] = imf_a_coeff
    imf_params["ms"] = imf_m_s
    pychem.set_parameters(imf_params)

    Mct = M_star_continuous      # "stellar particle" mass (continuous part) [solar mass]
    mmax_c = M_cont_max   # maximal mass of the continuous part of the IMF [solar mass]

    # get minimal and maximal mass
    mmin = pychem.get_Mmin()
    mmax = pychem.get_Mmax()

    # get the IMF mass of the continous part
    m1 = np.array([mmin])
    m2 = np.array([mmax_c])
    Mc = pychem.get_imf_M(m1,m2)

    # get the IMF mass of the discrete part
    m1 = np.array([mmax_c])
    m2 = np.array([mmax])
    Md = pychem.get_imf_M(m1,m2)

    # compute the total IMF mass
    M0 = Mct/Mc
    Md = M0-Mct
    Mc = Mct

    return Mc, Md
