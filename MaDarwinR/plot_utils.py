#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Nov 13 09:46:21 2024

@author: darwinr
"""

import numpy as np
from MaDarwinR.analysis_functions import is_IMF_continuous_part
from pNbody import *

def choose_x_axis_star_properties(x_axis, nb_star):
    if x_axis == "mass":
        observable = "M"
        units = r"[M$_\odot$]"
        x_data = nb_star.Mass(units="Msol")
    elif x_axis == "Fe/H":
        observable = "[Fe/H]"
        units = ""
        x_data = nb_star.AbRatio("Fe", "H")
    elif x_axis == "Ca/Fe":
        observable = "[Ca/Fe]"
        units = ""
        x_data = nb_star.CaFe()
    elif x_axis == "Mg/Fe":
        observable = "[Mg/Fe]"
        units = ""
        x_data = nb_star.MgFe()
    elif x_axis == "Z" or x_axis == "metallicity":
        observable = "Metallicity"
        units = ""
        x_data = nb_star.MetalsH()
    elif x_axis == "birth_time":
        observable = "Birth time"
        units = "[Gyr]"
        x_data = nb_star.StellarFormationTime(units="Gyr")
    else:
        raise ValueError("Observable {} not yet implemented".format(x_axis))

    return x_data, observable, units

def choose_y_axis_star_properties(y_axis, nb_star):
    if y_axis == "mass":
        observable = "M"
        units = r"[M$_\odot$]"
        y_data = nb_star.Mass(units="Msol")
    elif y_axis == "Fe/H":
        observable = "[Fe/H]"
        units = ""
        y_data = nb_star.AbRatio("Fe", "H")
    elif y_axis == "Ca/Fe":
        observable = "[Ca/Fe]"
        units = ""
        y_data = nb_star.CaFe()
    elif y_axis == "Mg/Fe":
        observable = "[Mg/Fe]"
        units = ""
        y_data = nb_star.MgFe()
    elif y_axis == "Mg/Ca":
        observable = "[Mg/Ca]"
        units = ""
        y_data = nb_star.AbRatio("Mg", "Ca")
    elif y_axis == "L_V" or y_axis == "luminosity":
        observable = "L"
        units = "[L$_\odot$]"
        y_data = nb_star.Luminosity()
    elif y_axis == "Z" or y_axis == "metallicity":
        observable = "Metallicity"
        units = ""
        y_data = nb_star.MetalsH()
    else:
        raise ValueError("Observable {} not yet implemented".format(y_axis))

    return y_data, observable, units

def select_stars(nb_star, star_type, popIII_threshold=-5):
    if star_type == "all":
        nb_select = nb_star
    elif star_type == "PopII":
        is_PopII = (nb_star.Fe() >= popIII_threshold)
        nb_select =  nb_star.selectc(is_PopII)
    elif star_type == "PopIII":
        is_PopIII = (nb_star.Fe() < popIII_threshold)
        nb_select = nb_star.selectc(is_PopIII)
    elif star_type == "single stars":
        is_discrete = np.logical_not(is_IMF_continuous_part(nb_star))
        nb_select = nb_star.selectc(is_discrete)
    elif star_type == "star population":
        is_IMF_continuous = is_IMF_continuous_part(nb_star)
        nb_select = nb_star.selectc(is_IMF_continuous)
    else:
        raise ValueError("Star type {} not yet implemented".format(star_type))
    return nb_select

def set_label(log, observable, units):
    if log:
        label = r"$\log_{10}$"
    else:
        label =""

    label += r"{}".format(observable) + " " + units
    return label
