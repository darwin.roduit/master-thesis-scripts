#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Mar 12 09:23:06 2024

@author: darwin
"""

import sys
import os

#Those two lines allows to run this script from whatever directory, ex : in TP4b :  python3 Scripts/data_analysis_module/mass_given_radius.py -h
# SCRIPT_DIR = os.path.dirname(os.path.abspath(__file__))
# sys.path.append(os.path.dirname(SCRIPT_DIR))

import argparse
import numpy as np


class ExplicitDefaultsHelpFormatter(argparse.ArgumentDefaultsHelpFormatter):
    """Prints the default arguments when they are provided."""

    def _get_help_string(self, action):
        #If we have an array
        if not isinstance(action.default, (np.ndarray)):
            if action.default in (None, False):
                return action.help
        return super()._get_help_string(action)


class RawTextArgumentDefaultsHelpFormatter(ExplicitDefaultsHelpFormatter,
                                           argparse.RawTextHelpFormatter):
    """Provides the defaults values and allows to format the text help."""
    pass


class store_as_array(argparse._StoreAction):
    """Provides numpy array as argparse arguments."""

    def __call__(self, parser, namespace, values, option_string=None):
        values = np.array(values)
        return super().__call__(parser, namespace, values, option_string)
