#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Nov 11 19:19:30 2024

@author: darwinr
"""

import subprocess
import os

#%%
def analyze_folders(folder_list, file_name, commands):
    for folder in folder_list:
        # Define the path to the file and output directory
        snap_dir = os.path.join(folder, "snap")
        file_path = os.path.join(snap_dir, file_name)

        if not os.path.exists(file_path):
            print(f"File {file_path} not found. Skipping folder '{folder}'...")
            continue

        print(f"Processing folder: {folder}")
        for command in commands:
            # Replace {file} with the actual file path and {output} with the snap directory path
            full_command = command.format(file=file_path, output=snap_dir)
            print(f"Running command: {full_command}")

            try:
                subprocess.run(full_command, shell=True, check=True)
            except subprocess.CalledProcessError as e:
                print(f"Error occurred while processing command: {full_command}")
                print(e)

# Define the folders, file name, and commands to be executed
# folders = ["h177/mahsa", "h177/smag_0", "h177/smag_0_045", "h177/smag_0_08", "h177/smag_0_1", "h177/smag_0_45", "h177/smag_1"]
folders = ["h177/sink_smoothed"]
# folders = ["h187/smag_0_045", "h187/smag_0_45", "h187/smag_0_0045"]
# folders = ["h190/smag_0_045", "h190/smag_0_0045"]
# file_name = "UFD_0172.hdf5"
# file_name = "snapshot_0084.hdf5"
file_name = "snapshot_0026.hdf5"
commands = [
    "madr_plot_sfr.py {file} --x_min 0.0 --n_bins 200 --output_location {output}/SFR",
    "madr_plot_integrated_mass.py {file} --x_min 0.0 --n_bins 200 --output_location {output}/imass",
    "madr_plot_star_chemistry.py {file} --x_axis 'Fe/H' --y_axis 'Mg/Fe' --x_min -6 --y_min -1 --y_max 1 --output_location {output}/MgFe_v_FeH",
    "madr_plot_star_chemistry.py {file} --x_axis 'Fe/H' --y_axis 'Ca/Fe' --x_min -6 --y_min -1 --y_max 1 --output_location {output}/CaFe_v_FeH",
    "madr_plot_star_chemistry.py {file} --x_axis 'Fe/H' --y_axis 'L_V' --x_min -6 --output_location {output}/Lv_v_FeH",
    "madr_plot_star_chemistry.py {file} --x_axis 'Fe/H' --y_axis 'Mg/Ca' --x_min -6 --y_min -1 --y_max 1 --output_location {output}/MgCa_v_FeH",
    "madr_plot_star_chemistry_dispersion.py {file} --x_axis 'Fe/H' --y_axis 'Mg/Fe' --x_min -6 --y_min -1 --y_max 1 --output_location {output}/sigma_MgFe_v_FeH",
    "madr_plot_mdf.py {file} --x_min -20 --x_max 0 --output_location {output}/MDF --n_bins 100"
    # "madr_plot_gal_FeH_v_Lv.py {file} --output_location {output}/FeH_v_Lv --n_bins 50 --log x"
]

# Run the analysis
analyze_folders(folders, file_name, commands)
