import numpy as np
import matplotlib.pyplot as plt
from scipy.stats import binned_statistic

#%%
# Function to load data from a file
def load_mgfe_data(file_path):
    return np.load(file_path, allow_pickle=True)

# Function to plot Mg/Fe vs Fe/H data
def plot_mgfe_data(ax, data, shift, label, color, marker='.', alpha=1.0, markersize=2.0):
    ax.plot(data["x_observable"], data["y_observable_std"] + shift, linestyle='-', marker=marker, label=label, color=color, alpha=alpha, markersize=markersize)

# Function to configure plot axes
def configure_axes(ax, x_min, x_max, y_min, y_max):
    ax.set_xlabel("[Fe/H]")
    ax.set_ylabel("$\sigma_\mathrm{[Mg/Fe]}$")
    ax.legend()

    # Set the axis limits, allowing None to auto-determine
    x_left, x_right = ax.get_xlim()
    y_left, y_right = ax.get_ylim()

    x_min = x_left if x_min is None else x_min
    x_max = x_right if x_max is None else x_max
    y_min = y_left if y_min is None else y_min
    y_max = y_right if y_max is None else y_max

    ax.set_xlim([x_min, x_max])
    ax.set_ylim([y_min, y_max])

#%%
# Open-color colors from https://yeun.github.io/open-color/
colors = {
    "gray": {
        0: "#f8f9fa",
        1: "#f1f3f5",
        2: "#e9ecef",
        3: "#dee2e6",
        4: "#ced4da",
        5: "#adb5bd",
        6: "#868e96",
        7: "#495057",
        8: "#343a40",
        9: "#212529",
    },
    "red": {
        0: "#fff5f5",
        1: "#ffe3e3",
        2: "#ffc9c9",
        3: "#ffa8a8",
        4: "#ff8787",
        5: "#ff6b6b",
        6: "#fa5252",
        7: "#f03e3e",
        8: "#e03131",
        9: "#c92a2a",
    },
    "pink": {
        0: "#fff0f6",
        1: "#ffdeeb",
        2: "#fcc2d7",
        3: "#faa2c1",
        4: "#f783ac",
        5: "#f06595",
        6: "#e64980",
        7: "#d6336c",
        8: "#c2255c",
        9: "#a61e4d",
    },
    "grape": {
        0: "#f8f0fc",
        1: "#f3d9fa",
        2: "#eebefa",
        3: "#e599f7",
        4: "#da77f2",
        5: "#cc5de8",
        6: "#be4bdb",
        7: "#ae3ec9",
        8: "#9c36b5",
        9: "#862e9c",
    },
    "violet": {
        0: "#f3f0ff",
        1: "#e5dbff",
        2: "#d0bfff",
        3: "#b197fc",
        4: "#9775fa",
        5: "#845ef7",
        6: "#7950f2",
        7: "#7048e8",
        8: "#6741d9",
        9: "#5f3dc4",
    },
    "indigo": {
        0: "#edf2ff",
        1: "#dbe4ff",
        2: "#bac8ff",
        3: "#91a7ff",
        4: "#748ffc",
        5: "#5c7cfa",
        6: "#4c6ef5",
        7: "#4263eb",
        8: "#3b5bdb",
        9: "#364fc7",
    },
    "blue": {
        0: "#e7f5ff",
        1: "#d0ebff",
        2: "#a5d8ff",
        3: "#74c0fc",
        4: "#4dabf7",
        5: "#339af0",
        6: "#228be6",
        7: "#1c7ed6",
        8: "#1971c2",
        9: "#1864ab",
    },
    "cyan": {
        0: "#e3fafc",
        1: "#c5f6fa",
        2: "#99e9f2",
        3: "#66d9e8",
        4: "#3bc9db",
        5: "#22b8cf",
        6: "#15aabf",
        7: "#1098ad",
        8: "#0c8599",
        9: "#0b7285",
    },
    "teal": {
        0: "#e6fcf5",
        1: "#c3fae8",
        2: "#96f2d7",
        3: "#63e6be",
        4: "#38d9a9",
        5: "#20c997",
        6: "#12b886",
        7: "#0ca678",
        8: "#099268",
        9: "#087f5b",
    },
    "green": {
        0: "#ebfbee",
        1: "#d3f9d8",
        2: "#b2f2bb",
        3: "#8ce99a",
        4: "#69db7c",
        5: "#51cf66",
        6: "#40c057",
        7: "#37b24d",
        8: "#2f9e44",
        9: "#2b8a3e",
    },
    "lime": {
        0: "#f4fce3",
        1: "#e9fac8",
        2: "#d8f5a2",
        3: "#c0eb75",
        4: "#a9e34b",
        5: "#94d82d",
        6: "#82c91e",
        7: "#74b816",
        8: "#66a80f",
        9: "#5c940d",
    },
    "yellow": {
        0: "#fff9db",
        1: "#fff3bf",
        2: "#ffec99",
        3: "#ffe066",
        4: "#ffd43b",
        5: "#fcc419",
        6: "#fab005",
        7: "#f59f00",
        8: "#f08c00",
        9: "#e67700",
    },
    "orange": {
        0: "#fff4e6",
        1: "#ffe8cc",
        2: "#ffd8a8",
        3: "#ffc078",
        4: "#ffa94d",
        5: "#ff922b",
        6: "#fd7e14",
        7: "#f76707",
        8: "#e8590c",
        9: "#d9480f",
    },
}



#%%
# Main function to handle the plotting logic
def main():
    output_format = "png"

    # File paths and settings for each dataset
    files = [
        {"path": "/run/media/darwinr/Seagate/Documents/halos_2018/h177/mahsa/snap/sigma_MgFe_v_FeH/UFD_0172.npz", "label": "Smooted M", "color": colors["red"][6]},
        {"path": "/run/media/darwinr/Seagate/Documents/halos_2018/h177/smag_1/snap/sigma_MgFe_v_FeH/UFD_0172.npz", "label": "Smag, C = 1", "color": colors["orange"][6]},
        {"path": "/run/media/darwinr/Seagate/Documents/halos_2018/h177/smag_0_45/snap/sigma_MgFe_v_FeH/UFD_0172.npz", "label": "Smag, C = 0.45", "color": colors["lime"][6]},
        {"path": "/run/media/darwinr/Seagate/Documents/halos_2018/h177/smag_0_1/snap/sigma_MgFe_v_FeH/UFD_0172.npz", "label": "Smag, C = 0.1", "color": colors["teal"][6]},
        {"path": "/run/media/darwinr/Seagate/Documents/halos_2018/h177/smag_0_08/snap/sigma_MgFe_v_FeH/UFD_0172.npz", "label": "Smag, C = 0.08", "color": colors["blue"][6]},
        {"path": "/run/media/darwinr/Seagate/Documents/halos_2018/h177/smag_0_045/snap/sigma_MgFe_v_FeH/UFD_0172.npz", "label": "Smag, C = 0.045", "color": colors["violet"][6]},
        {"path": "/run/media/darwinr/Seagate/Documents/halos_2018/h177/smag_0/snap/sigma_MgFe_v_FeH/UFD_0172.npz", "label": "Smag, C = 0.0", "color": colors["pink"][6]},
    ]
    shift = np.zeros(len(files))
    markersize = 4.0
    halo = "h177"

    # files = [
    #     # {"path": "/run/media/darwinr/Seagate/Documents/halos_2018/h187/smag_0_45/snap/sigma_MgFe_v_FeH/UFD_0172.npz", "label": "Smag, C = 0.45", "color": colors["orange"][6]},
    #     {"path": "/run/media/darwinr/Seagate/Documents/halos_2018/h187/smag_0_045/snap/sigma_MgFe_v_FeH/UFD_0172.npz", "label": "Smag, C = 0.045", "color": colors["lime"][6]},
    #     {"path": "/run/media/darwinr/Seagate/Documents/halos_2018/h187/smag_0_0045/snap/sigma_MgFe_v_FeH/UFD_0172.npz", "label": "Smag, C = 0.0045", "color": colors["blue"][6]},
    # ]
    # shift = np.zeros(len(files))
    # markersize = 4.0
    # halo="h187"

    # files = [
    #     # {"path": "/run/media/darwinr/Seagate/Documents/halos_2018/h187/smag_0_45/snap/sigma_MgFe_v_FeH/UFD_0172.npz", "label": "Smag, C = 0.45", "color": colors["orange"][6]},
    #     {"path": "/run/media/darwinr/Seagate/Documents/halos_2018/h190/smag_0_045/snap/sigma_MgFe_v_FeH/UFD_0172.npz", "label": "Smag, C = 0.045", "color": colors["lime"][6]},
    #     {"path": "/run/media/darwinr/Seagate/Documents/halos_2018/h190/smag_0_0045/snap/sigma_MgFe_v_FeH/UFD_0172.npz", "label": "Smag, C = 0.0045", "color": colors["blue"][6]},
    # ]
    # shift = np.zeros(len(files))
    # markersize = 4.0
    # halo="h190"

    # Axis limits (None to auto-determine)
    # x_min = -6 ; x_max = 1
    # y_min = -0.1 ;  y_max =0.75
    x_min = -6 ; x_max = 1
    y_min = None ;  y_max = None

    #Ca/Fe
    # x_min, x_max, y_min, y_max = None, None, -0.5, 1.75

    fig, ax = plt.subplots(num=2, ncols=1, nrows=1, layout="tight")

    # Loop over files and plot each dataset
    for i, file_info in enumerate(files):
        data = load_mgfe_data(file_info["path"])
        plot_mgfe_data(ax, data, shift[i], file_info["label"], file_info["color"], markersize=markersize)

    configure_axes(ax, x_min, x_max, y_min, y_max)

    #Plot observations--------------
    # For Sextans
    n_bins = 10
    sextan_file = "/home/darwinr/Desktop/Scripts/MaDarwinR/UFDs_data/sextans_data.npz"
    data_sextan = np.load(sextan_file, allow_pickle=True)

    MgFe_sextan = data_sextan["MgFe"]
    FeH_sextan = data_sextan["FeH"]
    # MgFe_error = data_sextan["MgFe_syst_error"]
    # Fe_errorH = data_sextan["FeH_syst_error"]

    # Remove NaN values from MgFe_sextan and FeH_sextan
    mask = ~np.isnan(MgFe_sextan) & ~np.isnan(FeH_sextan)
    MgFe_sextan_clean = MgFe_sextan[mask]
    FeH_sextan_clean = FeH_sextan[mask]

    # Calculate binned statistic after removing NaNs
    y_data_std, x_edges, _ = binned_statistic(FeH_sextan_clean, MgFe_sextan_clean, statistic='std', bins=n_bins)
    x_data_middle = (x_edges[:-1] + x_edges[1:]) / 2
    # print(y_data_std)
    ax.plot(x_data_middle, y_data_std, linestyle='-', marker="x", label="Sextans", color=colors["gray"][9])

    # Now for Sculptor
    sculptor_file = "/home/darwinr/Desktop/Scripts/MaDarwinR/UFDs_data/sculptor_data.npz"
    data_sculptor = np.load(sculptor_file, allow_pickle=True)

    MgFe_sculptor = data_sculptor["MgFe"]
    FeH_sculptor = data_sculptor["FeH"]
    # MgFe_error = data_sculptor["error_MgFe"]
    # Fe_errorH = data_sculptor["error_FeH"]

    # Remove NaN values from MgFe_sextan and FeH_sextan
    mask = ~np.isnan(MgFe_sculptor) & ~np.isnan(FeH_sculptor)
    MgFe_sculptor_clean = MgFe_sculptor[mask]
    FeH_sculptor_clean = FeH_sculptor[mask]

    # Calculate binned statistic after removing NaNs
    y_data_std, x_edges, _ = binned_statistic(FeH_sculptor_clean, MgFe_sculptor_clean, statistic='std', bins=n_bins)
    x_data_middle = (x_edges[:-1] + x_edges[1:]) / 2
    print(y_data_std)
    ax.plot(x_data_middle, y_data_std, linestyle='-', marker="+", label="Sculptor", color=colors["gray"][7])

    ax.legend()

    fig.savefig("comparison_sigma_MgFe_v_FeH_" + halo + "."+output_format, format=output_format, bbox_inches='tight', dpi=300)

if __name__ == "__main__":
    main()

