#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Nov 14 09:31:53 2024

@author: darwinr
"""
import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit

from pNbody import *
from pNbody import Nbody

from MaDarwinR.stars_utils import mdf
# from MaDarwinR.integrated_properties import mdf_function


def mdf_function(Fe_H, p):
    return 10**Fe_H * np.exp(-10**Fe_H / p)

# def mdf_function(Fe_H, p):
#     x = 10**Fe_H
#     return - p**2 * ( x/p + 1) * np.exp(-x/p)

def mdf_function(Fe_H, A, p):
    return A * 10**Fe_H * np.exp(-10**Fe_H / p)

#Get star data
filename = "/run/media/darwinr/Seagate/Documents/halos_2018/h177/mahsa/snap/UFD_0172.hdf5"
nb = Nbody(filename, ptypes=[4])

n_bins = 200

# Compute MDF
Fe_H = nb.AbRatio("Fe", "H")
n_star = len(Fe_H)
Fe_H_counts, Fe_H_stellar_fraction, Fe_H_edges = mdf(Fe_H, n_star, n_bins)
x_data = (Fe_H_edges[:-1] + Fe_H_edges[1:]) / 2  # Midpoints of bins
y_data = Fe_H_counts

# Plot
fig, ax = plt.subplots(num=0, ncols=1, nrows=1, layout="tight")
ax.step(x_data, y_data, linestyle="-", marker='None', color="tomato", alpha=1)
ax.set_xlim([-5, 0])

# Fit the MDF to the function with parameter p
A_initial = np.max(y_data)  # Use max value as guess for A
p_initial = 0.1 # Initial guess for p
popt, _ = curve_fit(mdf_function, x_data, y_data, p0=[A_initial, p_initial])
A_fit, p_fit = popt

# Calculate the mode of the MDF from p (this is found by maximalising the MDF function --> exp() must be =)
FeH_gal = np.log10(p_fit)

# Plot the fit
x_fit = np.linspace(-5, 0, 100)
y_fit = mdf_function(x_fit, A_fit, p_fit)
ax.plot(x_fit, y_fit, linestyle="-", marker='None', color="seagreen", alpha=1)

ax.axvline(FeH_gal, color="firebrick", linestyle="--", label=f"Mode: {FeH_gal:.2f}")

print(A_fit, p_fit)
print(FeH_gal)


