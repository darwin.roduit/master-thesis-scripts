#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Aug 25 13:41:40 2024

@author: darwinr
"""

class star:
    def __init__(self, mass, metallicity):
        self.birth_mass = mass
        self.mass = mass
        self.metallicity = metallicity
        self.R = 0
        self.T_eff = 0
        self.L = 0
