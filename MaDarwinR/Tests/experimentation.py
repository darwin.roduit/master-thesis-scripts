#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Mar  7 10:50:00 2024

@author: darwin
"""
import sys
import os
import copy

SCRIPT_DIR = os.path.dirname(os.path.abspath(__file__))
sys.path.append(os.path.dirname(SCRIPT_DIR))

from pNbody import *
from pNbody import units
from pNbody import libgrid
import Gtools as gt

import matplotlib.pyplot as plt
import numpy as np
from scipy.optimize import bisect
from scipy.stats import binned_statistic, binned_statistic_2d
from mpl_toolkits.axes_grid1 import make_axes_locatable
from tqdm import tqdm
from astropy import units as u
from astropy.constants import G as G_a
from astropy.constants import m_p

from analysis_functions import get_exploded_stars, compute_IMF
from cosmo import get_redshift
import snapshots_utils as snap_u

plt.style.use("../"  + 'lab_style_figure.mplstyle')


#%% Functions
def getr200(r, nb1, G, Hubble, Omega_0):
    """This function is used by a root finding algorithm to find r_200"""
    rho_crit =  3 * Hubble * Hubble / (8 * np.pi * G);
    nbs = nb1.selectc(nb1.rxyz()<r)
    r_2 = nbs.rxyz()
    r_2 = np.max(r_2)
    M = np.sum(nbs.mass)
    V = 4/3.*np.pi*r_2**3
    return (M/V - 200*rho_crit*Omega_0)

def get_r_200(nb_dm, G, Hubble):
    """Determine r_200 for a halo. The nb_dm object must be centered on the halo of interest."""
    r = nb_dm.rxyz()
    r_max = np.max(r)
    r_min =1.01 * np.min(r)
    Omega_0 = nb_dm.omega0
    r_200 = bisect(getr200, r_min, r_max, args = (nb_dm, G, Hubble, Omega_0), xtol = 1e-4, maxiter = 400)
    return r_200

def get_M_200(nb_dm, r_200):
    nb_dm_r200 = nb_dm.selectc(nb_dm.rxyz() < r_200)
    M_200 = nb_dm_r200.TotalMass()
    return M_200

def get_baryon_fraction(mass_matter, mass_DM):
    return mass_matter/(mass_DM + mass_matter)

def get_star_mass(nb_star, maximal_distance, star_BH_mass_limit):
    nb_star_frac = nb_star.selectc(nb_star.rxyz() < maximal_distance)
    nb_star_frac = nb_star.selectc(nb_star_frac.Mass() < star_BH_mass_limit)
    M_star = nb_star_frac.TotalMass()
    return M_star

def v_circ(nb, G, unit_length, unit_mass, unit_time, unit_velocity):
    pos = nb.pos*unit_length *nb.atime #physical position
    r = np.linalg.norm(pos, axis=1)
    M = np.cumsum(nb.mass*unit_mass);
    G = G.to(unit_length**3/(unit_mass * unit_time**2))
    v_circ = np.sqrt(G * M / r).to(unit_velocity)
    return v_circ

def r_half_bissection(r, nb):
    nb_minus = nb.selectc(nb.rxyz() < r)
    nb_plus = nb.selectc(nb.rxyz() >= r)
    # r_2 = np.max(nb.rxyz())
    M_minus = np.sum(nb_minus.mass)
    M_plus = np.sum(nb_plus.mass)
    return M_minus - M_plus

def get_softening_length(epsilon_comoving, max_epsilon_physical, scale_factor):
    return min(epsilon_comoving, max_epsilon_physical/scale_factor)

def density_NFW(r, rho_0, r_s):
    return rho_0/(r/r_s * (1 + r/r_s)**2)

def velocity_dispersion_cylindrical_grid(nb, **kwargs):
    # kwargs parameters----------------------------------------------------------------
    r_max = kwargs.get("r_max", None)
    n_los = kwargs.get("n_los", 3)
    # "number of particles per bin (sub plots)"
    n_part_per_bin_1 = kwargs.get("n_part_per_bin_1", int(1e3))
    # "number of particles per bin (total plot)"
    n_part_per_bin_2 = kwargs.get("n_part_per_bin_2", 30)
    # "mode used to define the bin radius in irregular grid (center, mean, uniform)"
    r_mode = kwargs.get("r_mode", "uniform")
    do_potcenter = kwargs.get("do_potcenter", True)
    # --------------------------------------------------------------------------

    r_max_saved = r_max

    nb2 = nb  # copy.deepcopy(nb)

    if do_potcenter:
        nb2.potcenter()  # Centers at the min of the potential

    # output units
    out_units_x = units.UnitSystem(
        'local', [units.Unit_kpc, units.Unit_Ms, units.Unit_Myr, units.Unit_K])
    out_units_y = units.UnitSystem(
        'local', [units.Unit_km, units.Unit_Ms, units.Unit_s, units.Unit_K])

    fx = nb2.localsystem_of_units.convertionFactorTo(
        out_units_x.UnitLength)
    fy = nb2.localsystem_of_units.convertionFactorTo(
        out_units_y.UnitVelocity)

    nb.pos = nb2.pos * fx
    nb.vel = nb2.vel * fy

    xs = np.zeros(0)
    ys = np.zeros(0)

    if n_los > 1:
        los = gt.getLOS(n_los, seed=0)
    else:
        los = np.array([0, 0, 1])

    for j in range(n_los):
        try:
            del nb2.Tree
        except AttributeError:
            # print("No nb.Tree attribute. Passing this step.")
            pass
        nbc = copy.deepcopy(nb2)  # copy the model

        # rotate
        if n_los > 1:
            nbc.align(axis=los[j])

        if do_potcenter:
            nbc.potcenter()

        r0 = nbc.rxy()  # compute the radius R
        v_z = nbc.Vz()

        # Keeps only the values at r0<r_max
        index = np.zeros(0)
        do_first_iteration = True
        r_max = r_max_saved

        # Iterates to take into account the fact that all the stars may move
        # out of r_max during the simulation
        while do_first_iteration or (len(index) == 0):
            do_first_iteration = False
            if r_max is not None:
                index = np.argwhere(r0 < r_max).flatten()
                r02 = r0[index]
                v_z2 = v_z[index]
            r_max *= 2.0
            # Stops increasing if r_max is bigger thant the size of the box
            if r_max >= nbc.boxsize[0]:
                r_max = nbc.boxsize[0]
                break

        # print("After while")
        r_max = r_max_saved

        # print("r_max = {}".format(r_max))
        # define the grid

        G = libgrid.CylindricalIrregular_1dr_Grid(r02, n_part_per_bin_1, rmode=r_mode)

        # compute the values
        rs = G.get_R()

        #Try to increase r_max if there is not enough points
        #Note: I should try to do the reverse : decrease r_max if it is indeed too big...
        #Alternative : Find the maximal size of the system and increase r_max based on that. Or choose
        # r_max based on that (e.g. take at least half of the points, if it's not enough, increase r_max to take 3/4 of the points, etc).
        while (rs.size < 3):
            if r_max <= 5:
                r_max *= 2.0
            else: #Avoid to big changes...
                r_max += 1

            G = libgrid.CylindricalIrregular_1dr_Grid(r02, n_part_per_bin_1, rmode=r_mode)
            rs = G.get_R()

            # Stops increasing if r_max is bigger thant half the size of the box
            if r_max >= 0.5*nbc.boxsize[0]:
                r_max = 0.5*nbc.boxsize[0]
                break

        Means, Sigmas = G.get_MeanAndStd(v_z2)

        xs = np.concatenate((xs, rs))
        ys = np.concatenate((ys, Sigmas))

    if n_los > 1:
        G = libgrid.CylindricalIrregular_1dr_Grid(xs, n_part_per_bin_2)
        x = G.get_R()
        mean, std = G.get_MeanAndStd(ys)
        y = mean
    return x, y, std


def velocity_dispersion(files, n_profile, ftype, n_DM_per_bin_1, n_DM_per_bin_2, n_stars_per_bin_1,
                        n_stars_per_bin_2, n_los, r_max_DM, r_max_stars, do_potcenter, no_DM_half_light_radius_computations):
    r_array_stars = np.zeros(n_profile+1, dtype=object)
    r_array_DM = np.zeros(n_profile+1, dtype=object)
    sigma_array_stars = np.zeros(n_profile+1, dtype=object)
    sigma_array_DM = np.zeros(n_profile+1, dtype=object)
    D_sigma_array_stars = np.zeros(n_profile+1, dtype=object)
    D_sigma_array_DM = np.zeros(n_profile+1, dtype=object)
    time_array_n_profile = np.zeros(n_profile+1)

    print("Computation of the velocity dispersion within r_max_DM = {:.2e} and "
          "r_max_stars = {:.2e}.".format(r_max_DM, r_max_stars))
    for i, file in enumerate(tqdm(files)):
        nb = Nbody(file, ftype=ftype)

        nb2 = nb.select("stars")
        r_array_stars[i], sigma_array_stars[i], D_sigma_array_stars[i] = velocity_dispersion_cylindrical_grid(nb2, n_los=n_los,
                                                                                                              n_part_per_bin_1=n_stars_per_bin_1, n_part_per_bin_2=n_stars_per_bin_2,
                                                                                                              part_type=4, r_max=r_max_stars, do_potcenter=do_potcenter) #Centers on the mass of the stars, useful if they decouple from DM
        if not no_DM_half_light_radius_computations:
            nb2 = nb.select("halo")
            r_array_DM[i], sigma_array_DM[i], D_sigma_array_DM[i] = velocity_dispersion_cylindrical_grid(nb2, n_los=n_los,
                                                                                                         n_part_per_bin_1=n_DM_per_bin_1, n_part_per_bin_2=n_DM_per_bin_2,
                                                                                                         part_type=1, r_max=r_max_DM, do_potcenter=do_potcenter)
        time_array_n_profile[i] = get_snapshot_time(file)
    print("End of the velocity dispersion !")
    return r_array_stars, r_array_DM, sigma_array_stars, sigma_array_DM, D_sigma_array_stars, D_sigma_array_DM, time_array_n_profile


#%%

snap_basename = "snapshot"
folder = "/home/darwinr/tmp/S5/snap"
filenames = snap_u.get_all_snapshot_in_directory(folder, snap_basename)
n_files = len(filenames)

file = "UFD_0172.hdf5"
filename = os.path.join(folder, file)

#Select the object
print("Loading the data...")
# nb = Nbody(p_name=filename)
# nb_star = Nbody(p_name=filename, ptypes=[4])

#Load DM only
nb_dm = Nbody(p_name=filename, ptypes=[1])

#Get the units from the object
UnitLength_in_cm = nb_dm.UnitLength_in_cm[0]*u.cm
UnitMass_in_g = nb_dm.UnitMass_in_g[0]*u.g
UnitVelocity_in_cm_per_s = nb_dm.UnitVelocity_in_cm_per_s*u.cm/u.s
Unit_time_in_cgs = nb_dm.Unit_time_in_cgs[0]*u.s

#Convert to kpc, M_sun km/s and Gyr
unit_length = UnitLength_in_cm.to(u.kpc)
unit_mass = UnitMass_in_g.to(u.M_sun)
unit_velocity = UnitVelocity_in_cm_per_s.to(u.km/u.s)
unit_time = Unit_time_in_cgs.to(u.Gyr)
unit_atom_per_cm_3 = m_p.to(u.g)/u.cm**3

scale_factor = nb_dm.atime

#Parameters
comoving_DM_softening =  0.2823*unit_length
max_physical_DM_softening = 0.07429*unit_length
# comoving_DM_softening =  0.1427*unit_length
# max_physical_DM_softening = 0.037*unit_length
maximal_radius = 50
hist_radius = 150
figsize = (10, 4)
bins_r = 50
r_200_frac = 1 #used to select the stars within r_200*r_200_frac
# star_BH_mass_limit = 40*u.M_sun #Limit between stars regime and FSNe/PISNe
star_BH_mass_limit = 400*u.M_sun #Limit between stars regime and FSNe/PISNe


#Center at the maximum density. It should correspond to the main UFD halo
#Do it twice to be better centered.
center_1 = nb_dm.get_histocenter(rbox=hist_radius, nb=500)
nb_dm.translate(-center_1)
center_2 = nb_dm.get_histocenter(rbox=hist_radius/10, nb=500)
nb_dm.translate(-center_2)
center = center_1 + center_2

#Cosmological parameters
Hubble = 0.1
G  = G_a.to(unit_length**3/(unit_mass * unit_time**2)).value
Omega_0 = nb_dm.omega0

r_200 = get_r_200(nb_dm, G, Hubble)*unit_length
M_200 = get_M_200(nb_dm, r_200.value)*unit_mass
print('r_200 = {:e} kpc (physical)'.format(r_200*scale_factor))
print('M_200 = {:e} M_sun'.format(M_200))

#%%  M_star, fraction of baryon within r_200
nb_star = Nbody(p_name=filename, ptypes=[4]) ; nb_star.translate(-center)
nb_sink = Nbody(p_name=filename, ptypes=[3]) ; nb_sink.translate(-center)
nb_gas = Nbody(p_name=filename, ptypes=[0]) ; nb_gas.translate(-center)

#Baryon fraction
nb_matter = Nbody(p_name=filename, ptypes=[0, 3, 4]) ; nb_matter.translate(-center)
mass_matter = nb_matter.TotalMass()
mass_DM = nb_dm.TotalMass()
f_bar = get_baryon_fraction(mass_matter, mass_DM)

print('f_baryon(r < r_200) = {:.3e}'.format(f_bar))

# Mass of the stars
M_star = get_star_mass(nb_star, r_200_frac*r_200.value, star_BH_mass_limit.to(unit_mass).value)*unit_mass
print('M_star (r < {:.2e}*r_200) = {:e}'.format(r_200_frac, M_star))

#%%r_1/2 for the star
nb_star_frac = nb_star.selectc(nb_star.Mass()*unit_mass< star_BH_mass_limit) #Remove stars that are BH
nb_star_frac = nb_star_frac.selectc(nb_star_frac.rxyz()*unit_length < r_200_frac*r_200) #Keep only the stars within the DM halo

r_star = nb_dm.rxyz()
r_max = np.max(r_star)
r_min =1.01 * np.min(r_star)

#Get half light radius
r_half_light = bisect(r_half_bissection, r_min, r_max, args = (nb_star_frac), xtol = 1e-4, maxiter = 400)*unit_length
print('r_1/2 = {:e}'.format(r_half_light*scale_factor))

#Get hal fmass radius
M_half_light = get_star_mass(nb_star_frac, r_half_light.value, star_BH_mass_limit.value)*unit_mass
print('M_1/2 = {:e}'.format(M_half_light))


#%% Then do rho(r), v_circ(r), v_max

from scipy.optimize import curve_fit

n_bins_r = 30
r_min = 5e-1
r_max = r_200.value


softening_length_dm = get_softening_length(comoving_DM_softening, max_physical_DM_softening, scale_factor)
softening_length_dm_ph = softening_length_dm.value*scale_factor

#Compute rho(r)
m = nb_dm.mass
pos = nb_dm.pos*unit_length
r = np.linalg.norm(pos, axis=1).value

if r_min is None:
    r_min = r.min()

if r_max is None:
    r_max = r.max()

# Bin the radii using np.digitize to get the bin indices
r_bins = np.logspace(np.log10(r_min), np.log10(r_max), n_bins_r + 1)
# r_bins = np.linspace(r.min(), r.max(), n_bins_r + 1)
bin_indices = np.digitize(r, bins=r_bins) - 1

# Calculate the centers of the bins for plotting
r_centers = 0.5 * (r_bins[:-1] + r_bins[1:])

# Loop through each bin index and accumulate masses and volumes
masses = np.zeros(n_bins_r)
volumes = np.zeros(n_bins_r)
for i in range(n_bins_r):
    in_bin = bin_indices == i
    masses[i] = np.sum(m[in_bin])  # Total mass in bin
    volumes[i] = (4/3) * np.pi * (r_bins[i+1]**3 - r_bins[i]**3)  # Volume of shell

# rho_r = (M*unit_mass/(V*unit_length**3)).to(unit_atom_per_cm_3)
rho_r = (masses*unit_mass/(volumes*unit_length**3)).to(unit_atom_per_cm_3)
rho_r_hist = rho_r/scale_factor**3
r_ph = r_centers*scale_factor

#Plot
fig, axes = plt.subplots(num=10, nrows=1, ncols=2, figsize=figsize, layout="tight")
ax = axes[0]
ax.clear()
ax.set_xscale('log')
ax.set_yscale('log')
ax.plot(r_ph, rho_r_hist, color='tomato', label='DM')


#Make an NFW fit
popt, pcov = curve_fit(density_NFW, r_ph, rho_r_hist)
# popt, pcov = curve_fit(density_NFW, np.log10(r_ph), rho_r_hist, bounds=(0, np.inf))
perr = np.sqrt(np.diag(pcov))

rho_0 = popt[0]
r_s = popt[1]
rho_NFW = density_NFW(r_ph, rho_0, r_s)

print('NFW fit : \n  rho_0 = {:e} +/- {:e}, r_s = {:e} +/- {:e}'.format(rho_0, perr[0], r_s, perr[1]))

# ax.plot(10**r_log*scale_factor, rho_r, linestyle='None', marker='.')
ax.plot(r_ph, rho_r_hist, color='tomato', label='DM')
ax.plot(r_ph, rho_NFW, label='NFW fit', color='purple', linestyle='--')
ax.axvspan(ax.get_xlim()[0], softening_length_dm_ph, color='grey', zorder=10, alpha=0.3)
ax.vlines(r_200.value*scale_factor, ax.get_ylim()[0], ax.get_ylim()[1], color='gray', zorder=10)
ax.text(r_200.value*scale_factor, 5e0, "$r_{200}$", fontsize=20)
ax.set_xlabel(r'$r$ [kpc]')
ax.set_ylabel(r'$\rho_{\mathrm{DM}}$ [atom/cm$^3$]')
ax.legend(loc='lower left')

#%%Compute v_circ(r)

#Plot
# fig, ax = plt.subplots(num=2, nrows=1, ncols=1, figsize=figsize, layout="tight")
# axes[1].cla()
# ax = axes[1]

# nb_array = [nb_gas, nb_dm, nb_star]
# label = ['Gas', 'DM', 'Star']
# color = ['royalblue', 'tomato', 'gold']

# for i, nb in enumerate(nb_array):
#     #Remove particles beyond some maximal radius
#     nb = nb.selectc(nb.rxyz() < maximal_radius)
#     pos = nb.pos*unit_length
#     r = np.linalg.norm(pos, axis=1)*scale_factor
#     r_log = np.log10(r.value)
#     v_c = v_circ(nb, G_a, unit_length, unit_mass, unit_time, unit_velocity)

#     #If this is the DM Nbody object
#     if nb.npart[1] != 0:
#         v_circ_max_DM = np.max(v_c)

#     #Do the histogram
#     v_circ_hist, r_edges, binnumber = binned_statistic(r_log, v_c, statistic='mean', bins=bins_r)
#     r_edges = 10**r_edges*unit_length
#     r_ph = r_edges[:-1]#*scale_factor
#     ax.plot(r_ph, v_circ_hist, color=color[i], label=label[i]) # plt.step(r_edges[:-1], v_circ_hist,  color="royalblue")


# ax.axvspan(ax.get_xlim()[0], softening_length_dm_ph, color='grey', zorder=10, alpha=0.3)
# ax.text(0.05, 0.9, "DM softened", transform=ax.transAxes, fontsize=12)
# ax.legend()
# ax.set_xscale('log')
# ax.set_yscale('log')
# ax.set_xlabel(r'$r$ [kpc]')
# ax.set_ylabel(r'v$_{\mathrm{circ}}$ [km/s]')

# #Peak/maximal value of the rotation curve for the stars
# print('v_circ_max = {:e}'.format(v_circ_max_DM))
# plt.savefig("h177l11_rho_DM_and_v_circ.pdf", format='pdf', bbox_inches='tight', dpi=300)


#%%Compute velocity dispersion
n_los = 5
n_part_sigma_1 = [100, 40]
n_part_sigma_2 = [100, 40]

axes[1].cla()
ax = axes[1]

nb_array = [nb_dm, nb_star]
label = ['DM', 'Star']
color = ['tomato', 'gold']

r_list = []
sigma_list = []

for i, nb in enumerate(nb_array):
    #Remove particles beyond some maximal radius
    nb = nb.selectc(nb.rxyz() < maximal_radius)
    r = np.zeros(n_part_sigma_2)
    sigma= np.zeros(n_part_sigma_2)
    D_sigma = np.zeros(n_part_sigma_2)
    r, sigma, D_sigma = velocity_dispersion_cylindrical_grid(nb, n_los=n_los,
                                                                                                         n_part_per_bin_1=n_part_sigma_1[i], n_part_per_bin_2=n_part_sigma_2[i],
                                                                                                         r_max=10, do_potcenter=False) #Centers on the mass of the stars, useful if they
    r_list.append(r)
    sigma_list.append(sigma)
    ax.plot(r, sigma, color=color[i], label=label[i]) # plt.step(r_edges[:-1], v_circ_hist,  color="royalblue")


ax.axvspan(ax.get_xlim()[0], softening_length_dm_ph, color='grey', zorder=10, alpha=0.3)
# ax.text(0.05, 0.9, "DM softened", transform=ax.transAxes, fontsize=12)
ax.legend()
ax.set_xscale('log')
# ax.set_yscale('log')
ax.set_xlabel(r'$r$ [kpc]')
ax.set_ylabel(r'$\sigma$ [km/s]')

plt.savefig("h177l11_rho_DM_and_vel_disp.pdf", format='pdf', bbox_inches='tight', dpi=300)


print("END-------------------------------------")
