#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Dec 22 09:19:53 2023

@author: darwin
"""

from pNbody import Nbody
import matplotlib.pyplot as plt
import numpy as np
from tqdm import tqdm
import argparse

import snapshots_utils as snap_u

plt.switch_backend('agg')

#%% Constantes
id_sink = 3
id_gas = 0


#%%



#%%

def get_accretion_mass(directory, output_location, snap_basename):
        #Adds the "/" at the end if needed
        if directory[-1] != "/":
                directory=directory+"/"
       
        if (output_location[-1] != "/"):
                output_location=output_location+"/"

        filenames = snap_u.get_all_snapshot_in_directory(directory, snap_basename)
        n_files = len(filenames)
        m_sink = np.zeros(n_files) #Need to treat the cas of sink formation
        t = np.zeros(n_files)

        for i, file in enumerate(tqdm(filenames)):
                nb = Nbody(file)
                nb = nb.select("sink")
                m_sink[i] =  nb.mass[0]
                t[i] = nb.atime
                
        #Save the data	
        np.savez_compressed(output_location+"accreted_mass_over_time", mass_sink=m_sink, time=t)

        fig, ax = plt.subplots(nrows=1, ncols=1, num=1)
        ax.plot(t, m_sink)
        plt.tight_layout()
        plt.savefig(output_location + "accreted_mass.pdf", format="pdf")
        plt.close()
        
        accretion_rate = np.diff(m_sink) / np.diff(t) 
        
        fig, ax = plt.subplots(nrows=1, ncols=1, num=1)
        ax.plot(t[:-1], accretion_rate)
        plt.tight_layout()
        plt.savefig(output_location + "accretion_rate.pdf", format="pdf")
        plt.close()
	

#%%
def parse_options():
        description = "Generate an homogeneous sphere model"
        epilog = """
Examples:
--------

"""
        parser = argparse.ArgumentParser(description=description, epilog=epilog)
        parser.add_argument('input_location', action="store", default=None, type=str, help="Directory containing snapshots to analyze.")
        parser.add_argument('--output_location', default="", help='Output folder to store the results.')
        parser.add_argument('--snap_basename', default="snapshot", type=str, help="Basename of the snapshots files, i.e the name before " "the numbers. E.g. snapshot, snap, etc.")            
        return parser.parse_args()
        
#%%
if __name__ == '__main__':
        args = parse_options()
        get_accretion_mass(args.input_location, args.output_location, args.snap_basename)


