#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Apr 29 14:33:38 2024

@author: darwinr
"""

import sys

def trace(frame, event, arg):
    print("%s, %s:%d" % (event, frame.f_code.co_filename, frame.f_lineno))
    return trace

import numpy as np
from pNbody import *
from pNbody import Nbody
import matplotlib.pyplot as plt
import time

from pNbody import mapping
from MaDarwinR import interpolate


#%%

def __kernel_cubic_spline_3D(q):
    """The kernel function needs to be mutpliplied by 1/h^3."""
    sigma_3 = 1/np.pi

    if 0 <= q and q < 1:
        return sigma_3*(1 - 1.5*q**2 + 3/4*q**3)
    elif 1<= q and q <= 2:
        return sigma_3*(1/4*(2-q)**3)
    else:
        return 0

kernel_cubic_spline_3D = np.vectorize(__kernel_cubic_spline_3D)

def __kernel_Wendland_C2(q):

    if 0 <= q and q <= 1:
        return (1-q)**4 * (4*1+1)
    else:
        return 0.0

kernel_Wendland_C2 = np.vectorize(__kernel_Wendland_C2)

def get_kernel_integrated(kernel_fct, kernel_radius = 2, n_points=1000):
    kernel_integrated = np.zeros(n_points+1) #last element = 0

    #loop over i
    q_xy = np.linspace(0, kernel_radius, n_points)
    delta_z = np.sqrt(kernel_radius**2 - q_xy**2)

    #Loop over j
    dz = delta_z/n_points
    lin = np.linspace(0, n_points-1, n_points)
    z = np.outer(lin, dz) #Voir dans quel sens ca doit etre
    q = np.sqrt(q_xy**2 + z**2)
    kernel = kernel_fct(q)

    kernel_integrated[:-1] = 2*np.trapz(kernel, q) #intgral is symmetric around 0

    q_xy_return = np.zeros(n_points+1) #last element just outside the kernel
    q_xy_return[:-1] = q_xy
    q_xy_return[-1] = q_xy_return[-2] + 1/n_points
    return q_xy_return, kernel_integrated


def __create_carthesian_grid_2d(x_min, x_max, y_min, y_max, n_pixel_x=200, n_pixel_y=200):
    x_lin = np.linspace(x_min, x_max, n_pixel_x)
    y_lin = np.linspace(y_min, y_max, n_pixel_y)

    #Create the carthesian grid
    x_grid, y_grid = np.meshgrid(x_lin, y_lin)

    #Grid of the quantity of interest that will be interpolated
    value_grid = np.zeros((n_pixel_x, n_pixel_y))
    normalization_grid = np.zeros((n_pixel_x, n_pixel_y))

    #Get the pixel size
    pixel_size_x = np.abs(x_max-x_min)/n_pixel_x
    pixel_size_y = np.abs(y_max-y_min)/n_pixel_y

    pixel_width_max = max(pixel_size_x, pixel_size_y)

    return x_grid, y_grid, value_grid, normalization_grid, pixel_size_x, pixel_size_y, pixel_width_max

def __interpolate_3d_projection(x, y, value, smoothing_length, mass, density, pixel_width_max, kernel_integrated,
                                                            x_grid, y_grid, value_grid, normalization_grid):
    """Compute the 3D projection along xy axis (rendered quantity is integrated along the line of sight)"""

    n_part = len(x)

    #This loop need to be performed in C and accelerated with openMP
    for part in range(n_part):
        #Set a minimal smoothing length
        h = max(smoothing_length[part], pixel_width_max/2)

        #Compute the 2D radius of ech pixel centered on this particle
        r = np.sqrt((x_grid - x[part])**2 + (y_grid - y[part])**2)

        #Find the pixel
        mask = (r <= h)
        r = r[mask]
        if len(r) != 0 :
            w_j = mass[part]/(density[part]*h**3)
            value_grid[mask] += w_j*h*value[part]*kernel_integrated(r/h)
            normalization_grid[mask] += w_j*h*kernel_integrated(r/h)

    value_grid = value_grid/normalization_grid

    #This is meaningless not to divide by the normalization. The value does not have the right scales.
    # eg. the Iron (Fe) fraction is as small as -1000...
    #The normalized version has the right scaling. Fe goes down to -20.
    #The same happenened with other quantities
    return value_grid

def get_kernel_from_name(kernel_choice):
    if kernel_choice == "Wendland C2":
        kernel = kernel_Wendland_C2
        kernel_radius = 1
    elif kernel == "Cubic spline":
        kernel = kernel_cubic_spline_3D
        kernel_radius = 1
    else:
        raise ValueError("The kernel '{}' is not implemented. Choose another kernel", kernel_choice)
    return kernel, kernel_radius

def interpolate_3D_projection(x, y, value, smoothing_length, mass, density, kernel_choice="Wendland C2",
                              x_min=None, x_max=None, y_min=None, y_max=None, n_pixel_x=200,
                              n_pixel_y=200, n_points_kernel_tabulated=1000):
    """Compute the 3D projection along xy axis (rendered quantity is integrated along the line of sight)"""

    #Get min and max values
    if x_min is None:
        x_min = np.min(x)
    if x_max is None:
        x_max = np.max(x)
    if y_min is None:
        y_min = np.min(y)
    if y_max is None:
        y_max = np.max(y)

    #Convert to float64
    x = x.astype(np.float64)
    y = y.astype(np.float64)
    value = value.astype(np.float64)
    smoothing_length = smoothing_length.astype(np.float64)
    mass = mass.astype(np.float64)
    density = density.astype(np.float64)


    #Now, choose the kernel
    kernel, kernel_radius = get_kernel_from_name(kernel_choice)

    #Do the kernel integration and interpolation
    q_xy, kernel_integrated_table = get_kernel_integrated(kernel, kernel_radius=kernel_radius, n_points=n_points_kernel_tabulated)
    kernel_integrated = lambda x : np.interp(x, q_xy, kernel_integrated_table)

    x_grid, y_grid, value_grid, normalization_grid, pixel_size_x, pixel_size_y, pixel_width_max = __create_carthesian_grid_2d(x_min, x_max, y_min, y_max, n_pixel_x, n_pixel_y)


    x_grid = x_grid.astype(np.float64)
    y_grid = y_grid.astype(np.float64)
    value_grid = value_grid.astype(np.float64)
    normalization_grid = normalization_grid.astype(np.float64)

    # interpolate.do_interpolate_3d_projection(x, y, value, smoothing_length, mass, density, pixel_width_max,
    #                                                                             x_grid, y_grid, value_grid, normalization_grid)
    # value_grid = __interpolate_3d_projection(x, y, value, smoothing_length, mass, density, pixel_width_max, kernel_integrated,
                                                                # x_grid, y_grid, value_grid, normalization_grid)

    #This C function modifies value_grid and normalization_grid in place
    interpolate.do_interpolate_3d_projection(x, y, value, smoothing_length, mass, density, pixel_width_max,
                                                                                x_grid, y_grid, value_grid, normalization_grid)

    #Here we just want to return the x and y of the grid, but in 1D (without repeating x and y along the other axis)
    x_lin = x_grid[0, :]
    y_lin = y_grid[:, 0]

    return value_grid, x_lin, y_lin


class SphPlot:
    """This is the base class for SPH plot. """
    #Voir dans yt...
    #Plan de l'interface a offrir
#%%

# folder ="/run/media/darwinr/Seagate/Documents/h177l11/snap/outputs/"
# file = "snapshot_0525.hdf5"

# folder ="/run/media/darwinr/Seagate/Documents/h177l11_2/snap/"
# file = "UFD_285.hdf5"
# folder ="/home/darwinr/Desktop/Specialisation/tests/test_feedback/snap/"
# folder ="/home/darwinr/Desktop/Specialisation/tests/test_feedback/base_test/snap/"
# file = "snapshot_0010.hdf5"
folder ="/run/media/darwinr/Seagate/Documents/h177l11_new_fb/snap_fb_inefficient/"
file = "UFD_359.hdf5"
# folder ="/home/darwinr/"
# file = "UVB_UFD_308_zoom.hdf5"
filename = folder + file
nb_gas = Nbody(p_name=filename, ptypes=[0])
nb_gas = nb_gas.selectc(nb_gas.rxyz() < 10)
nb_gas.histocenter()
print("Number of particles {}".format(nb_gas.npart))

# pos = nb_gas.Pos(units='kpc')
pos = nb_gas.pos
# x = pos[:, 0]
# y = pos[:, 1]
# z = pos[:, 2]

x = pos[:, 1]
y = pos[:, 2]

smoothing_length = nb_gas.rsp
mass = nb_gas.mass
density = nb_gas.rho
# value = nb_gas.T()
value = nb_gas.Rho(units='acc')
# value = nb_gas.Fe()

#%% Create the grid
n_pixel_x = 300 ; n_pixel_y = n_pixel_x

x_min = None ; x_max = None
y_min = None ; y_max = None

if x_min is None:
    x_min = np.min(x)
if x_max is None:
    x_max = np.max(x)
if y_min is None:
    y_min = np.min(y)
if y_max is None:
    y_max = np.max(y)

x_grid, y_grid, value_grid, normalization_grid, pixel_size_x, pixel_size_y, pixel_width_max = __create_carthesian_grid_2d(x_min, x_max, y_min, y_max, n_pixel_x, n_pixel_y)


x = x.astype(np.float64)
y = y.astype(np.float64)
value = value.astype(np.float64)
smoothing_length = smoothing_length.astype(np.float64)
mass = mass.astype(np.float64)
density = density.astype(np.float64)
x_grid = x_grid.astype(np.float64)
y_grid = y_grid.astype(np.float64)
value_grid = value_grid.astype(np.float64)
normalization_grid = normalization_grid.astype(np.float64)


start_time = time.time()
interpolate.do_interpolate_3d_projection(x, y, value, smoothing_length, mass, density, pixel_width_max,
                                                                            x_grid, y_grid, value_grid, normalization_grid)
end_time = time.time()
execution_time = end_time - start_time
print(f"Execution time: {execution_time} seconds")
# value_grid2, x_lin, y_lin = interpolate_3D_projection(x, y, value, smoothing_length, mass, density, kernel_choice="Wendland C2",
#                                 x_min=x_min, x_max=x_max, y_min=y_min, y_max=y_max, n_pixel_x=n_pixel_x,
#                                 n_pixel_y=n_pixel_y)


# delta = np.abs(value_grid2 - value_grid)
# delta_rel = delta / value_grid2
# %%
# x_min = x_lin[0] ; x_max = x_lin[-1]
# y_min = y_lin[0] ; y_max = y_lin[-1]

# cmap_min = 1e-1 ; cmap_max = 1e0
cmap_min = 1e-2 ; cmap_max = 0.5e1
# cmap_min = None ; cmap_max = None

if cmap_min is None:
    cmap_min = np.min(value_grid)
if cmap_max is None:
    cmap_max = np.max(value_grid)

# plt.figure(1)
# plt.clf()
# plt.imshow(value_grid, interpolation='none', origin='lower', vmin=cmap_min, vmax=cmap_max,
#                         extent=[x_min, x_max, y_min, y_max], aspect='equal', zorder=10)
# plt.colorbar()

#Log plot
cmap_min =np.log10(cmap_min) ; cmap_max = np.log10(cmap_max)
plt.figure(2)
plt.clf()
log_value_grid = np.log10(value_grid)
plt.imshow(log_value_grid, interpolation='none', origin='lower', vmin=cmap_min, vmax=cmap_max,
                        extent=[x_min, x_max, y_min, y_max], aspect='equal', zorder=10)
plt.colorbar()




# if cmap_min is None:
#     cmap_min = np.min(value_grid2)
# if cmap_max is None:
#     cmap_max = np.max(value_grid2)
# cmap_min =np.log10(cmap_min) ; cmap_max = np.log10(cmap_max)
# plt.figure(3)
# plt.clf()
# log_value_grid = np.log10(value_grid2)
# plt.imshow(log_value_grid, interpolation='none', origin='lower', vmin=cmap_min, vmax=cmap_max,
#                         extent=[x_min, x_max, y_min, y_max], aspect='equal', zorder=10)
# plt.colorbar()
