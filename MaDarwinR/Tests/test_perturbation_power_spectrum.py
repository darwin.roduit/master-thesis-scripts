import numpy as np
from matplotlib import pyplot as plt
from scipy.stats import binned_statistic

from pNbody.power_spectra import PowerLawPowerSpectrum
from pNbody.power_spectra.perturbation import PerturbationPowerSpectrum
# from pNbody.power_spectra.cosmology import Cosmology

plt.rc('text', usetex=True)
plt.rc('text.latex', preamble=r"\usepackage{amsmath}")

#%%  Parameters
boxsize = 1.01e1
N_mesh = 16 #We should have N_mesh > N_sample
N_sample = 10 #suppose we have N_sample**3 particles
epsilon = 1e-1

seed = -1 #select a random seed
seed = 128951219801374517513834785014410262710

#Powerspectrum choice
power_spectrum_type = "power_law"
exponent = 8

#Verification plot
do_verification_plots = True


# %%Code

power_spectrum = None

#Define the power spectrum
if power_spectrum_type == "power_law":
    power_spectrum = PowerLawPowerSpectrum(exponent)
else:
    raise ValueError("Not yet implemented")

PPS = PerturbationPowerSpectrum(power_spectrum, boxsize, N_sample, N_mesh, seed, epsilon, do_verification_plots, bin_number=20)
PPS.compute_perturbation_field()

N_part = N_sample**3
rng = np.random.default_rng(seed=PPS.seed)
pos = rng.random((3, N_part))*PPS.boxsize
pos_pert, vel_pert = PPS.apply_perturbation(N_part, pos)


#Plot part
# u = pos[0] / PPS.boxsize * PPS.N_mesh ; v = pos[1] / PPS.boxsize * PPS.N_mesh ; w = pos[2] / PPS.boxsize * PPS.N_mesh
# i = u.astype(np.int32) % PPS.N_mesh ; j = v.astype(np.int32) % PPS.N_mesh ; k = w.astype(np.int32) % PPS.N_mesh
# mesh_index = np.stack((i, j, k))

# k_vect = PPS.generate_k(mesh_index)
# k_norm = np.linalg.norm(k_vect, axis=0)


# displacement = pos_pert - pos
# ft_displacement = np.fft.fftn(displacement, axes=[0])
# ft_displacement_norm = np.linalg.norm(ft_displacement, axis=0)

# displacement_median, k_norm_edges, bin_number = binned_statistic(k_norm, ft_displacement_norm, statistic="median", bins=10)


# fig, ax = plt.subplots(num=1)
# # ax.plot(k_norm, ft_displacement_norm, marker=".", linestyle="None", label="")
# ax.bar(k_norm_edges[:-1], displacement_median, label="Perturbation from data", color="red")
# ax.set_xlabel(r"$\| k \|$")
# ax.set_ylabel(r"$P(\vec{k})  \cdot \frac{i \vec{k}}{k^2} \exp{(i\phi)}$")


# # ft_displacement2 = np.fft.fftn(pos_pert, axes=[0])
# # ft_displacement_norm2 = np.linalg.norm(ft_displacement2, axis=0)
# # ax.plot(k_norm, ft_displacement_norm2, marker=".", linestyle="None", label="Pos perturbed")
# # ax.set_ylim([-1, 30])


# # complex_perturbation_norm = np.linalg.norm(PPS.complex_perturbation, axis=0)
# # ax.plot(k_norm.reshape(PPS.N_mesh**3), complex_perturbation_norm.reshape(PPS.N_mesh**3), marker=".", linestyle="None", label="Original perturbation")

