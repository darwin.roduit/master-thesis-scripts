#!/usr/bin/env python3
"""
This file provides a comparison in the bias between different
 IMF sampling methods
"""

import numpy as np
import matplotlib.pyplot as plt
from scipy.interpolate import interp1d
from scipy.optimize import bisect
import time
import Ptools as pt
import matplotlib

from pNbody import pychem
import argparse


description = ""
epilog = """
Example:
-------

mist_example mist_directory

"""

parser = argparse.ArgumentParser(
    description=description, epilog=epilog, formatter_class=argparse.RawDescriptionHelpFormatter)

parser.add_argument("-o",
                    action="store",
                    dest="outputname",
                    metavar='FILENAME',
                    type=str,
                    default=None,
                    help='output file')

parser.add_argument("--do_not_plot_IMF",
                    action="store_true",
                    dest="do_not_plot_IMF",
                    help='do not plot the sampled IMF')

parser.add_argument("--do_not_split_IMF",
                    action="store_true",
                    dest="do_not_split_IMF",
                    help='do not split the sampled IMF')


np.random.seed(0)


opt = parser.parse_args()


############################################
# additional function

def power_law_imf_sampling(Mmin, Mmax, exp):
    """
    Sample a simple power law
    """
    f = np.random.random()
    pmin = pow(Mmin, exp)
    pmax = pow(Mmax, exp)
    m = pow(f*(pmax - pmin) + pmin, 1./exp)
    return m


# test the imf


'''
Mmin = 1.
Mmax = 30.
exp = exp_d

n = 100000
ms = np.zeros(n)

for i in range(n):
  ms[i] = power_law_imf_sampling(Mmin,Mmax,exp)

bins = 10**np.linspace(np.log10(1),np.log10(30),100)
h,b = np.histogram(ms,bins=bins)
pt.plot(b[:-1],h)

pt.loglog()
pt.show()
'''

############################################
# some parameters and constants


# note : here we need to change the name due to a conflixt with exec above.
imf_params = {}
imf_params["Mmax"] = 300.
imf_params["Mmin"] = 0.5
imf_params["as"] = [-1.3, -1.3, -1.3, -1.3]
imf_params["ms"] = [0.08, 0.5, 1.0]
pychem.set_parameters(imf_params)


toMsol = 1e11

Mct = 20      # "stellar particle" mass (continuous part) [solar mass]
mmax_c = 8   # maximal mass of the continuous part of the IMF [solar mass]

if mmax_c < 1:    # we should check that from the h5 file
    print("Must be larger than 1. Instead the slope changes")
    exit()


# get minimal and maximal mass
mmin = pychem.get_Mmin()
mmax = pychem.get_Mmax()

print()
print("IMF mmin            : ", mmin)
print("IMF mmax            : ", mmax)
print()

# get the IMF mass of the continous part
m1 = np.array([mmin])
m2 = np.array([mmax_c])
Mc = pychem.get_imf_M(m1, m2)
Nc = pychem.get_imf_N(m1, m2)


# get the IMF mass of the discrete part
m1 = np.array([mmax_c])
m2 = np.array([mmax])
Md = pychem.get_imf_M(m1, m2)
Nd = pychem.get_imf_N(m1, m2)

# compute the total IMF mass
M0 = Mct/Mc
Md = M0-Mct
Mc = Mct

# normalize
Nc = Nc*M0
Nd = Nd*M0


print()
print("Mass of the continuous part            : ", Mc)
print("Mass of the discrete   part            : ", Md)
print("Total mass of the IMF                  : ", M0)
print("Mass fraction of the continuous part   : ", Mc/M0)
print("Mass fraction of the discrete   part   : ", Md/M0)
print("Number of stars in the continuous part : ", Nc)
print("Number of stars in the discrete   part : ", Nd)

print()
# exit()


# get the IMF parameter for the discrete part
exps = pychem.get_as()
exp_d = exps[-1]

mmin_d = mmax_c
mmax_d = mmax

# compute the probabilities
#
# def f(Pc):
#  Mcs = 0
#  Mds = 0
#  for i in range(100000):
#    xc = np.random.random()
#    if xc < Pc:
#      Mcs = Mcs + Mc
#    else:
#      m = power_law_imf_sampling(mmin_d,mmax_d,exp_d)
#      Mds = Mds + m
#
#  #print(Pc,Mcs/Mds)
#  return Mcs/Mds - Mc/Md
#
# Pc = bisect(f, 0.0, 0.99,xtol=1e-4)
#

########################
# Analytical solution:

Pc = 1/(Nd+1)
Pd = 1-Pc

print()
print("probability of the continuous part : ", Pc)
print("probability of the discrete   part : ", Pd)
print()


class IMF:
    """
    Class representing the initial mass function
    """

    def __init__(self, mmin_d, mmax_d, exp_d):
        self.count = 0

        self.mmin_d = mmin_d
        self.mmax_d = mmax_d
        self.exp_d = exp_d

    def get_a_star(self):
        """
        Get a single star from the IMF
        """
        self.count = self.count+1
        return chemistry.imf_sampling(int(1), self.count)[0]*toMsol

    def get_a_low_mass_group_of_stars(self):
        """
        Get a single star from the IMF
        """
        self.count = self.count+1
        return chemistry.imf_sampling(int(1), self.count)[0]*toMsol

    def get_a_massive_star(self):
        """
        Get a single massive star from the IMF (from a simple power low)
        """
        return power_law_imf_sampling(self.mmin_d, self.mmax_d, self.exp_d)


imf = IMF(mmin_d, mmax_d, exp_d)


# Create the sink particles
class Sink:
    """
    Class representing the sink particles
    """

    def __init__(self, initial_mass, imf, Mc):
        """
        Initialize a single sink particle.
        """
        self.mass = initial_mass
        self.Mc = Mc
        self.imf = imf

        self.target_mass = self.update_target_mass()

    def accretion(self):
        """
        Update the mass of the sink particle
        due to accretion
        """

        old_mass = self.mass

        x = np.random.random()
        if x > 0.9:
            self.mass = self.mass + 50

            # print("accretion %5.1f ->  %5.1f"%(old_mass,self.mass))

    def run(self):
        """
        Generate a list of stars from the IMF
        """
        mass_stars_c = []
        mass_stars_d = []

        for i in range(1000000):

            # print(i,self.mass,self.target_mass)

            # we have enough material
            while self.mass > self.target_mass:

                # an individual star (discrete part of the IMF)
                if self.stellar_type == "d":
                    mass_stars_d.append(self.target_mass)

                # a stellar particle (continuous part of the IMF)
                else:
                    mass_stars_c.append(self.target_mass)

                self.mass = self.mass - self.target_mass
                self.target_mass = self.update_target_mass()

            # do accretion
            self.accretion()

        return mass_stars_c, mass_stars_d

    def update_target_mass(self):
        """
        Update the mass target of the sink particle.
        """

        xc = np.random.random()

        if xc < Pc:
            self.stellar_type = "c"
            return self.Mc
        else:
            self.stellar_type = "d"
            m = power_law_imf_sampling(
                self.imf.mmin_d, self.imf.mmax_d, self.imf.exp_d)
            return m


# Do the simulation for sink particles
print()
print("Starting...")
print()

t0 = time.time()


# we can choose the masses limits in order to have in average the same numbers of stars
sink = Sink(initial_mass=40, imf=imf, Mc=Mc)
stars_c, stars_d = sink.run()
t1 = time.time()


print()
print("ExecTime=%g" % (t1-t0))
print()


print("Mass of the continuous part            : ", Mc)
print("Mass of the discrete   part            : ", Md)
print("Number of stars in the continuous part : ", Nc)
print("Number of stars in the discrete   part : ", Nd)
print()


stars_c = np.array(stars_c)
stars_d = np.array(stars_d)


print("number of continuous part :", len(stars_c))
print("number of discrete   part :", len(stars_d))
print("mass max of discrete  part:", max(stars_d))
print(stars_c.mean(), stars_c.min(), stars_c.max())
print(stars_d.mean(), stars_d.min(), stars_d.max())
print()


# check the mass ratio
print("Ratio Mc/Md")
rth = Mc/Md
rme = sum(stars_c)/sum(stars_d)
err = 100*(np.fabs(rme-rth))/rth
print("expected  : ", rth)
print("effective : ", rme)
print("error  : ", err)

#########################
# do the IMF plot
#########################


pt.rcParams.update({"figure.figsize": (10, 8)})
pt.rcParams.update({"font.size": 28})

pt.rcParams.update({"figure.subplot.left":   0.180})
pt.rcParams.update({"figure.subplot.right":  0.950})
pt.rcParams.update({"figure.subplot.bottom": 0.145})
pt.rcParams.update({"figure.subplot.top":    0.950})
pt.rcParams.update({"figure.subplot.wspace": 0.2})


bins = 10**np.linspace(np.log10(2), np.log10(400), 1000)
h, b = np.histogram(stars_d, bins=bins)

if not opt.do_not_plot_IMF:
    pt.plot(b[:-1], h)

pt.loglog()


ax = pt.gca()
ax.set_xlim([0.3, 400])
ax.set_ylim([1, 5e4])

ax.set_xticks([1, 2, 4, 8, 20, 50, 100, 300])
ax.get_xaxis().set_major_formatter(matplotlib.ticker.ScalarFormatter())


pt.xlabel("Star mass\,$[M_\odot]$")
pt.ylabel("dN/dM")


# theoretical imf
s = -1.3
bins = 10**np.linspace(np.log10(mmin), np.log10(mmax), 100)
n = 0.9*10000*bins**s
pt.plot(bins, n, "k--")


# shade

if not opt.do_not_split_IMF:

    bins = 10**np.linspace(np.log10(mmin), np.log10(8), 100)
    n = 0.9*10000*bins**s
    ax.fill_between(bins, 0.1, n, color="red", alpha=0.1)

    bins = 10**np.linspace(np.log10(8), np.log10(mmax), 100)
    n = 0.9*10000*bins**s
    ax.fill_between(bins, 0.1, n, color="blue", alpha=0.1)

    pt.text(2, 1e2, r"$M_{\rm c}$", horizontalalignment='center')
    pt.text(50, 2, r"$M_{\rm d}$", horizontalalignment='center')

# error
txt = r"$\epsilon\left(M_{\rm c}/M_{\rm d} \right)= %4.2f$" % (err)
print(txt)

if not opt.do_not_plot_IMF:
    pt.text(9, 1e4, txt+"$\,\%$")


if opt.outputname is None:
    pt.show()
else:
    pt.savefig(opt.outputname)
