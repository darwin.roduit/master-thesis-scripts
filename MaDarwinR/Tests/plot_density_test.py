#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Dec 19 16:25:12 2023

@author: darwin
"""
from pNbody import *
from pNbody import Nbody
from pNbody import units, ctes, libgrid
import matplotlib.pyplot as plt
import numpy as np
# from scipy.interpolate import RBFInterpolator, griddata



#%%
# filename="test_h_sphere.hdf5"
# filename="test_h_sphere/snap/snapshot_0042.hdf5"
# folder="/home/darwin/Bureau/h_sphere/S01/snap/"
# folder="/mnt/lesta/hpcstorage/sink_tests/h_sphere_test/S01/snap/"
folder="/home/darwin/Bureau/Phd application/Michaela_EPFL/interview/figures/"
filename = folder + "BB_02_snapshot_0000.hdf5"
nb = Nbody(p_name=filename)

boxsize = nb.boxsize[0]

x = nb.pos[:, 0] - boxsize
y = nb.pos[:, 1] - boxsize
z = nb.pos[:, 2] - boxsize

Unit_atom = ctes.PROTONMASS.into(units.cgs) * units.Unit_g
Unit_atom.set_symbol('atom')
out_units = units.UnitSystem('local', [units.Unit_cm, Unit_atom, units.Unit_s, units.Unit_K])

rho = nb.Rho(units=out_units.UnitDensity)
pos = np.array([x, y]).transpose()

n_x = 100
n_y = n_x
n_z = n_x

x_min = np.min(x) ; x_max = np.max(x)
y_min = np.min(y) ; y_max = np.max(y)
z_min = np.min(z) ; z_max = np.max(z)

step_x = (x_max -x_min)/n_x
step_y = (y_max -y_min)/n_y

grid_z, grid_x, grid_y = np.histogram2d(x, y, weights=rho, bins=(n_x, n_y))

fig, ax = plt.subplots(num=1, ncols=1, nrows=1)
im = ax.pcolormesh(grid_x[:-1], grid_y[:-1], grid_z, edgecolors=None, shading="gouraud")
# im = ax.contourf(grid_x[:-1], grid_y[:-1], grid_z)
ax.set_xlabel("$x$ [AU]" )
ax.set_ylabel("$y$ [AU]" )
ax.axis("equal")
# cbar_ax = fig.add_axes([0.85, 0.15, 0.05, 0.7])
# fig.colorbar(im, cax=cbar_ax, label="Density [atm/cm^3]")
fig.colorbar(im, label="Density [atm/cm^3]")
# ax.set_ylim(y_min, y_max)


#%%
folder="/home/darwin/Bureau/Phd application/Michaela_EPFL/interview/figures/"
filename = folder + "BB_02_snapshot_1000.hdf5"
nb = Nbody(p_name=filename)

boxsize = nb.boxsize[0]

x = nb.pos[:, 0] - boxsize
y = nb.pos[:, 1] - boxsize
z = nb.pos[:, 2] - boxsize

Unit_atom = ctes.PROTONMASS.into(units.cgs) * units.Unit_g
Unit_atom.set_symbol('atom')
out_units = units.UnitSystem('local', [units.Unit_cm, Unit_atom, units.Unit_s, units.Unit_K])

rho = nb.Rho(units=out_units.UnitDensity)
pos = np.array([x, y]).transpose()

n_x = 500
n_y = n_x
n_z = n_x

x_min = np.min(x) ; x_max = np.max(x)
y_min = np.min(y) ; y_max = np.max(y)
z_min = np.min(z) ; z_max = np.max(z)

step_x = (x_max -x_min)/n_x
step_y = (y_max -y_min)/n_y

grid_z, grid_x, grid_y = np.histogram2d(x, y, weights=rho, bins=(n_x, n_y))

fig, ax = plt.subplots(num=2, ncols=1, nrows=1)
im = ax.pcolormesh(grid_x[:-1], grid_y[:-1], grid_z, edgecolors=None)
# im = ax.plot_surface(grid_x[:-1], grid_y[:-1], grid_z)
ax.set_xlabel("$x$ [AU]" )
ax.set_ylabel("$y$ [AU]" )
ax.axis("equal")
# cbar_ax = fig.add_axes([0.85, 0.15, 0.05, 0.7])
# fig.colorbar(im, cax=cbar_ax, label="Density [atm/cm^3]")
fig.colorbar(im, label="Density [atm/cm^3]")
# ax.set_ylim(y_min, y_max)

#%%
plt.close("all")
#%%
scale = 20
figsize=(15, 5)
folder=""
filename = folder + "cloud_collapse_100_M_sun_compressive.hdf5"
# filename = folder + "test_h_sphere.hdf5"
nb = Nbody(p_name=filename)
boxsize = nb.boxsize[0]

v_norm = np.linalg.norm(nb.vel, axis=1)

x = nb.pos[:, 0] - boxsize
y = nb.pos[:, 1] - boxsize
z = nb.pos[:, 2] - boxsize

v_x = nb.vel[:, 0]
v_y = nb.vel[:, 1]
v_z = nb.vel[:, 2]


num = 1000
fig, ax = plt.subplots(num=3, ncols=3, nrows=1, figsize=figsize)
ax[0].quiver(x[0:-1:num], y[0:-1:num], v_x[0:-1:num], v_y[0:-1:num], scale=scale)
ax[1].quiver(x[0:-1:num], z[0:-1:num], v_x[0:-1:num], v_z[0:-1:num], scale=scale)
ax[2].quiver(y[0:-1:num], z[0:-1:num], v_y[0:-1:num], v_z[0:-1:num], scale=scale)

# fig, ax = plt.subplots(num=4, ncols=1, nrows=1)
# ax.plot(v_x, v_y, marker=".", linestyle="None")

#%%

folder=""
filename = folder + "cloud_collapse_100_M_sun_solenoidal.hdf5"
nb = Nbody(p_name=filename)
boxsize = nb.boxsize[0]

v_norm = np.linalg.norm(nb.vel, axis=1)

x = nb.pos[:, 0] - boxsize
y = nb.pos[:, 1] - boxsize
z = nb.pos[:, 2] - boxsize

v_x = nb.vel[:, 0]
v_y = nb.vel[:, 1]
v_z = nb.vel[:, 2]


num = 1000
fig, ax = plt.subplots(num=4, ncols=3, nrows=1, figsize=figsize)
ax[0].quiver(x[0:-1:num], y[0:-1:num], v_x[0:-1:num], v_y[0:-1:num], scale=scale)
ax[1].quiver(x[0:-1:num], z[0:-1:num], v_x[0:-1:num], v_z[0:-1:num], scale=scale)
ax[2].quiver(y[0:-1:num], z[0:-1:num], v_y[0:-1:num], v_z[0:-1:num], scale=scale)

# fig, ax = plt.subplots(num=4, ncols=1, nrows=1)
# ax.plot(v_x, v_y, marker=".", linestyle="None")

#%%
folder=""
filename = folder + "cloud_collapse_100_M_sun_mixed.hdf5"
# filename = folder + "test_h_sphere.hdf5"
nb = Nbody(p_name=filename)
boxsize = nb.boxsize[0]

v_norm = np.linalg.norm(nb.vel, axis=1)

x = nb.pos[:, 0] - boxsize
y = nb.pos[:, 1] - boxsize
z = nb.pos[:, 2] - boxsize

v_x = nb.vel[:, 0]
v_y = nb.vel[:, 1]
v_z = nb.vel[:, 2]


num = 1000
fig, ax = plt.subplots(num=5, ncols=3, nrows=1, figsize=figsize)
ax[0].quiver(x[0:-1:num], y[0:-1:num], v_x[0:-1:num], v_y[0:-1:num], scale=scale)
ax[1].quiver(x[0:-1:num], z[0:-1:num], v_x[0:-1:num], v_z[0:-1:num], scale=scale)
ax[2].quiver(y[0:-1:num], z[0:-1:num], v_y[0:-1:num], v_z[0:-1:num], scale=scale)
# fig, ax = plt.subplots(num=4, ncols=1, nrows=1)
# ax.plot(v_x, v_y, marker=".", linestyle="None")

#%%
plt.close("all")