#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Mar 14 15:23:11 2024

@author: darwin
"""


import sys
import os

SCRIPT_DIR = os.path.dirname(os.path.abspath(__file__))
sys.path.append(os.path.dirname(SCRIPT_DIR))

from pNbody import *
from pNbody import Nbody
import matplotlib.pyplot as plt
import numpy as np

# from scipy.stats import binned_statistic, binned_statistic_2d
from mpl_toolkits.axes_grid1 import make_axes_locatable
from analysis_functions import get_redshift, get_exploded_stars, compute_IMF

#%%

folder = "/home/darwin/Bureau/Specialisation/UFDs/Revaz_Jablonka_DMO_2018/"
# folder_2 = "h177l11_jed/snap/"
folder_2 = "h177l11_adaptive_jed/snap/"
# folder_2 = "h177l11_adaptive_UVB_jed/snap/"
# file = "snap_0010_UFD.hdf5"
# file = "no_sinks/snapshot_0003.hdf5"
file = "snapshot_0020.hdf5"
# file = "snap_0020_UFD.hdf5"
# file = "snapshot_0020.hdf5"
filename = folder + folder_2 + file
minimal_discrete_mass = 8.0
m_star_part_continuous =  95.0
n_bins = 20

#Select the object
print("Loading the data...")
nb = Nbody(p_name=filename)

nb_gas = nb.select("gas")
rho = nb_gas.Rho(units="acc")
T = nb_gas.T()

log_rho_min = -4.
log_rho_max =  8.
log_T_min  = 0
log_T_max  = 7
log_mass_min = -4
bins_x = 100 ; bins_y = bins_x

log_rho = np.log10(rho)
log_T = np.log10(T)

#%%
#Plot phase space diagram colored by mass
mass_part = nb_gas.Mass(units="Msol")
log_mass = np.log10(mass_part)
mass, log_rho_edges, log_T_edges = np.histogram2d(log_rho, log_T, weights=mass_part, density=False, bins=(bins_x, bins_y))

figsize = (5, 10)
fig, ax = plt.subplots(num=1, ncols=1, nrows=2, figsize=figsize)
ax[0].cla()
im = ax[0].imshow(np.log10(mass).transpose(), interpolation='none', origin='lower',
               extent=[log_rho_edges[0], log_rho_edges[-1], log_T_edges[0], log_T_edges[-1]], \
               aspect='auto', zorder = 10, cmap = 'viridis') #or rainbow    
ax[0].set_ylabel(r"$\log_{10}(\mathrm{T})$ [K]")
ax[0].set_xlabel(r"$\log_{10}(\rho)$ [atom/cm$^3$]")


#Histogram 1D: projection over the rho axis
# hist_projection_rho = np.sum(mass, axis=0)
hist_projection_rho = np.log10(np.sum(mass, axis=0))

ax[1].plot(log_rho_edges[:-1], hist_projection_rho)
ax[1].set_xlabel(r"$\log_{10}(\rho)$ [atom/cm$^3$]")
ax[1].set_ylabel(r"M [M$_\odot$]")

#Add colorbar to ax[0]
divider = make_axes_locatable(ax[0])
cax = divider.append_axes('right', size='5%', pad=0.05)
fig.colorbar(im, cax=cax, orientation='vertical')

print(nb.redshift)
# exit("END")
#%%
ncols = 4 ; nrows = 3


figsize = (15, 10)
fig, axes = plt.subplots(num=2, ncols=ncols, nrows=nrows, figsize=figsize, layout="tight")

#%%
bins_x = 100 ; bins_y = bins_x

c_array = [nb_gas.XHI, nb_gas.XHII, nb_gas.XH2I, nb_gas.XH2II,
                  nb_gas.XHDI, nb_gas.XDI, nb_gas.XDII, nb_gas.XHM,
                  nb_gas.XHeI, nb_gas.XHeII, nb_gas.XHeIII, nb_gas.Xe]
# title = ["XHI", "XHII", "XH2I", "XH2II", "XHDI", "XDI", "XDII", "XHM", "XHeI", "XHeII", "XHeIII", "Xe"]
title = ["HI", "HII", "H$_2$I", "H$_2$II", "HDI", "DI", "DII", "H-", "HeI", "HeII", "HeIII", "e"]
cut_array = np.array([0.75, 1e-1, 1e-4, 1e-8, 1e-5, 1e-5, 1e-5, 1e-8, 0.24, 1e-5, 0.1,  1e-1])

# fig2, axes2 = plt.subplots(num=3, ncols=ncols, nrows=nrows, figsize=figsize, layout="tight")

for i, ax_array in enumerate(axes):
    for j, ax in enumerate(ax_array):
        ax.cla()

        #Get the right 1D index from a 2D indexing
        index = np.ravel_multi_index((i,j), dims=(3, 4))

        #2D Histogram
        c_cut = c_array[index]
        cut_indices = c_cut > cut_array[index]        
        weights = mass_part[cut_indices]*c_cut[cut_indices]
        hist, log_rho_edges, log_T_edges = np.histogram2d(log_rho[cut_indices], log_T[cut_indices], weights=weights, density=False, bins=(bins_x, bins_y))

        #Remove values under some minimum
        log_hist = np.log10(hist).transpose()

        #Plot all 2D histgrams
        im = ax.imshow(log_hist, interpolation='none', origin='lower',
                       extent=[log_rho_edges[0], log_rho_edges[-1], log_T_edges[0], log_T_edges[-1]], \
                       aspect='auto', zorder = 10, cmap = 'viridis') #or rainbow

        divider = make_axes_locatable(ax)
        cax = divider.append_axes('right', size='5%', pad=0.05)
        fig.colorbar(im, cax=cax, orientation='vertical')

        #Add the lines for T_max and rho_threshold of sink formation
        x_min = -7 ; x_max = 7 ; y_min = 0 ; y_max = 8
        
        ax.vlines(x=np.log10(1e3), ymin=y_min, ymax=y_max, color="royalblue")
        ax.hlines(y=np.log10(3e3), xmin=x_min, xmax=x_max, color="royalblue")

        #Set title
        title_final = title[index] + " (X{} ".format(title[index]) + "$ > {:.2e}$)".format(cut_array[index])
        ax.set_title(title_final, y=1.0, pad=-14)
        ax.set_xlim([-7, 7])
        ax.set_ylim([0, 8])

        #Add the axis labels to the left and bottom plots
        if j== 0:
            ax.set_ylabel(r"$\log_{10}(\mathrm{T})$ [K]")
            # axes2[i,j].set_ylabel(r"M [M$_\odot$]")
        # else:
            # ax.set_yticklabels([])
            # axes2[i,j].set_yticklabels([])
        if i ==2:
            ax.set_xlabel(r"$\log_{10}(\rho)$ [atom/cm$^3$]")
            # axes2[i,j].set_xlabel(r"$\log_{10}(\rho)$ [atom/cm$^3$]")
            # ax.set_xticks([-4, -2, 0, 2, 4])
        # else:
            # ax.set_xticklabels([])
            # axes2[i,j].set_xticklabels([])
            
        #1D projection on rho axis
        # hist_projection_rho = np.sum(hist, axis=0) #this is correct
        # # hist_projection_rho = np.log10(np.sum(hist, axis=0))
        # axes2[i,j].plot(log_rho_edges[:-1], hist_projection_rho)


#Adjust spacing between subplots
# fig.subplots_adjust(wspace=0.0)
fig.subplots_adjust(hspace=0.01)
# fig2.subplots_adjust(hspace=0.01)