#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Sep  3 15:34:09 2024

@author: darwinr
"""

import os
import numpy as np
import matplotlib.pyplot as plt
import matplotlib
import matplotlib.ticker as ticker
from pNbody import Nbody
from pNbody import *


plt.style.use('paper_style_figure.mplstyle')

imf_mass_max = 30
imf_transition_mass = 8
min_mass_plot = 5
min_mass_for_bins = 7.5
n_bins = 40

# filename = "/run/media/darwinr/Seagate/Documents//h177l11_UVB/snap/UFD_533.hdf5"
filename = "/run/media/darwinr/Seagate/Documents//h177l11_UVB/snap/snapshot_0533.hdf5"

# Select the object
print("Loading the data...")
nb_star = Nbody(p_name=filename, ptypes=[4])

# Do not continue if you do not find stars.
if nb_star.npart[4] == 0:
    print("No stars found in this file. Continue to the next file...")

# Take the initial star mass
mass_s = nb_star.InitialMass(units="Msol")

is_IMF_continuous_part = np.equal(nb_star.sp_type, 1)
m_discrete = mass_s[np.argwhere(np.logical_not(is_IMF_continuous_part))]
m_cont = mass_s[np.argwhere(is_IMF_continuous_part)]


# Number of stars
N_star = nb_star.npart[4]

# Compute the relative error
M_c_over_M_d_th = 9.057189331983617 #(from the sink_scheme.py script)
M_c_over_M_d_exp = np.sum(m_cont)/np.sum(m_discrete) #(from the sink_scheme.py script)

error = np.abs(M_c_over_M_d_th - M_c_over_M_d_exp) / \
    M_c_over_M_d_th * 100

# Create the histogram
bins = 10**np.linspace(np.log10(min_mass_for_bins), np.log10(imf_mass_max), n_bins)
mass_histogram, mass_bins = np.histogram(m_discrete, bins=bins)


figsize = (6.4, 4.8)
fig, ax = plt.subplots(num=1, ncols=1, nrows=1,
                       figsize=figsize, layout="tight")

#Experimental/numerical IMF
ax.plot(mass_bins[:-1], mass_histogram, color='red')

# Theoretical imf
a = mass_histogram.sum()
s = -1.3
bins = 10**np.linspace(np.log10(min_mass_plot), np.log10(imf_mass_max), 100)
n = 0.9*a*bins**s
ax.plot(bins, n, "k--")

bins = 10**np.linspace(np.log10(min_mass_plot), np.log10(imf_transition_mass), 100)
n = 0.9*a*bins**s
ax.fill_between(bins, 0.1, n, color="red", alpha=0.1)

bins = 10**np.linspace(np.log10(imf_transition_mass), np.log10(imf_mass_max), 100)
n = 0.9*a*bins**s
ax.fill_between(bins, 0.1, n, color="blue", alpha=0.1)

ax.text(0.2, 0.2, r"$M_{\rm c}$", horizontalalignment='center', transform=ax.transAxes, fontsize=20)
ax.text(0.5, 0.2, r"$M_{\rm d}$", horizontalalignment='center', transform=ax.transAxes, fontsize=20)

# Add limit
ax.vlines(x=imf_transition_mass, ymin=0, ymax=a/15, color='k', linestyle='-')

# Plot the error
ax.text(0.7, 0.7, r"$N_\text{star} =" + " {}$".format(N_star), transform=ax.transAxes,  horizontalalignment='center', fontsize=20)
ax.text(0.7, 0.8, r"$\epsilon(M_c/M_d) = {:.2f} \%$".format(error), transform=ax.transAxes,
        horizontalalignment='center', fontsize=20)

# Finlize plot
ax.set_xlim([min_mass_plot-1, 40])
ax.set_ylim([1, 500])
ax.set_xlabel("$M_{\star}$ $[M_\odot]$")
ax.set_ylabel("d$N/$d$M$")
ax.set_xscale('log')
ax.set_yscale('log')

# Set custom ticks on a logarithmic scale
ax.xaxis.set_major_locator(ticker.FixedLocator([7, 8, 10, 20, 30, 40]))
ax.xaxis.set_major_formatter(ticker.ScalarFormatter())

# Turn on minor ticks
ax.minorticks_on()

# Remove minor tick labels (if any exist) using FixedFormatter with empty labels
ax.xaxis.set_minor_formatter(ticker.NullFormatter())

# Set properties of minor ticks (e.g., no labels)
ax.tick_params(axis='x', which='minor', length=4, color='gray')
ax.tick_params(axis='x', which='major', length=6)


# Save the figure
plt.savefig('h177l11_UVB_imf_samping.png', dpi=300, bbox_inches='tight')

