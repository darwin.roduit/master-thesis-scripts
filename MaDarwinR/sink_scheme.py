#!/usr/bin/env python3
"""
This file provides a comparison in the bias between different
 IMF sampling methods
"""

import numpy as np
import matplotlib.pyplot as plt
import matplotlib
from scipy.interpolate import interp1d
from scipy.optimize import bisect
import time
import Ptools as pt

from pNbody import pychem

np.random.seed(0)


############################################
# additional function

def power_law_imf_sampling(Mmin, Mmax, exp):
  """
  Sample a simple power law
  """
  f = np.random.random()
  pmin = pow(Mmin, exp)
  pmax = pow(Mmax, exp)
  m = pow(f*(pmax - pmin) + pmin, 1./exp)
  return m


# test the imf


'''
Mmin = 1.
Mmax = 30.
exp = exp_d

n = 100000
ms = np.zeros(n)

for i in range(n):
  ms[i] = power_law_imf_sampling(Mmin,Mmax,exp)

bins = 10**np.linspace(np.log10(1),np.log10(30),100)
h,b = np.histogram(ms,bins=bins)
pt.plot(b[:-1],h)

pt.loglog()
pt.show()
'''

############################################
# some parameters and constants


# note : here we need to change the name due to a conflixt with exec above.
imf_params = {}
imf_params["Mmax"] = 30
imf_params["Mmin"] = 0.05
imf_params["as"] = [0.7, -0.8, -1.7, -1.3]
imf_params["ms"] = [0.08, 0.5, 1.0]
pychem.set_parameters(imf_params)


toMsol = 1e10

Mct = 10      # "stellar particle" mass (continuous part) [solar mass]
mmax_c = 8   # maximal mass of the continuous part of the IMF [solar mass]

if mmax_c < 1:    # we should check that from the h5 file
  print("Must be larger than 1. Instead the slope changes")
  exit()


# get minimal and maximal mass
mmin = pychem.get_Mmin()
mmax = pychem.get_Mmax()


# get the IMF mass of the continous part
m1 = np.array([mmin])
m2 = np.array([mmax_c])
Mc = pychem.get_imf_M(m1, m2)
Nc = pychem.get_imf_N(m1, m2)

# get the IMF mass of the discrete part
m1 = np.array([mmax_c])
m2 = np.array([mmax])
Md = pychem.get_imf_M(m1, m2)
Nd = pychem.get_imf_N(m1, m2)

# compute the total IMF mass
M0 = Mct/Mc
Md = M0-Mct
Mc = Mct

# normalize
Nc = Nc*M0
Nd = Nd*M0


print()
print("Mass of the continuous part            : ", Mc)
print("Mass of the discrete   part            : ", Md)
print("Total mass of the IMF                  : ", M0)
print("Mass fraction of the continuous part   : ", Mc/M0)
print("Mass fraction of the discrete   part   : ", Md/M0)
print("Number of stars in the continuous part : ", Nc)
print("Number of stars in the discrete   part : ", Nd)

print()


# get the IMF parameter for the discrete part
exps = pychem.get_as()
exp_d = exps[-1]

mmin_d = mmax_c
mmax_d = mmax

# compute the probabilities


def f(Pc):
  Mcs = 0
  Mds = 0
  for i in range(100000):
    xc = np.random.random()
    if xc < Pc:
      Mcs = Mcs + Mc
    else:
      m = power_law_imf_sampling(mmin_d, mmax_d, exp_d)
      Mds = Mds + m

  # print(Pc,Mcs/Mds)

  return Mcs/Mds - Mc/Md


# Pc = bisect(f, 0.0, 0.99,xtol=1e-4)
Pc = 1/(Nd+1)

# Pc = Nc/(Nc+Nd) / max(Nd,1)    *0.605   # does not works
Pd = 1-Pc

print()
print("probability of the continuous part : ", Pc)
print("probability of the discrete   part : ", Pd)
print()


########################
# Analytical solution:

Pc = 1/(Nd+1)


class IMF:
    """
    Class representing the initial mass function
    """

    def __init__(self, mmin_d, mmax_d, exp_d):
      self.count = 0

      self.mmin_d = mmin_d
      self.mmax_d = mmax_d
      self.exp_d = exp_d

    def get_a_star(self):
        """
        Get a single star from the IMF
        """
        self.count = self.count+1
        return chemistry.imf_sampling(int(1), self.count)[0]*toMsol

    def get_a_low_mass_group_of_stars(self):
        """
        Get a single star from the IMF
        """
        self.count = self.count+1
        return chemistry.imf_sampling(int(1), self.count)[0]*toMsol

    def get_a_massive_star(self):
        """
        Get a single massive star from the IMF (from a simple power low)
        """
        return power_law_imf_sampling(self.mmin_d, self.mmax_d, self.exp_d)


imf = IMF(mmin_d, mmax_d, exp_d)


# Create the sink particles
class Sink:
    """
    Class representing the sink particles
    """

    def __init__(self, initial_mass, imf, Mc):
        """
        Initialize a single sink particle.
        """
        self.mass = initial_mass
        self.Mc = Mc
        self.imf = imf

        self.target_mass = self.update_target_mass()

    def accretion(self):
        """
        Update the mass of the sink particle
        due to accretion
        """

        old_mass = self.mass

        x = np.random.random()
        if x > 0.9:
          self.mass = self.mass + 50

          # print("accretion %5.1f ->  %5.1f"%(old_mass,self.mass))

    def run(self):
        """
        Generate a list of stars from the IMF
        """
        mass_stars_c = []
        mass_stars_d = []

        for i in range(1000000):

          # print(i,self.mass,self.target_mass)

          # we have enough material
          while self.mass > self.target_mass:

            # an individual star (discrete part of the IMF)
            if self.stellar_type == "d":
              mass_stars_d.append(self.target_mass)

            # a stellar particle (continuous part of the IMF)
            else:
              mass_stars_c.append(self.target_mass)

            self.mass = self.mass - self.target_mass
            self.target_mass = self.update_target_mass()

          # do accretion
          self.accretion()

        return mass_stars_c, mass_stars_d

    def update_target_mass(self):
        """
        Update the mass target of the sink particle.
        """

        xc = np.random.random()

        if xc < Pc:
          self.stellar_type = "c"
          return self.Mc
        else:
          self.stellar_type = "d"
          m = power_law_imf_sampling(
              self.imf.mmin_d, self.imf.mmax_d, self.imf.exp_d)
          return m


# Do the simulation for sink particles
print()
print("Starting...")
print()

t0 = time.time()


# we can choose the masses limits in order to have in average the same numbers of stars
sink = Sink(initial_mass=40, imf=imf, Mc=Mc)
stars_c, stars_d = sink.run()
t1 = time.time()


print()
print("ExecTime=%g" % (t1-t0))
print()


print("Mass of the continuous part            : ", Mc)
print("Mass of the discrete   part            : ", Md)
print("Number of stars in the continuous part : ", Nc)
print("Number of stars in the discrete   part : ", Nd)
print()


stars_c = np.array(stars_c)
stars_d = np.array(stars_d)


print("number of continuous part:", len(stars_c))
print("number of discrete   part:", len(stars_d))
print(stars_c.mean(), stars_c.min(), stars_c.max())
print(stars_d.mean(), stars_d.min(), stars_d.max())
print()


# check the mass ratio
theoretical_Mc_over_M_d = Mc/Md
numerical_Mc_over_M_d = sum(stars_c)/sum(stars_d)
error = np.abs(theoretical_Mc_over_M_d - numerical_Mc_over_M_d) / \
    theoretical_Mc_over_M_d * 100
print("Ratio Mc/Md")
print("expected  : ", theoretical_Mc_over_M_d)
print("effective : ", numerical_Mc_over_M_d)
print("Error (%) : ", error)


# %%
mmax = imf_params["Mmax"]
mmin = imf_params["Mmin"]

m_min_plot = 0.5

matplotlib.rcParams.update({'font.size': 16})

figsize = (6.4, 4.8)
fig, ax = plt.subplots(num=1, ncols=1, nrows=1,
                       figsize=figsize, layout="tight")
ax.set_xlim([0.1, 400])
ax.set_ylim([1, 1e6])

ax.get_xaxis().set_major_formatter(matplotlib.ticker.ScalarFormatter())


ax.set_xlabel("$M_{\star}$ $[M_\odot]$")
ax.set_ylabel("d$N/$d$M$")
ax.set_xscale('log')
ax.set_yscale('log')

# theoretical imf
a = 16000
s = -1.3
bins = 10**np.linspace(np.log10(m_min_plot), np.log10(mmax), 100)
n = 0.9*a*bins**s
ax.plot(bins, n, "k--")

bins = 10**np.linspace(np.log10(m_min_plot), np.log10(mmax_c), 100)
n = 0.9*a*bins**s
ax.fill_between(bins, 0.1, n, color="red", alpha=0.1)

bins = 10**np.linspace(np.log10(mmax_c), np.log10(mmax), 100)
n = 0.9*a*bins**s
ax.fill_between(bins, 0.1, n, color="blue", alpha=0.1)

ax.text(2, 1e2, r"$M_{\rm c}$", horizontalalignment='center')
ax.text(50, 2, r"$M_{\rm d}$", horizontalalignment='center')

# Add limit
ax.vlines(x=mmax_c, ymin=0, ymax=1300, color='k', linestyle='-')

ax.text(0.7, 0.8, r"$\epsilon(M_c/M_d) = {:.2f} \%$".format(error), transform=ax.transAxes,
        horizontalalignment='center', fontsize=20)

# Experimental IMF ----------

bins = 10**np.linspace(np.log10(m_min_plot), np.log10(mmax), 300)
h, b = np.histogram(stars_d, bins=bins)
ax.plot(b[:-1], h, color='red')

ax.set_xticks([1, 2, 4, 8, 20, 50, 100, 300])


plt.savefig('sink_imf_samping.png', dpi=300, bbox_inches='tight')
