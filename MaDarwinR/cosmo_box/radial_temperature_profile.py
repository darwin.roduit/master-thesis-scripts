#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Aug 21 11:17:37 2024

@author: darwinr
"""
import os
import numpy as np
import matplotlib.pyplot as plt
from pNbody import *

#%%

def radial_temperature_profile(T, r, r_min=None, r_max=None, n_bins=30):
    # Handle default r_min and r_max
    if r_min is None:
        r_min = r.min()
    if r_max is None:
        r_max = r.max()

    # Create logarithmic bin edges
    r_bins = np.logspace(np.log10(r_min), np.log10(r_max), n_bins + 1)

    # Get bin indices for each radius
    bin_indices = np.digitize(r, bins=r_bins) - 1

    # Handle edge cases where radius might fall outside the range
    bin_indices = np.clip(bin_indices, 0, n_bins - 1)

    # Calculate the centers of the bins for plotting
    r_centers = 0.5 * (r_bins[:-1] + r_bins[1:])

    # Initialize temperature array
    temperatures = np.zeros(n_bins)

    # Loop through each bin and accumulate temperatures
    for i in range(n_bins):
        in_bin = (bin_indices == i)
        if np.any(in_bin):  # Ensure there are values in the bin
            temperatures[i] = np.mean(T[in_bin])  # Average temperature in bin

    return r_centers, temperatures

#%%
snap = "dense_region1_centered.hdf5"
nb = Nbody(snap, ptypes=[0])

temperature = nb.T()
r = nb.rxyz()
r_min = 1e-4
r_max = 1e-1
n_bins = 50


#%%
figsize = (6.4, 4.8)
fig, ax = plt.subplots(num=1, nrows=1, ncols=1, figsize=figsize, layout="tight")
ax.clear()

r_centers, T = radial_temperature_profile(temperature, r, r_min, r_max, n_bins=n_bins)
ax.plot(r_centers, T)
ax.set_xlabel("$r$ [kpc]")
ax.set_ylabel("$T$ [K]")
ax.set_xscale("log")
ax.set_ylim([0, 2000])
# ax.set_yscale("log")
