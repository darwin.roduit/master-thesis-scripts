#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jun  4 11:35:45 2024

@author: darwinr
"""
import swiftsimio as sw
import numpy as np
import matplotlib.pyplot as plt
import unyt
from swiftsimio.visualisation.projection import project_gas, project_pixel_grid
from swiftsimio.visualisation.smoothing_length_generation import generate_smoothing_lengths


#%%
filename = "/run/media/darwinr/Seagate/Documents/fourth_box/snap/snapshot_0017.hdf5"
filename = "/run/media/darwinr/Seagate/Documents/fourth_box/snap/snapshot_0028.hdf5"

image_resolution = 1024
do_dm_projection = False

do_mask_data = True
# xmin = 490*unyt.kpc ; xmax = 530*unyt.kpc
# ymin = 435*unyt.kpc ; ymax = 465*unyt.kpc
# zmin = 860*unyt.kpc ; zmax = 950*unyt.kpc

xmin = 0.4 ; xmax = 0.9
ymin = -0.2 ; ymax = 0.2
zmin = 0.4 ; zmax = 0.9

write_subset = True
# filename_out = "dense.hdf5"
filename_out = "subset_big.hdf5"

# To get densities in acc
# rho = data.gas.densities
# rho.convert_to_cgs()
# m_H = unyt.mh
# rho_acc = rho.to(m_H/unyt.cm**3)
# I_dense = np.argwhere(rho_acc.value > 1e3).flatten()
# data.gas.coordinates[I_dense]
# rho_acc[I_dense]

#%%
#Create a mask

if do_mask_data:
    mask = sw.mask(filename)
    boxsize = mask.metadata.boxsize

    # load_region = [[xmin, xmax],
    #                           [ymin, ymax],
    #                           [zmin, zmax]]

    load_region = [[xmin*boxsize[0], xmax*boxsize[0]],
                              [ymin*boxsize[0], ymax*boxsize[0]],
                              [zmin*boxsize[0], zmax*boxsize[0]]]


    # Constrain the mask
    mask.constrain_spatial(load_region)

    #Maks the data
    data = sw.load(filename, mask=mask)
else:
    #Load data
    data = sw.load(filename)
    boxsize = data.metadata.boxsize

#Compute projected density
projected_density = project_gas(
    data,
    resolution=image_resolution,
    project="masses",
    parallel=True,
    periodic=True,
).T


projected_density.convert_to_cgs()

# projected_density = projected_density/unyt.mh.to(unyt.g)
# projected_density_log = projected_density

projected_density_log = np.log10(projected_density.value)


#%% Dark matter

if do_dm_projection:
    print("Computing smoothing length for DM")
    # Generate smoothing lengths for the dark matter
    data.dark_matter.smoothing_length = generate_smoothing_lengths(
        data.dark_matter.coordinates,
        data.metadata.boxsize,
        kernel_gamma=1.9,
        neighbours=57,
        speedup_fac=2,
        dimension=3,
    )

    # Project the dark matter mass
    print("Compute projection for DM")
    projected_density_dm = project_pixel_grid(
        # Note here that we pass in the dark matter dataset not the whole
        # data object, to specify what particle type we wish to visualise
        data=data.dark_matter,
        boxsize=data.metadata.boxsize,
        resolution=image_resolution,
        project="masses",
        parallel=True,
        region=None,
        periodic=True,
    )

    projected_density_dm = projected_density_dm*unyt.Msun/unyt.kpc**2
    projected_density_dm = projected_density_dm.T
    projected_density_dm.convert_to_cgs()
    projected_density_dm_log = np.log10(projected_density_dm.value)



#%% Projection plot for gas
figsize = (10,10)
# fig, axes = plt.subplots(ncols=4, nrows=1, figsize=figsize, layout="tight", num=1)
fig, ax = plt.subplots(ncols=1, nrows=1, figsize=figsize, num=1)




# vmin = -7
vmin = None
vmax = None

im = ax.imshow(projected_density_log, origin='lower',  zorder = 1, #interpolation='None',
                    extent=[0, boxsize[0].value, 0, boxsize[1].value], aspect='equal',
                    cmap = 'inferno',
                    vmin=vmin, vmax=vmax)

ax.scatter(data.stars.coordinates[:,0].value, data.stars.coordinates[:,1], c='limegreen', zorder=1, marker='.')

ax.scatter(data.sinks.coordinates[:,0].value, data.sinks.coordinates[:,1], c='blue', zorder=1, marker='.')

# ax.set_xlim([xmin, xmax]*boxsize[0])
# ax.set_ylim([ymin, ymax]*boxsize[1])

ax.set_xlabel("$x$ [ckpc]")
ax.set_ylabel("$y$ [ckpc]")

ax.text(0.05, 0.98, "$z = {:.2f}".format(data.metadata.redshift) +"$", transform=ax.transAxes, fontsize=12, bbox=dict(facecolor='white', alpha=0.8))
ax.text(0.4, 0.98, "$N_{\mathrm{star}}" + " = {}$".format(data.metadata.n_stars), transform=ax.transAxes, fontsize=12, bbox=dict(facecolor='white', alpha=0.8))

# Add a colorbar for all subplots
cbar_ax = fig.add_axes([0.125, 0.9, 0.775, 0.05])  # Adjust these values to position the colorbar
cbar = fig.colorbar(im, cax=cbar_ax, orientation='horizontal')
cbar.set_label('$\log_{10}(\Sigma$ [g/cm$^2$]$)$', labelpad=1)
cbar.ax.xaxis.set_label_position('top')
cbar.ax.xaxis.set_ticks_position('top')

plt.subplots_adjust(top=0.9)  # Adjust the top to make space for the colorbar
plt.savefig("box_gas.png", format='png', bbox_inches='tight', dpi=300)


#%%

if write_subset == True:
    sw.subset_writer.write_subset(filename_out, mask)



#%%

if do_dm_projection:
    figsize = (10,10)
    # fig, axes = plt.subplots(ncols=4, nrows=1, figsize=figsize, layout="tight", num=1)
    fig, ax = plt.subplots(ncols=1, nrows=1, figsize=figsize, num=2)




    # vmin = -7
    vmin = None
    vmax = None

    im = ax.imshow(projected_density_dm_log, origin='lower',  zorder = 1, #interpolation='None',
                        extent=[0, boxsize[0].value, 0, boxsize[1].value], aspect='equal',
                        cmap = 'inferno',
                        vmin=vmin, vmax=vmax)

    ax.scatter(data.stars.coordinates[:,0].value, data.stars.coordinates[:,1], c='limegreen', zorder=1, marker='.')

    ax.scatter(data.sinks.coordinates[:,0].value, data.sinks.coordinates[:,1], c='blue', zorder=1, marker='.')

    # ax.set_xlim([xmin, xmax]*boxsize[0])
    # ax.set_ylim([ymin, ymax]*boxsize[1])

    ax.set_xlabel("$x$ [ckpc]")
    ax.set_ylabel("$y$ [ckpc]")

    ax.text(0.05, 0.98, "$z = {:.2f}".format(data.metadata.redshift) +"$", transform=ax.transAxes, fontsize=12, bbox=dict(facecolor='white', alpha=0.8))
    ax.text(0.4, 0.98, "$N_{\mathrm{star}}" + " = {}$".format(data.metadata.n_stars), transform=ax.transAxes, fontsize=12, bbox=dict(facecolor='white', alpha=0.8))

    # Add a colorbar for all subplots
    cbar_ax = fig.add_axes([0.125, 0.9, 0.775, 0.05])  # Adjust these values to position the colorbar
    cbar = fig.colorbar(im, cax=cbar_ax, orientation='horizontal')
    cbar.set_label('$\log_{10}(\Sigma$ [g/cm$^2$]$)$', labelpad=1)
    cbar.ax.xaxis.set_label_position('top')
    cbar.ax.xaxis.set_ticks_position('top')

    plt.subplots_adjust(top=0.9)  # Adjust the top to make space for the colorbar
    plt.savefig("box_dm.png", format='png', bbox_inches='tight', dpi=300)

