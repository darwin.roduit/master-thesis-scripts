#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Oct 27 13:25:31 2024

@author: darwinr
"""

from scipy.stats import binned_statistic
import numpy as np
from pNbody import *

from .cosmo import get_redshift


def mdf(Fe_H, n_star, n_bins):
    """Compute the metallicity distribution function"""
    Fe_H_counts, Fe_H_edges, _ = binned_statistic(
        Fe_H, Fe_H, statistic='count', bins=n_bins)
    Fe_H_stellar_fraction = Fe_H_counts/n_star
    return Fe_H_counts, Fe_H_stellar_fraction, Fe_H_edges


def compute_SFR(nb_star, n_bins):
    t_formation = nb_star.StellarFormationTime(units="Gyr")
    z_formation = get_redshift(nb_star.StellarFormationTime())

    m_formation = nb_star.InitialMass(units="Msol")
    M_tot_star, time_edges, bin_num = binned_statistic(
        t_formation, m_formation, statistic="sum", bins=n_bins)
    M_tot_star_z, z_edges, bin_num = binned_statistic(
        z_formation, m_formation, statistic="sum", bins=n_bins)

    delta_t = np.diff(time_edges)*1e9  # in year
    SFR = M_tot_star/delta_t
    t_mean = np.diff(np.cumsum(time_edges))

    # Reverse the order of the z but not he SFR
    z_mean = np.diff(np.cumsum(z_edges))
    z_mean = z_mean[::-1]
    SFR_z = SFR
    return t_mean, SFR, z_mean, SFR_z


def compute_integrated_mass(nb_star, n_bins):
    t_formation = nb_star.StellarFormationTime(units="Gyr")
    m_formation = nb_star.InitialMass(units="Msol")
    M_tot_star, time_edges, bin_num = binned_statistic(
        t_formation, m_formation, statistic="sum", bins=n_bins)
    t_mean = np.diff(np.cumsum(time_edges))
    integrated_mass = np.cumsum(M_tot_star)
    return t_mean, integrated_mass
