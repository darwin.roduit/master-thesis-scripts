#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jun 11 10:54:21 2024

@author: darwinr
"""

def smoothing_length(X_H, m_H, n_H, m_B, eta_res=1.2348):
    """
    Compute the smoothing length of particles as does SWIFT.

    Parameters
    ----------
    X_H : float
        Hydrogen mass fraction.
    m_H : float
        Hydrogen particle mass.
    n_H : float
        Hydrogen number density.
    m_B : float
        Baryon particle mass.
    eta_res : float, optional
        Constant related to the number of neighbours in the SPH kernel. The default is 1.2348.

    Returns
    -------
    h : float
        Smoothing length.

    """
    h = eta_res*((X_H*m_B)/(m_H*n_H))**(1.0/3.0)
    return h

def hydrogen_number_density_from_smoohting_length(X_H, m_H, m_B, h, eta_res=1.2348):
    """
    Compute the hydrogen number density from the smoothing length.

    This function uses the same formula than smoothing_length().

    Parameters
    ----------
    X_H : float
        Hydrogen mass fraction.
    m_H : float
        Hydrogen particle mass.
    m_B : float
        Baryon particle mass.
    h : float
        Smoothing length.
    eta_res : float, optional
        Constant related to the number of neighbours in the SPH kernel. The default is 1.2348.

    Returns
    -------
    n_H : float
        Hydrogen number density.

    """
    n_H = X_H/m_H * eta_res**3 * (m_B/h**3)
    return n_H
