#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Oct 27 15:10:59 2024

@author: darwinr
"""

import os
import requests
import zipfile
import tarfile
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

# %%
def read_table10(file_path):
    """
    Reads the `table10.dat` file, which contains star IDs and abundance data,
    including [Fe/H], [Mg/Fe], [Ca/Fe], and [Sc/Fe] with their respective errors.

    Parameters:
    - file_path (str): Path to the `table10.dat` file.

    Returns:
    - pd.DataFrame: DataFrame containing the abundances and error columns.
    """
    # Define column names and their fixed widths based on byte description
    colspecs = [
        (0, 7),    # ID
        (8, 9),    # n_ID
        (9, 14),   # [Fe/H]
        (15, 19),  # e_[Fe/H]
        (20, 24),  # er_[Fe/H]
        (25, 26),  # ---
        (26, 28),  # o_[Fe/H]
        (30, 35),  # [Mg/Fe]
        (36, 40),  # e_[Mg/Fe]
        (41, 45),  # er_[Mg/Fe]
        (46, 51),  # [Ca/Fe]
        (52, 56),  # e_[Ca/Fe]
        (57, 61),  # er_[Ca/Fe]
        (62, 63),  # ---
        (63, 65),  # o_[Ca/Fe]
        (67, 72),  # [Sc/Fe]
        (73, 77),  # e_[Sc/Fe]
        (78, 82),  # er_[Sc/Fe]
        (83, 84),  # ---
        (84, 85),  # o_[Sc/Fe]
    ]

    column_names = [
        "ID", "n_ID", "[Fe/H]", "e_[Fe/H]", "er_[Fe/H]", "dummy1", "o_[Fe/H]",
        "[Mg/Fe]", "e_[Mg/Fe]", "er_[Mg/Fe]", "[Ca/Fe]", "e_[Ca/Fe]", "er_[Ca/Fe]",
        "dummy2", "o_[Ca/Fe]", "[Sc/Fe]", "e_[Sc/Fe]", "er_[Sc/Fe]", "dummy3", "o_[Sc/Fe]"
    ]

    # Read the fixed-width file and drop dummy columns
    df = pd.read_fwf(file_path, colspecs=colspecs, names=column_names,
                     na_values="?").drop(columns=["dummy1", "dummy2", "dummy3"])

    return df


def read_table11(file_path):
    colspecs = [
        (0, 7),    # ID
        (7, 8),    # n_ID
        (9, 14),   # [TiI/Fe]
        (15, 19),  # e_[TiI/Fe]
        (20, 24),  # er_[TiI/Fe]
        (26, 27),  # Open bracket '('
        (27, 28),  # o_[TiI/Fe]
        (28, 29),  # Close bracket ')'
        (30, 35),  # [TiII/Fe]
        (36, 40),  # e_[TiII/Fe]
        (41, 45),  # er_[TiII/Fe]
        (47, 48),  # Open bracket '('
        (48, 49),  # o_[TiII/Fe]
        (49, 50),  # Close bracket ')'
        (51, 56),  # [Cr/Fe]
        (57, 61),  # e_[Cr/Fe]
        (62, 66),  # er_[Cr/Fe]
        (68, 69),  # Open bracket '('
        (69, 70),  # o_[Cr/Fe]
        (70, 71),  # Close bracket ')'
        (72, 77),  # [Mn/Fe]
        (78, 82),  # e_[Mn/Fe]
        (83, 87),  # er_[Mn/Fe]
        (89, 90),  # Open bracket '('
        (90, 91),  # o_[Mn/Fe]
        (91, 92)   # Close bracket ')'
    ]

    column_names = [
        "ID", "n_ID", "[TiI/Fe]", "e_[TiI/Fe]", "er_[TiI/Fe]", "open_bracket_TiI",
        "o_[TiI/Fe]", "close_bracket_TiI", "[TiII/Fe]", "e_[TiII/Fe]", "er_[TiII/Fe]",
        "open_bracket_TiII", "o_[TiII/Fe]", "close_bracket_TiII", "[Cr/Fe]",
        "e_[Cr/Fe]", "er_[Cr/Fe]", "open_bracket_Cr", "o_[Cr/Fe]", "close_bracket_Cr",
        "[Mn/Fe]", "e_[Mn/Fe]", "er_[Mn/Fe]", "open_bracket_Mn", "o_[Mn/Fe]",
        "close_bracket_Mn"
    ]

    # Read the data
    data = pd.read_fwf(file_path, colspecs=colspecs,
                       names=column_names, na_values="?")

    # Drop extra columns (open/close brackets for readability)
    data.drop(columns=["open_bracket_TiI", "close_bracket_TiI",
                       "open_bracket_TiII", "close_bracket_TiII",
                       "open_bracket_Cr", "close_bracket_Cr",
                       "open_bracket_Mn", "close_bracket_Mn"], inplace=True)

    return data


def read_table12(file_path):
    colspecs = [
        (0, 7),    # ID
        (7, 8),    # n_ID
        (10, 15),  # [Co/Fe]
        (16, 20),  # e_[Co/Fe]
        (21, 25),  # er_[Co/Fe]
        (27, 31),  # [NI/Fe]
        (32, 36),  # e_[NI/Fe]
        (37, 41),  # er_[NI/Fe]
        (43, 44),  # Open bracket '('
        (44, 45),  # o_[NI/Fe]
        (45, 46),  # Close bracket ')'
        (46, 51),  # [Ba/Fe]
        (52, 56),  # e_[Ba/Fe]
        (57, 61),  # er_[Ba/Fe]
        (63, 64),  # Open bracket '('
        (64, 65),  # o_[Ba/Fe]
        (65, 66),  # Close bracket ')'
        (67, 71),  # [Eu/Fe]
        (72, 76),  # e_[Eu/Fe]
        (77, 81),  # er_[Eu/Fe]
    ]

    column_names = [
        "ID", "n_ID", "[Co/Fe]", "e_[Co/Fe]", "er_[Co/Fe]", "[NI/Fe]",
        "e_[NI/Fe]", "er_[NI/Fe]", "open_bracket_NI", "o_[NI/Fe]",
        "close_bracket_NI", "[Ba/Fe]", "e_[Ba/Fe]", "er_[Ba/Fe]",
        "open_bracket_Ba", "o_[Ba/Fe]", "close_bracket_Ba",
        "[Eu/Fe]", "e_[Eu/Fe]", "er_[Eu/Fe]"
    ]

    # Read the data
    data = pd.read_fwf(file_path, colspecs=colspecs,
                       names=column_names, na_values="?")

    # Drop extra columns (open/close brackets for readability)
    data.drop(columns=["open_bracket_NI", "close_bracket_NI",
                       "open_bracket_Ba", "close_bracket_Ba"], inplace=True)

    return data


def read_abundance(directory_path, abundance_label):
    """
    Reads the specified abundance data from the appropriate table file in a given directory.

    Parameters:
    - directory_path: str, path to the directory containing the table files (e.g., table10.dat, table11.dat, table12.dat).
    - abundance_label: str, the abundance label to retrieve (e.g., '[Fe/H]', '[Mg/Fe]', '[Ba/Fe]').

    Returns:
    - pd.DataFrame with relevant data if the abundance label is valid, otherwise None.
    """
    # Map each abundance label to the corresponding reading function and file name
    abundance_to_info = {
        '[Fe/H]': ('table10.dat', read_table10),
        '[Mg/Fe]': ('table10.dat', read_table10),
        '[Ca/Fe]': ('table10.dat', read_table10),
        '[Sc/Fe]': ('table10.dat', read_table10),
        '[TiI/Fe]': ('table11.dat', read_table11),
        '[TiII/Fe]': ('table11.dat', read_table11),
        '[Cr/Fe]': ('table11.dat', read_table11),
        '[Mn/Fe]': ('table11.dat', read_table11),
        '[Co/Fe]': ('table12.dat', read_table12),
        '[NI/Fe]': ('table12.dat', read_table12),
        '[Ba/Fe]': ('table12.dat', read_table12),
        '[Eu/Fe]': ('table12.dat', read_table12)
    }

    # Check if the abundance label is valid
    if abundance_label not in abundance_to_info:
        print(f"Error: Abundance label '{abundance_label}' is not recognized.")
        return None

    # Extract the file name and function based on the abundance label
    file_name, read_function = abundance_to_info[abundance_label]
    file_path = os.path.join(directory_path, file_name)

    # Verify if the file exists
    if not os.path.isfile(file_path):
        print(
            f"Error: File '{file_name}' not found in directory '{directory_path}'.")
        return None

    # Read the file using the selected function
    data = read_function(file_path)

    # Check if the specified abundance label exists in the data
    if abundance_label in data.columns:
        return data[abundance_label], data[f"e_{abundance_label}"], data[f"er_{abundance_label}"]
    else:
        print(
            f"Warning: Abundance data '{abundance_label}' not found in the file.")
        return None, None, None


def download_and_extract(url):
    """
    Downloads a file from the specified URL, extracts it, and returns the directory name.

    Parameters:
    - url: str, the URL to download the file from.

    Returns:
    - str: The name of the directory created after extraction, or None if an error occurs.
    """
    # Define the local file name based on the URL
    local_filename = url.split('/')[-1]

    # Download the file
    print(f"Downloading {local_filename} from {url}...")
    response = requests.get(url, stream=True)
    if response.status_code == 200:
        with open(local_filename, 'wb') as f:
            for chunk in response.iter_content(chunk_size=8192):
                f.write(chunk)
    else:
        print(f"Error downloading file: {response.status_code}")
        return None

    print(local_filename)

    # Extract the downloaded file
    directory_name = None
    if local_filename.endswith('.zip'):
        with zipfile.ZipFile(local_filename, 'r') as zip_ref:
            zip_ref.extractall()
            # Get the first directory in the zip
            directory_name = zip_ref.namelist()[0]
    elif local_filename.endswith('.tar.gz'):
        with tarfile.open(local_filename, 'r:gz') as tar_ref:
            tar_ref.extractall()
            # Get the first directory in the tar.gz
            directory_name = tar_ref.getnames()[0]
    else:
        print("Unsupported file format. Please provide a .zip or .tar.gz file.")
        return None

    # Clean up by removing the downloaded file
    os.remove(local_filename)

    return directory_name

# %%
# Download the archive

# Extract the data
# url = "https://cdsarc.cds.unistra.fr/viz-bin/nph-Cat/tar.gz?J/A+A/642/A176"  # Replace with the actual URL
# directory = download_and_extract(url)


# %%
# Get the folder name
directory = "theler_et_al_2020_sextans/"
output_name = "/home/darwinr/Desktop/Scripts/MaDarwinR/UFDs_data/sextans_data"

# Example usage:
file_path = 'theler_et_al_2020_sextans/table10.dat'
MgFe, e_Mg_Fe, er_MgFe = read_abundance(directory, "[Mg/Fe]")
FeH, e_FeH, er_FeH = read_abundance(directory, "[Fe/H]")

plt.scatter(FeH, MgFe)

# Save the data to a more manageable format
np.savez_compressed(output_name, MgFe=MgFe, FeH=FeH, MgFe_syst_error=e_Mg_Fe,
                    MgFe_random_error=er_MgFe,  FeH_syst_error=e_FeH, FeH_random_error=er_FeH)
