#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Oct 27 15:10:59 2024

@author: darwinr
"""

import os
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

# %%


def read_tablec5(file_path):
    """
    Reads the `tablec5.dat` file, which contains star IDs and abundance data
    for multiple elements, including error columns.

    Parameters:
    - file_path (str): Path to the `tablec5.dat` file.

    Returns:
    - pd.DataFrame: DataFrame containing the abundances and error columns.
    """
    # Define column specifications and names based on the file structure
    colspecs = [
        (0, 7), (8, 13), (14, 18), (19, 24), (25, 29), (30, 35), (36, 40), (41, 46),
        (47, 51), (52, 57), (58, 62), (63, 68), (69, 73), (74, 79), (80, 84),
        (85, 90), (91, 95), (96, 101), (102, 106), (107, 112), (113, 117),
        (118, 122), (123, 127), (128, 133), (134, 138), (139, 144), (145, 149),
        (150, 155), (156, 160), (161, 166), (167, 171), (172, 177), (178, 182),
        (183, 188), (189, 193), (194, 199), (200, 204)
    ]

    column_names = [
        "Star", "[Fe/H]", "e_[Fe/H]", "[O/Fe]", "e_[O/Fe]", "[Na/Fe]", "e_[Na/Fe]", "[Mg/Fe]",
        "e_[Mg/Fe]", "[Si/Fe]", "e_[Si/Fe]", "[Ca/Fe]", "e_[Ca/Fe]", "[Sc/Fe]", "e_[Sc/Fe]",
        "[TiI/Fe]", "e_[TiI/Fe]", "[TiII/Fe]", "e_[TiII/Fe]", "[Cr/Fe]", "e_[Cr/Fe]",
        "[FeII/Fe]", "e_[FeII/Fe]", "[Co/Fe]", "e_[Co/Fe]", "[Ni/Fe]", "e_[Ni/Fe]", "[Zn/Fe]",
        "e_[Zn/Fe]", "[Ba/Fe]", "e_[Ba/Fe]", "[La/Fe]", "e_[La/Fe]", "[Nd/Fe]", "e_[Nd/Fe]",
        "[Eu/Fe]", "e_[Eu/Fe]"
    ]

    # Read fixed-width file, replacing '?' or other placeholders with NaN
    df = pd.read_fwf(file_path, colspecs=colspecs,
                     names=column_names, na_values=["?", "---"])

    # Convert all columns except 'Star' to floats
    for col in df.columns[1:]:
        df[col] = pd.to_numeric(df[col], errors='coerce')

    return df


def read_abundance(directory_path, abundance_label):
    """
    Reads the specified abundance data from the appropriate table file in a given directory.

    Parameters:
    - directory_path: str, path to the directory containing the table files (e.g., table10.dat, table11.dat, table12.dat).
    - abundance_label: str, the abundance label to retrieve (e.g., '[Fe/H]', '[Mg/Fe]', '[Ba/Fe]').

    Returns:
    - pd.DataFrame with relevant data if the abundance label is valid, otherwise None.
    """
    # Map each abundance label to the corresponding reading function and file name
    abundance_to_info = {
        '[Fe/H]': ('tablec5.dat', read_tablec5),
        '[O/Fe]': ('tablec5.dat', read_tablec5),
        '[Na/Fe]': ('tablec5.dat', read_tablec5),
        '[Mg/Fe]': ('tablec5.dat', read_tablec5),
        '[Si/Fe]': ('tablec5.dat', read_tablec5),
        '[Ca/Fe]': ('tablec5.dat', read_tablec5),
        '[Sc/Fe]': ('tablec5.dat', read_tablec5),
        '[TiI/Fe]': ('tablec5.dat', read_tablec5),
        '[TiII/Fe]': ('tablec5.dat', read_tablec5),
        '[Cr/Fe]': ('tablec5.dat', read_tablec5),
        '[FeII/Fe]': ('tablec5.dat', read_tablec5),
        '[Co/Fe]': ('tablec5.dat', read_tablec5),
        '[Ni/Fe]': ('tablec5.dat', read_tablec5),
        '[Zn/Fe]': ('tablec5.dat', read_tablec5),
        '[Ba/Fe]': ('tablec5.dat', read_tablec5),
        '[La/Fe]': ('tablec5.dat', read_tablec5),
        '[Nd/Fe]': ('tablec5.dat', read_tablec5),
        '[Eu/Fe]': ('tablec5.dat', read_tablec5)
    }

    # Check if the abundance label is valid
    if abundance_label not in abundance_to_info:
        print(f"Error: Abundance label '{abundance_label}' is not recognized.")
        return None

    # Extract the file name and function based on the abundance label
    file_name, read_function = abundance_to_info[abundance_label]
    file_path = os.path.join(directory_path, file_name)

    # Verify if the file exists
    if not os.path.isfile(file_path):
        print(
            f"Error: File '{file_name}' not found in directory '{directory_path}'.")
        return None

    # Read the file using the selected function
    data = read_function(file_path)

    # Check if the specified abundance label exists in the data
    if abundance_label in data.columns:
        return data[abundance_label], data[f"e_{abundance_label}"]
    else:
        print(
            f"Warning: Abundance data '{abundance_label}' not found in the file.")
        return None, None


# %%
# Get the folder name
directory = "hill_et_al_2019_sculptor/"
output_name = "/home/darwinr/Desktop/Scripts/MaDarwinR/UFDs_data/sculptor_data"

MgFe, e_MgFe = read_abundance(directory, "[Mg/Fe]")
FeH, e_FeH = read_abundance(directory, "[Fe/H]")
plt.scatter(FeH, MgFe)

# Save the data to a more manageable format
np.savez_compressed(output_name, MgFe=MgFe, FeH=FeH,
                    error_MgFe=e_MgFe, error_FeH=e_FeH)
