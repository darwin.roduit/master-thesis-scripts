
void c_interpolate_3d_projection(double n_part, double n_px_x, double n_px_y,
				 const double* x, const double* y, const double* value,
				 const double* smoothing_length, const double* mass,
				 const double* density, double pixel_width_max,
				 double (*kernel_integrated)(double),
				 const double** x_grid, const double** y_grid,
				 double** value_grid, double** normalization_grid);
