
double kernel_Wendland_C2(double q);
double kernel_cubic_spline_3D(double q);

/* Integrate 3D kernel in the z direction. Do this for a subset of points to be
   efficient and do linear interpolation */
void get_kernel_integrated(double (*kernel_fct)(double), double kernel_radius,
                           int n_points, double *q_xy_return,
                           double *kernel_integrated);


double linear_interpolate(double x, double x0, double y0, double x1, double y1);
