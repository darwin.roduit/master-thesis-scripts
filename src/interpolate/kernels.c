#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <omp.h>

#include "kernels.h"

// Define M_PI if it is not defined
#ifndef M_PI
#define M_PI 3.14159265358979323846
#endif

double kernel_Wendland_C2(double q) {
  if ((0 <= q) && (q <= 1)){
    return (1-q)*(1-q)*(1-q)*(1-q) * (4*1+1);
  } else {
    return 0.0;
  }
}

double kernel_cubic_spline_3D(double q) {
     /* """The kernel function needs to be mutpliplied by 1/h^3.""" */
  const double sigma_3 = 1.0/M_PI;

  if ((0.0 <= q) && (q < 1)) {
    return sigma_3*(1 - 1.5*q*q + 3.0/4.0*q*q*q);
  } else if ((1<= q) && (q <= 2)) {
    return sigma_3*(1.0/4.0*(2-q)*(2-q)*(2-q)) ;
  } else {
      return 0;
  }
}


// Optimized version 
void get_kernel_integrated(double (*kernel_fct)(double), double kernel_radius, int n_points, double *q_xy_return, double *kernel_integrated) {
    double *q_xy = (double *)malloc(n_points * sizeof(double));
    double integral;

    #pragma omp parallel for private(dz, integral, z, q, W_kern)
    for (int i = 0; i < n_points; ++i) {
        q_xy[i] = kernel_radius * i / (n_points - 1);
        double dz = sqrt(kernel_radius * kernel_radius - q_xy[i] * q_xy[i])/(n_points -1);
        integral = 0.0;
        for (int j = 0; j < n_points; ++j) {
	  double z = (j-1) * dz;
	  double q = sqrt(q_xy[i] * q_xy[i] + z * z);
	  double W_kern =  kernel_fct(q);

	  /* Trapezoidal integral */
	  if (j==1 || j==n_points) {
	    integral += 0.5*W_kern*dz;
	  } else {
	    integral += W_kern*dz;
	  }
        }
        kernel_integrated[i] = 2.0 * integral;
	q_xy_return[i] = q_xy[i];
    }
    q_xy_return[n_points] = q_xy_return[n_points - 1] + 1.0 / n_points;
    free(q_xy);
}


// Linear interpolation function
double linear_interpolate(double x, double x0, double y0, double x1, double y1) {
    return y0 + (y1 - y0) * ((x - x0) / (x1 - x0));
}
