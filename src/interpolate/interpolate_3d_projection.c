#include <Python.h> /* Must be the first header */
#include <math.h>
#include <numpy/arrayobject.h>
#include <omp.h>

#include "interpolate_3d_projection.h"
#include "../minmax.h"
#include "kernels.h"

/* Non parallel version */
/* static PyObject *do_interpolate_3d_projection(PyObject *self, PyObject *args) { */

/*   PyArrayObject *x; */
/*   PyArrayObject *y; */
/*   PyArrayObject *value = NULL; */
/*   PyArrayObject *smoothing_length = NULL; */
/*   PyArrayObject *mass = NULL; */
/*   PyArrayObject *density = NULL; */

/*   double pixel_width_max = 0; */

/*   PyArrayObject *x_grid = NULL; */
/*   PyArrayObject *y_grid = NULL; */

/*   /\* Return values (are given as argument)*\/ */
/*   PyArrayObject *value_grid = NULL; */
/*   PyArrayObject *normalization_grid = NULL; */
 
/*   if (!PyArg_ParseTuple(args, "O!O!O!O!O!O!dO!O!O!O!", &PyArray_Type, &x, &PyArray_Type, &y, */
/* 			&PyArray_Type, &value, &PyArray_Type, &smoothing_length, */
/* 			&PyArray_Type, &mass, &PyArray_Type, &density, &pixel_width_max, */
/* 			&PyArray_Type, &x_grid, &PyArray_Type, &y_grid, */
/* 			&PyArray_Type, &value_grid, &PyArray_Type, &normalization_grid)) { */
/*     return NULL; */
/* } */

/*   /\* Get n_part, n_px_x, n_px_y *\/ */
/*   npy_intp n_part = PyArray_DIM(x, 0); */
/*   npy_intp n_px_x = PyArray_DIM(x_grid, 0); */
/*   npy_intp n_px_y = PyArray_DIM(x_grid, 1); */

/*   /\* Check that all arrays have the right length *\/ */
      
/*   /\* Should be options *\/ */
/*   double kernel_radius = 1.0; */
/*   int n_points = 1000; */

/*   // Allocate memory for kernel integration results */
/*   double *q_xy_return = (double *)malloc((n_points + 1) * sizeof(double)); */
/*   double *kernel_integrated = (double *)malloc((n_points + 1) * sizeof(double)); */

/*   // Compute the kernel integration */
/*   get_kernel_integrated(kernel_Wendland_C2, kernel_radius, n_points, q_xy_return, kernel_integrated); */


/*   /\* Do the deed *\/ */
/*   double h = 0; */
/*   double w_j = 0; */
/*   double r = 0; */

/*   double h_part, x_part, y_part, mass_part, density_part, value_part; */
/*   double x_grid_ij, y_grid_ij ; */
/*   double* value_grid_ij; */
/*   double* normalization_grid_ij; */

/*   //This loop need to be performed in C and accelerated with openMP */
/*   for (int part=0 ;part < n_part ; ++part) { */
/*     x_part = *(double *)PyArray_GETPTR1(x, part); */
/*     y_part = *(double *)PyArray_GETPTR1(y, part); */
/*     mass_part = *(double *)PyArray_GETPTR1(mass, part); */
/*     density_part = *(double *)PyArray_GETPTR1(density, part); */
/*     value_part = *(double *)PyArray_GETPTR1(value, part); */
/*     h_part = *(double *)PyArray_GETPTR1(smoothing_length, part); */

/*     /\* Set a minimal smoothing length *\/ */
/*     h = max(h_part, pixel_width_max/2.0) ; */

/*     /\* Loop over the matrix row and columns. *\/ */
/*     for (int i=0 ; i < n_px_x ; ++i) { */
/*       for (int j=0 ; j < n_px_y ; ++j) { */
/* 	x_grid_ij = *(double *)PyArray_GETPTR2(x_grid, i, j); */
/* 	y_grid_ij = *(double *)PyArray_GETPTR2(y_grid, i, j); */
	
/* 	value_grid_ij = (double *)PyArray_GETPTR2(value_grid, i, j); */
/* 	normalization_grid_ij = (double *)PyArray_GETPTR2(normalization_grid, i, j); */

/* 	/\* Compute the 2D radius of ech pixel centered on this particle *\/ */
/* 	r = sqrt((x_grid_ij - x_part)*(x_grid_ij - x_part) + (y_grid_ij - y_part)*(y_grid_ij - y_part)); */
	
/* 	/\* If you contribute to this pixel *\/ */
/* 	if (r <= h) { */
/* 	  // Perform linear interpolation for kernel value */
/* 	  double q = r / h; */
/* 	  int idx = (int)(q * n_points / kernel_radius); */
/* 	  double W; */
/* 	  if (idx >= n_points) { */
/* 	    W = 0.0; */
/* 	  } else { */
/* 	    W = linear_interpolate(q, q_xy_return[idx], kernel_integrated[idx], q_xy_return[idx + 1], kernel_integrated[idx + 1]); */
/* 	  } */
	  
/* 	  w_j = mass_part/(density_part*h*h*h); /\* sph weight *\/ */
/* 	  *value_grid_ij += w_j*h*value_part*W; */
/* 	  *normalization_grid_ij += w_j*h*W; */
/* 	} */
/*       } /\* loop over the j axis *\/ */
/*     } /\* Loop over the i axis *\/ */
/*   } /\* Loop over the particles *\/ */

/*   /\* Now normalize the value_grid *\/ */
/*   for (int i=0 ; i < n_px_x ; ++i) { */
/*     for (int j=0 ; j < n_px_y ; ++j) { */
/*       value_grid_ij = (double *)PyArray_GETPTR2(value_grid, i, j); */
/*       normalization_grid_ij = (double *)PyArray_GETPTR2(normalization_grid, i, j); */
/*       if (*normalization_grid_ij != 0) { */
/* 	*value_grid_ij /= *normalization_grid_ij; */
/*       } */
/*     } */
/*   } */

/*   free(q_xy_return); */
/*   free(kernel_integrated); */
/*   return Py_BuildValue(""); */
/* } */

/* Improved wihtout fct overheads */
static PyObject *do_interpolate_3d_projection(PyObject *self, PyObject *args) {

  PyArrayObject *x;
  PyArrayObject *y;
  PyArrayObject *value = NULL;
  PyArrayObject *smoothing_length = NULL;
  PyArrayObject *mass = NULL;
  PyArrayObject *density = NULL;

  double pixel_width_max = 0;

  PyArrayObject *x_grid = NULL;
  PyArrayObject *y_grid = NULL;

  /* Return values (are given as argument)*/
  PyArrayObject *value_grid = NULL;
  PyArrayObject *normalization_grid = NULL;
 
  if (!PyArg_ParseTuple(args, "O!O!O!O!O!O!dO!O!O!O!", &PyArray_Type, &x, &PyArray_Type, &y,
			&PyArray_Type, &value, &PyArray_Type, &smoothing_length,
			&PyArray_Type, &mass, &PyArray_Type, &density, &pixel_width_max,
			&PyArray_Type, &x_grid, &PyArray_Type, &y_grid,
			&PyArray_Type, &value_grid, &PyArray_Type, &normalization_grid)) {
    return NULL;
}

  /* Get n_part, n_px_x, n_px_y */
  npy_intp n_part = PyArray_DIM(x, 0);
  npy_intp n_px_x = PyArray_DIM(x_grid, 0);
  npy_intp n_px_y = PyArray_DIM(x_grid, 1);

  /* Check that all arrays have the right length */
      
  /* Should be options */
  double kernel_radius = 1.0;
  int n_points = 1000;

  // Allocate memory for kernel integration results
  double *q_xy_return = (double *)malloc((n_points + 1) * sizeof(double));
  double *kernel_integrated = (double *)malloc((n_points + 1) * sizeof(double));

  // Compute the kernel integration
  get_kernel_integrated(kernel_Wendland_C2, kernel_radius, n_points, q_xy_return, kernel_integrated);

  // Extract pointers to array data
  double *x_data = (double *)PyArray_DATA(x);
  double *y_data = (double *)PyArray_DATA(y);
  double *value_data = (double *)PyArray_DATA(value);
  double *smoothing_length_data = (double *)PyArray_DATA(smoothing_length);
  double *mass_data = (double *)PyArray_DATA(mass);
  double *density_data = (double *)PyArray_DATA(density);
  double *x_grid_data = (double *)PyArray_DATA(x_grid);
  double *y_grid_data = (double *)PyArray_DATA(y_grid);
  double *value_grid_data = (double *)PyArray_DATA(value_grid);
  double *normalization_grid_data = (double *)PyArray_DATA(normalization_grid);

  // Loop over the particles
  for (int part = 0; part < n_part; ++part) {
    double x_part = x_data[part];
    double y_part = y_data[part];
    double mass_part = mass_data[part];
    double density_part = density_data[part];
    double value_part = value_data[part];
    double h_part = smoothing_length_data[part];

    // Set a minimal smoothing length
    double h = max(h_part, pixel_width_max / 2.0);

    // Loop over the matrix rows and columns
    for (int i = 0; i < n_px_x; ++i) {
      for (int j = 0; j < n_px_y; ++j) {
	double x_grid_ij = x_grid_data[i * n_px_y + j];
	double y_grid_ij = y_grid_data[i * n_px_y + j];
	double *value_grid_ij = &value_grid_data[i * n_px_y + j];
	double *normalization_grid_ij = &normalization_grid_data[i * n_px_y + j];

	// Compute the 2D radius of each pixel centered on this particle
	double r = sqrt((x_grid_ij - x_part) * (x_grid_ij - x_part) + (y_grid_ij - y_part) * (y_grid_ij - y_part));

	// If you contribute to this pixel
	if (r <= h) {
	  // Perform linear interpolation for kernel value
	  double q = r / h;
	  int idx = (int)(q * n_points / kernel_radius);
	  double W = (idx >= n_points) ? 0.0 : linear_interpolate(q, q_xy_return[idx], kernel_integrated[idx], q_xy_return[idx + 1], kernel_integrated[idx + 1]);

	  double w_j = mass_part / (density_part * h * h * h); /* sph weight */
	  *value_grid_ij += w_j * h * value_part * W;
	  *normalization_grid_ij += w_j * h * W;
	}
      } /* loop over the j axis */
    } /* Loop over the i axis */
  } /* Loop over the particles */

  /* Now normalize the value_grid */
  for (int i=0 ; i < n_px_x ; ++i) {
    for (int j=0 ; j < n_px_y ; ++j) {
      double *value_grid_ij = &value_grid_data[i * n_px_y + j];
      double *normalization_grid_ij = &normalization_grid_data[i * n_px_y + j];
      if (*normalization_grid_ij != 0) {
	*value_grid_ij /= *normalization_grid_ij;
      }
    }
  }

  free(q_xy_return);
  free(kernel_integrated);
  return Py_BuildValue("");
}


/*------------------------------ Python things ------------------------------*/
static PyMethodDef InterpolateMethods[] = {
    {"do_interpolate_3d_projection", do_interpolate_3d_projection, METH_VARARGS,
     "Interpolate SPH data quantities in 3D and project them on a 2D grid."},
    {NULL, NULL, 0, NULL}        /* Sentinel */
};

 static struct PyModuleDef interpolate_module = {
    PyModuleDef_HEAD_INIT,
    "interpolate",   /* name of module */
    "Module doc", /* module documentation, may be NULL */
    -1,       /* size of per-interpreter state of the module,
                 or -1 if the module keeps state in global variables. */
    InterpolateMethods
};

PyMODINIT_FUNC PyInit_interpolate(void) {
  PyObject *m;
  m = PyModule_Create(&interpolate_module);
  if (m == NULL) return NULL;

  import_array();

  return m;
}
