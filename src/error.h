/*******************************************************************************
 * This file is part of MaDarwinR moduel and was copied from SWIFT.
 * Copyright (c) 2024 Darwin Roduit (darwin.roduit@epfl.ch)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/
#ifndef ERROR_H
#define ERROR_H


/* Some standard headers. */
#include <stdio.h>
#include <stdlib.h>


/* Local headers. */
#include "clocks.h"

/* Use exit when not developing, avoids core dumps. */
#define module_abort(errcode) exit(errcode)

/**
 * @brief Error macro. Prints the message given in argument and aborts.
 *
 */
#define error(s, ...)                                                      \
  ({                                                                       \
    fflush(stdout);                                                        \
    fprintf(stderr, "%s %s:%s():%i: " s "\n", clocks_get_timesincestart(), \
            __FILE__, __FUNCTION__, __LINE__, ##__VA_ARGS__);              \
    fflush(stderr);                                                        \
    module_abort(1);                                                        \
  })


/**
 * @brief Macro to print a localized message with variable arguments.
 *
 */
#define message(s, ...)                                                 \
  ({                                                                    \
    printf("%s %s: " s "\n", clocks_get_timesincestart(), __FUNCTION__, \
           ##__VA_ARGS__);                                              \
  })

/**
 * @brief Macro to print a simple message with variable arguments.
 * Useful for system messages when time has not started. Assumes these
 * are important, so immediately flushed (can get lost in MPI shutdowns).
 *
 */
#define pretime_message(s, ...)                         \
  ({                                                    \
    printf("%s: " s "\n", __FUNCTION__, ##__VA_ARGS__); \
    fflush(stdout);                                     \
  })

/**
 * @brief Macro to print a localized warning message with variable arguments.
 *
 * Same as message(), but this version prints to the standard error.
 *
 */
#define warning(s, ...)                                                     \
  ({                                                                        \
    fprintf(stderr, "%s %s: WARNING: " s "\n", clocks_get_timesincestart(), \
            __FUNCTION__, ##__VA_ARGS__);                                   \
  })

/**
 * @brief Assertion macro compatible with MPI
 *
 */
#define assert(expr)                                                          \
  ({                                                                          \
    if (!(expr)) {                                                            \
      fflush(stdout);                                                         \
      fprintf(stderr, "%s %s:%s():%i: FAILED ASSERTION: " #expr " \n",        \
              clocks_get_timesincestart(), __FILE__, __FUNCTION__, __LINE__); \
      fflush(stderr);                                                         \
      module_abort(1);                                                         \
    }                                                                         \
  })

#endif /* ERROR_H */
