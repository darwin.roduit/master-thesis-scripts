#!/usr/bin/env python
'''
 @package   MaDarwinR
 @file      setup.py
 @brief     Install MaDarwinR
 @copyright GPLv3
 @author    Darwin Roduit <darwin.roduit@epfl.ch>
 @section   COPYRIGHT  Copyright (C) 2024 EPFL (Ecole Polytechnique Federale de Lausanne)  LASTRO - Laboratory of Astrophysics of EPFL

 This file is part of MaDarwinR.
'''

import os
import sys
from setuptools import setup, find_packages, Extension
from glob import glob
import numpy as np
from copy import deepcopy
import versioneer
# import atexit
# import shutil
# import matplotlib

# get and save the current git version
# versioneer.get_version("MaDarwinR")

package_files = glob("")

###################################################
# src
###################################################

# C compiler flags
flags = ["-Werror=strict-prototypes", "-Wno-unused-parameter"]
OPTIMIZE  = ["-std=c11", "-ggdb", "-O3", "-Wall", "-Wno-format-security", "-Wno-unknown-pragmas",
             "-Wno-unused-function"]
# CFLAGS_SYST += -Wformat-overflow=0 -Wno-stringop-truncation

# C library headers
include_dirs = [
    ".",
    "/usr/include",     # gsl
    "/usr/local/include",     # macOS (homebrew) headers
    np.get_include(),
]

# libraries
libraries = ["m", "gomp"]

link_args = ["-fopenmp"]

# C modules
modules_name = [
    "interpolate"
]


###################################################
# Compile the C files
###################################################

# Generate extensions
ext_modules = []

# for k in modules_name:
#     # Get flags
#     extra_flags = deepcopy(flags) + deepcopy(OPTIMIZE)

#     # Get libraries
#     extra_lib = deepcopy(libraries)

#     # Get library directories
#     extra_lib_dirs = []

#     extra_link_args = deepcopy(link_args)


#     # Compile extension
#     tmp = Extension("MaDarwinR." + k,
#                     glob("src/" + k + "/*.c"),
#                     include_dirs=include_dirs,
#                     libraries=extra_lib,
#                     extra_compile_args=extra_flags,
#                     extra_link_args=extra_link_args,
#                     library_dirs=extra_lib_dirs)
#     ext_modules.append(tmp)


# # Add the files in the src folder but not in any subfolders
# ext = Extension("MaDarwinR.",
#                 glob("src/*.c"),
#                 include_dirs=include_dirs+["src/"],
#                 libraries=extra_lib,
#                 extra_compile_args=extra_flags,
#                 extra_link_args=extra_link_args,
#                 library_dirs=extra_lib_dirs)
# ext_modules.append(ext)

###################################################
# Script to install on the system
###################################################

scripts = glob("scripts/*")

###################################################
# Setup
###################################################

setup(
    name="MaDarwinR",
    author="Darwin Roduit",
    author_email="darwin.roduit@epfl.ch",
    url="",
    description="""Scripts used during Darwin Roduit's master thesis, all packaged inside a module""",
    license="GPLv3",
    version="1.0",

    packages=find_packages(),
    scripts=scripts,
    ext_modules=ext_modules,
    package_data={'MaDarwinR': package_files},
    install_requires=["wheel", "numpy", "scipy", "h5py",
                      "astropy", "pNbody", "matplotlib"],


)
